// Script responsável pela tradução da DataTable, está dividido nesse documento separadamente, porque será reutilizado em todas as outras futuras tabelas

  $(document).ready(function () {
  $('#dtBasicExample').DataTable({
				         "language": {
                                             "search": "Procurar:",
                                             "emptyTable": "Sem resultados",
                                             "infoEmpty": "Sem resultados disponíveis.",
                                             "lengthMenu": "Mostrar _MENU_ resultados",
				         "paginate": {
				             "previous": "Página Anterior", 
                                             "next": "Proxima página",
                                         }
                                        }
				
        });
  $('.dataTables_length').addClass('bs-select');
  
});



