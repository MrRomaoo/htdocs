<!DOCTYPE html>
<html lang="pt-br">

    <head>

        <?php
        include("../uteis/meta-link.php");
        include("../uteis/conectaBanco.php");
        ?>

    </head>

    <body>

        <?php include("../uteis/navbar.php"); ?>

        <div class="col-xs-2 col-sm-2 col-lg-2"> <!--Div para controle de colunas, esta div e necessária para a criação de todos os forms-->

            <div class="form-group" id="div-select"> <!-- Select padrão -->

                <label for="filtro">Filtro</label>
                <select class="form-control" name="filtro" id="filtro">

                    <?php
                    $resultados = $conn->query("SELECT id_filtro_titulo, desc_filtro_titulo, valor_banco_filtro FROM filtro_titulo");

                    while ($row = $resultados->fetch(PDO::FETCH_ASSOC)) {

                        echo "<option value=" . $row['valor_banco_filtro'] . ">" . $row['desc_filtro_titulo'] . "</option>";
                    }

                    $conn->connection = null;
                    ?>

                </select>

            </div> <!-- (FIM) Select padrão -->


            <div class="form-group"> <!-- Input padrão -->
                <label for="formGroupExampleInput">Pesquisa</label>    
                <input type="text" class="form-control" id="pesquisa" name="pesquisa" length="10" maxlength="10" placeholder="Digite aqui">
            </div> <!-- (FIM)Input padrão -->


            <div class="form-group"> <!-- Botão padrão -->
                <button  class="btn btn-yellow btn-md" onclick="exibeTabela()" > Pesquisar Novo</button>
            </div> <!-- (FIM)Botão padrão -->


            <div class="form-group"> <!-- Botão padrão -->
                <a href="novoTitulo.php"><button type="button" class="btn btn-dark-green btn-md" > Novo titulo </button></a>
            </div> <!-- (FIM)Botão padrão -->


            <div class="form-group" id="div-grupo-check"> <!-- Div que indica o uso de um grupo de check buttons -->           

                <div class="custom-control custom-checkbox ">
                    <input type="checkbox" class="custom-control-input" id="check-1" name="check-1" checked>
                    <label class="custom-control-label" for="check-1">Marcada</label>
                </div>

                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="check-2" name="check-2" >
                    <label class="custom-control-label" for="check-2">Desmarcada</label>
                </div>

            </div> <!-- (FIM) Div que indica o uso de um grupo de check buttons -->    


            <div class="form-group" id="div-grupo-radio"> <!-- Div que indica o uso de um grupo de radio buttons -->

                <div class="custom-control custom-radio"> <!-- Radio Padrão -->
                    <input type="radio" class="custom-control-input" id="radio-1" name="grupo-radio" checked>
                    <label class="custom-control-label" for="radio-1">Option 1 Marcada </label>
                </div> <!-- (FIM) Radio Padrão -->

                <div class="custom-control custom-radio">  <!-- Radio Padrão -->
                    <input type="radio" class="custom-control-input" id="radio-2" name="grupo-radio">
                    <label class="custom-control-label" for="radio-2">Option 2</label>
                </div> <!-- (FIM) Radio Padrão -->

                <div class="custom-control custom-radio"> <!-- Radio Padrão -->
                    <input type="radio" class="custom-control-input" id="radio-3" name="grupo-radio">
                    <label class="custom-control-label" for="radio-3">Option 3</label>
                </div>  <!-- (FIM) Radio Padrão -->

            </div> <!-- (FIM) Div que indica o uso de um grupo de radio buttons -->


        </div> <!-- (FIM) Div para controle de colunas, esta div e necessária para a criação de todos os forms-->


        <div id="tabela" >

        </div>


        <!-- Modais e outros elementos que não fazem parte da interface do usuário-->

        <!-- Mensagem modal para confirmação da exclusão do item. -->
        <div class="modal fade" id="exemploModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Atenção</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        Deseja realmente excluir o titulo?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-blue-grey" data-dismiss="modal">Não</button>
                        <button type="button" class="btn btn-blue-grey" id="btnSim" name="btnSim" onclick="deletaTitulo(recebeIdTitulo)">Sim </button>
                    </div>
                </div>
            </div>
        </div> <!-- (FIM) Mensagem modal para confirmação da exclusão do item. -->   

        <!-- (FIM) Modais e outros elementos que não fazem parte da interface do usuário-->


        <!-- SCRIPTS -->
        <!-- JQuery -->
        <script type="text/javascript" src="..\assets/mdb-data-table/js/jquery-3.3.1.min.js"></script>
        <!-- Bootstrap tooltips -->
        <script type="text/javascript" src="..\assets/mdb-data-table/js/popper.min.js"></script>
        <!-- Bootstrap core JavaScript -->
        <script type="text/javascript" src="..\assets/mdb-data-table/js/bootstrap.min.js"></script>
        <!-- MDB core JavaScript -->
        <script type="text/javascript" src="..\assets/mdb-data-table/js/mdb.min.js"></script>
        <script type="text/javascript" src="..\assets/mdb-data-table/js/addons/datatables.min.js"></script>
        <script type="text/javascript" src="..\assets/mdb-data-table/js/scriptTraducaoDataTables.js"></script>


        <!-- SCRIPTS AJAX  -->
        <script>
                            // Início do script de exibição do Modal
                            var recebeIdTitulo;

                            function exibeModal(idTitulo) {

                                recebeIdTitulo = idTitulo;

                                $('#exemploModal').modal().withTimeout(1000);

                            }

                            // (FIM) Script de exibição do Modal

                            //Script responsável pela exclusão do título. Ele é chamado no botão "Sim" do Modal, fazendo a exclusão AJAX do titulo
                            function deletaTitulo(idtitulo) {

                                var xhttp = new XMLHttpRequest();
                                var recebeIdTitulo = idtitulo;

                                xhttp.onreadystatechange = function () {
                                    if (this.readyState == 4 && this.status == 200) {
                                        //document.getElementById("tabela").innerHTML = this.responseText;
                                        alert('Deletado com sucesso!');
                                    }
                                };

                                xhttp.open("POST", "DAO/classes/trataExcluiTitulo.php", true);
                                xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                                xhttp.send("recebeIdTitulo=" + recebeIdTitulo);

                            }
                            // (FIM) Script responsável pela exclusão do título. Ele é chamado no botão "Sim" do Modal, fazendo a exclusão AJAX do titulo

                            //Script responsável pela inserção em AJAX da tabela criada no documento trataListagem.php 
                            function exibeTabela() {

                                var xhttp = new XMLHttpRequest();

                                var pesquisa = document.getElementById("pesquisa").value;
                                var filtro = document.getElementById("filtro").value;

                                xhttp.onreadystatechange = function () {
                                    if (this.readyState == 4 && this.status == 200) {
                                        document.getElementById("tabela").innerHTML = this.responseText;

                                        // Este bloco precisa ser executado neste momento, pois as configurações precisam ser aplicadas a tabela que vem de outro documento, não retirar daqui.
                                        $('#dtBasicExample').DataTable({
                                            "language": {
                                                "search": "Procurar:",
                                                "emptyTable": "Sem resultados",
                                                "infoEmpty": "Sem resultados disponíveis.",
                                                "lengthMenu": "Mostrar _MENU_ resultados",
                                                "paginate": {
                                                    "previous": "Página Anterior",
                                                    "next": "Proxima página",
                                                }
                                            }

                                        });
                                        $('.dataTables_length').addClass('bs-select');
//(FIM) Este bloco precisa ser executado neste momento, pois as configurações precisam ser aplicadas a tabela que vem de outro documento, não retirar daqui.

                                    }
                                };
                                xhttp.open("POST", "trataListagem.php", true);
                                xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                                xhttp.send("pesquisa=" + pesquisa + "&" + "filtro=" + filtro);

                            }

//(FIM) Script responsável pela inserção em AJAX da tabela criada no documento trataListagem.php 

        </script>

    </body>

</html>

