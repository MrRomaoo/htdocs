<?php

function populaFiltro() {

    include("conectaBanco.php");

    $resultados = $conn->query("SELECT id_filtro_titulo, desc_filtro_titulo, valor_banco_filtro FROM filtro_titulo");

    while ($row = $resultados->fetch(PDO::FETCH_ASSOC)) {

        echo "<option value=" . $row['valor_banco_filtro'] . ">" . $row['desc_filtro_titulo'] . "</option>";
    }

    $conn->connection = null;
}
?>

<?php

function populaTipo() {

    include('conectaBanco.php');
    ?>



    <?php

    $resultados = $conn->query("SELECT idtipo , nome FROM tipo");

    while ($row = $resultados->fetch(PDO::FETCH_ASSOC)) {


        echo "<option value=" . $row['idtipo'] . ">" . $row['nome'] . "</option>";
    }

    $conn->connection = null;
    ?>


    <?php

}
?>



<?php

function populaFornecedor() {

    include('conectaBanco.php');
    ?>

    <?php

    $resultados = $conn->query("SELECT idfornecedor , nome as novo_nome FROM fornecedor");

    while ($row = $resultados->fetch(PDO::FETCH_ASSOC)) {

        echo "<option value=" . $row['idfornecedor'] . ">" . $row['novo_nome'] . "</option>";
    }

    $conn->connection = null;
    ?>


    <?php

}
?>
