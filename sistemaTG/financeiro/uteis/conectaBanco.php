<?php 

$servidor = "localhost";
$banco = "mydb";
$usuario = "root";
$senha = "123456";

try {
    
    $conn = new PDO("mysql:dbname=$banco;host=$servidor",$usuario, $senha);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    
 } catch (PDOException $e) {
     
     echo $e->getMessage();
     
}   
    
?>