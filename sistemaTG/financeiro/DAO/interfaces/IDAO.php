<?php

interface IDAO {
    
     public function insert(Titulo $titulo);
     
     public function delete(Titulo $titulo);

     public function update(Titulo $titulo);
     
     public function select(Titulo $titulo);
}
