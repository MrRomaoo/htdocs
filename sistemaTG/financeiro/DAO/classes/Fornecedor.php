<?php

    class Fornecedor{
        
        private $id;
        
        private $nome;
        
        private $cnpj;
        
        private $cpf;
        
        function __construct($id, $nome=NULL, $cnpj=NULL, $cpf=NULL) {
          
            $this->id = $id;
            
            $this->nome = $nome;
            
            $this->cnpj = $cnpj;
            
            $this->cpf = $cpf;
        
            
        }

        
        
        
        function getId() {
            return $this->id;
        }

        function getNome() {
            return $this->nome;
        }

        function getCnpj() {
            return $this->cnpj;
        }

        function getCpf() {
            return $this->cpf;
        }

        function setId($id) {
            $this->id = $id;
        }

        function setNome($nome) {
            $this->nome = $nome;
        }

        function setCnpj($cnpj) {
            $this->cnpj = $cnpj;
        }

        function setCpf($cpf) {
            $this->cpf = $cpf;
        }


        
    }

