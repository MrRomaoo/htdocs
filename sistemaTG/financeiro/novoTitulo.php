<head>

    <title> Cadastro de Titulo </title>

    <?php include("../uteis/meta-link.php") ?>

</head>



<body>

    <?php include("../uteis/navbar.php") ?>

    <div class="cadtit col-xs-3 col-sm-3 col-lg-3">

        <h2 class="h2"> Cadastro de Títulos </h2>

    </div>

    <div>

        <form method="post" action="DAO/classes/trataInsere.php" enctype="multipart/form-data">

            <div class="col-xs-3 col-sm-3 col-lg-3">
                <label for="desc"> Descrição </label>
                <input type="text" id="desc" name="desc" class="form-control">
            </div>

            <div class="col-xs-3 col-sm-3 col-lg-3">
                <label for="dataEmissao"> Data da Emissão </label>
                <input type="date" id="dataEmissao" name="dataEmissao"class="form-control">
            </div>	

            <div class="col-xs-3 col-sm-3 col-lg-3">
                <label for="nDoc">N º do Documento</label>
                <input type="number" id="nDoc" name="nDoc" class="form-control">
            </div>

            <div class="col-xs-3 col-sm-3 col-lg-3">
                <label for="dataVcto"> Data Vencimento</label>
                <input type="date" id="dataVcto" name="dataVcto" class="form-control">
            </div>

            <div class="col-xs-3 col-sm-3 col-lg-3">
                <label for="valor">Valor</label>
                <input type="number" step="0.01" name="valor" id="valor" class="form-control">
            </div>

            <div class="col-xs-3 col-sm-3 col-lg-3 labforn">
                <label for="fornecedor"> Fornecedor</label>

                <select name="fornecedor">

                    <?php
                    $resultados = $conn->query("SELECT idfornecedor , nome FROM fornecedor");

                    while ($row = $resultados->fetch(PDO::FETCH_ASSOC)) {


                        echo "<option value=" . $row['idfornecedor'] . ">" . $row['nome'] . "</option>";
                    }

                    $conn->null;
                    ?>

                </select>

            </div>

            <div class="col-xs-3 col-sm-3 col-lg-3">


                <label for="tipo"> Tipo </label>

                <select name="tipo">

<?php
$resultados = $conn->query("SELECT idtipo , nome FROM tipo");

while ($row = $resultados->fetch(PDO::FETCH_ASSOC)) {


    echo "<option value=" . $row['idtipo'] . ">" . $row['nome'] . "</option>";
}

$conn->null;
?>

                </select>

            </div>

            <div class="col-xs-3 col-sm-3 col-lg-3 botaoCad" >

                <button type="submit" class="btn btn-dark-green"> Cadastrar </button>

            </div>

        </form>

    </div>

</body>

