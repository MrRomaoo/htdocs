<?php

function excluiTitulo() {

    include("../uteis/conectaBanco.php");

    $idTitulo = $_POST['recebeIdTitulo'];

    $sql = "DELETE
            FROM
                titulo
            WHERE
               
                idtitulo=$idTitulo";

    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $stmt = $conn->prepare($sql);
    $stmt->execute();

    $count = $stmt->rowCount();

    if ($count > 0) {

        echo "Foi";
    } else {

        echo "Não foi";
    }

    $conn->connection = null;
    $stmt->connection = null;
}
?>

<?php

function exibeTabela() {

       include("../uteis/conectaBanco.php");
    
    $resultado = $_POST['pesquisa'];
    $resultado2 = $_POST['filtro'];

    /* $resultado = 'a';
      $resultado2 = 'descricao'; */

    $abcs = $conn->query("SELECT
                            t.idtitulo,
                            t.descricao,
                            DATE_FORMAT(t.dataEmissao,'%d/%m/%Y') as t_dataEmissao,
                            t.numeroDocumento,
                            DATE_FORMAT(t.dataVencimento,'%d/%m/%Y') as t_dataVencimento,
                            t.valor,
                            f.nome as teste,
                            p.nome
                        FROM
                                titulo AS t
                        INNER JOIN
                                fornecedor AS f ON f.idfornecedor = t.forn_idfornecedor
                        INNER JOIN
                                tipo AS p ON p.idtipo = t.tipo_idtipo
                        WHERE t.$resultado2 LIKE '%$resultado%'");
    ?>
    <!-- Tabela com integração ao banco que retorna os registros baseados no filtro da página home.php -->

    <div class="container-fluid">

        <table id="dtBasicExample" class="table table-hover table-bordered table-sm" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th class="th-sm">Código
                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                    </th>
                    <th class="th-sm">Descri&ccedil&atildeo
                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                    </th>
                    <th class="th-sm"> Data de Emiss&atildeo
                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                    </th>
                    <th class="th-sm"> Data de Vencimento
                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                    </th>
                    <th class="th-sm">N&uacutemero do Documento
                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                    </th>
                    <th class="th-sm"> Valor
                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                    </th>
                    <th class="th-sm"> Fornecedor
                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                    </th>
                    <th class="th-sm"> Tipo
                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                    </th>
                    <th class="th-sm"> Editar
                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                    </th>
                    <th class="th-sm"> Deletar
                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                    </th>
                </tr>
            </thead>
            <tbody>
                <!-- Laço While que retorna os resultados do banco e preenche as células com os resultados da query, enquanto houver registros pendentes. -->
                <?php
                while ($row = $abcs->fetch(PDO::FETCH_ASSOC)) {
                    ?> 

                    <tr align="center">

                        <th scope="row"> <?php echo $row['idtitulo']; ?></th>
                        <td align="center"> <?php echo $row['descricao']; ?></td>
                        <td align="center"> <?php echo $row['t_dataEmissao']; ?></td> 
                        <td align="center"> <?php echo $row['t_dataVencimento']; ?></td>
                        <td align="center"> <?php echo $row['numeroDocumento']; ?></td>
                        <td align="center"> <?php echo $row['valor']; ?></td>
                        <td align="center"> <?php echo $row['teste']; ?></td>         
                        <td align="center"> <?php echo $row['nome']; ?></td>

                        <!-- Botões -->

                        <!-- Botão que por método GET envia o id do título que será editado na página EditaTitulo.php -->
                        <td align="center"> <a href="edita-titulo.php?codigo=<?php echo $row['idtitulo']; ?>"> <button type="button" class="btn btn-blue btn-sm m-0"> Editar </button> </a> </td>

                        <!-- Botão que irá acionar a exibição de um modal para exclusão do título por meio de AJAX -->
                        <td> <button type="button" class="btn btn-red btn-sm m-0" onclick="exibeModal(<?php echo $row['idtitulo']; ?>)"> Deletar </button> </td>

                        <!-- (FIM) Botões -->

                    </tr>

                    <?php
                }
                ?>

            </tbody>
            <!--Table body-->
            <tfoot>

                <tr>
                    <th> C&oa&oacutedigo</i>
                    </th>
                    <th>Descri&ccedil&atildeo</i>
                    </th>
                    <th>Data de Emiss&atildeo</i>
                    </th>
                    <th> Data de Vencimento</i>
                    </th>
                    <th> N&uacutemero do Documento </i>
                    </th>
                    <th> Valor</i>
                    </th>
                    <th> Fornecedor</i>
                    </th>
                    <th> Tipo</i>
                    </th>
                    <th> Editar</i>
                    </th>
                    <th> Deletar </i>
                    </th>
                </tr>

            </tfoot>

        </table>
    </div>

 <?php
}
?>

    
    
<?php
switch ($_POST["acao"]) {

    case 1:

        excluiTitulo();

        break;

    case 2:

        exibeTabela();

        break;

    default:
        break;
}
?>