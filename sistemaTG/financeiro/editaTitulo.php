<!DOCTYPE html>

<html>
    
    <head>
        
        <?php
        include("../uteis/meta-link.php");
        include('conectaBanco.php');
        ?>

        <title> Tela de Edição de Títulos</title>

    </head>

    <body>

        <?php include("../uteis/navbar.php"); ?>

        <?php
        $codigo = $_GET['codigo'];

        $resultado1 = $conn->query("
SELECT
                t.idtitulo,
                t.descricao,
                t.dataEmissao,
                t.numeroDocumento,
                t.dataVencimento,
                t.valor,
                f.nome as nomeFornecedor,
                f.idfornecedor,
                p.idtipo,
                p.nome
FROM
	titulo AS t
INNER JOIN
	fornecedor AS f ON f.idfornecedor = t.forn_idfornecedor
INNER JOIN
	tipo AS p ON p.idtipo = t.tipo_idtipo
WHERE 
        t.idtitulo =$codigo");

        while ($row = $resultado1->fetch(PDO::FETCH_ASSOC)) {

            $idTitulo = $row['idtitulo'];
            $descricao = $row['descricao'];
            $dataEmissao = $row['dataEmissao'];
            $numeroDocumento = $row['numeroDocumento'];
            $dataVencimento = $row['dataVencimento'];
            $valor = $row['valor'];
            $nomeFornecedor = $row['nomeFornecedor'];
            $idFornecedor = $row['idfornecedor'];
            $idTipo = $row['idtipo'];
            $nome = $row['nome'];
        }

        $conn->connection = null;
        ?>

        <div class="cadtit col-xs-3 col-sm-3 col-lg-3">

            <h2 class="h2"> Edição de Títulos </h2>

        </div>

        <div>

            <form method="post" action="DAO/classes/trataEditaTitulo.php" enctype="multipart/form-data">

                <div class="col-xs-3 col-sm-3 col-lg-3">
                    <label for="desc"> Descrição </label>
                    <input type="text" id="desc" name="desc" class="form-control" value="<?php echo $descricao; ?>" >
                </div>

                <div class="col-xs-3 col-sm-3 col-lg-3">
                    <label for="dataEmissao"> Data da Emissão </label>
                    <input type="date" id="dataEmissao" name="dataEmissao"class="form-control" required value="<?php echo $dataEmissao; ?>">
                </div>	

                <div class="col-xs-3 col-sm-3 col-lg-3">
                    <label for="nDoc">N º do Documento</label>
                    <input type="number" id="nDoc" name="nDoc" class="form-control" value="<?php echo $numeroDocumento; ?>">
                </div>

                <div class="col-xs-3 col-sm-3 col-lg-3">
                    <label for="dataVcto"> Data Vencimento</label>
                    <input type="date" id="dataVcto" name="dataVcto" class="form-control" value="<?php echo $dataVencimento; ?>">
                </div>

                <div class="col-xs-3 col-sm-3 col-lg-3">
                    <label for="valor">Valor</label>
                    <input type="number" step="0.01" name="valor" id="valor" class="form-control" value="<?php echo $valor; ?>">
                </div>

                <div class="col-xs-3 col-sm-3 col-lg-3 labforn">
                    <label for="fornecedor"> Fornecedor</label>

                    <select name="fornecedor" id="fornecedor">

                        <option value="<?php echo $idFornecedor; ?>"> <?php echo $nomeFornecedor; ?></option>";

                            <?php
                            $resultados = $conn->query("SELECT idfornecedor , nome as novo_nome FROM fornecedor");

                            while ($row = $resultados->fetch(PDO::FETCH_ASSOC)) {

                                echo "<option value=" . $row['idfornecedor'] . ">" . $row['novo_nome'] . "</option>";
                            }

                            $conn->connection = null;
                            ?>

                    </select>

                </div>

                <div class="col-xs-3 col-sm-3 col-lg-3">
                    <label for="tipo"> Tipo </label>

                    <select name="tipo">

                        <option value="<?php echo $idTipo; ?>"> <?php echo $nome; ?></option>";

                            <?php
                            $resultados = $conn->query("SELECT idtipo , nome FROM tipo");

                            while ($row = $resultados->fetch(PDO::FETCH_ASSOC)) {


                                echo "<option value=" . $row['idtipo'] . ">" . $row['nome'] . "</option>";
                            }

                            $conn->connection = null;
                            ?>

                    </select>

                </div>
                
                <div class="col-xs-3 col-sm-3 col-lg-3">
                    <input type="text" id="codigo" name="codigo" class="form-control" hidden value="<?php echo $codigo; ?>" >
                </div>

                <div class="col-xs-3 col-sm-3 col-lg-3 botaoCad" >
                    <button type="submit" class="btn btn-dark-green"> Salvar </button>
                </div>

            </form>
         
        </div>

    </body>

</html>
