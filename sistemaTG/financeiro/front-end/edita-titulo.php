<!DOCTYPE html>

<html>
    
    <head>
        
        <?php
        include("../uteis/meta-link.php");
        include("../back-end/edita-titulo.php");
        include("../uteis/popula.php");
        ?>

        <title> Tela de Edição de Títulos</title>

    </head>

    <body>

        <?php include("../uteis/navbar.php"); ?>



        <div class="cadtit col-xs-3 col-sm-3 col-lg-3">

            <h2 class="h2"> Edição de Títulos </h2>

        </div>

        
            <form method="post" action="" enctype="multipart/form-data">

                <div class="form-group col-xs-3 col-sm-3 col-lg-3">
                    <label for="desc"> Descrição </label>
                    <input type="text" id="desc" name="desc" class="form-control" value="<?php echo $descricao; ?>" >
                </div>

                <div class="form-group col-xs-3 col-sm-3 col-lg-3">
                    <label for="dataEmissao"> Data da Emissão </label>
                    <input type="date" id="dataEmissao" name="dataEmissao"class="form-control" required value="<?php echo $dataEmissao; ?>">
                </div>	

                <div class="form-group col-xs-3 col-sm-3 col-lg-3">
                    <label for="nDoc">N º do Documento</label>
                    <input type="number" id="nDoc" name="nDoc" class="form-control" value="<?php echo $numeroDocumento; ?>">
                </div>

                <div class="form-group col-xs-3 col-sm-3 col-lg-3">
                    <label for="dataVcto"> Data Vencimento</label>
                    <input type="date" id="dataVcto" name="dataVcto" class="form-control" value="<?php echo $dataVencimento; ?>">
                </div>

                <div class="form-group col-xs-3 col-sm-3 col-lg-3">
                    <label for="valor">Valor</label>
                    <input type="number" step="0.01" name="valor" id="valor" class="form-control" value="<?php echo $valor; ?>">
                </div>

                <div class="form-group col-xs-3 col-sm-3 col-lg-3">
                    <label for="fornecedor"> Fornecedor</label>

                    <select class="form-control" name="fornecedor" id="fornecedor">

                        <option hidden value="<?php echo $idFornecedor; ?>"> <?php echo $nomeFornecedor; ?></option>";
                        
                        <?php populaFornecedor() ?>

                    </select>

                </div>

                <div class="form-group col-xs-3 col-sm-3 col-lg-3">
                    <label for="tipo"> Tipo </label>

                    <select class="form-control" name="tipo">
                        
                      <option hidden value="<?php echo $idTipo; ?>"> <?php echo $nome; ?></option>";  
                      
                      <?php populaTipo() ?>

                    </select>

                </div>
                
                <div class="col-xs-3 col-sm-3 col-lg-3">
                    <input type="text" id="codigo" name="codigo" class="form-control" hidden value="<?php echo $codigo; ?>" >
                </div>
                
                
                <div class="col-xs-3 col-sm-3 col-lg-3">
                    <input type="text" id="acao" name="acao" class="form-control" hidden value="1" >
                </div>
                

                <div class="col-xs-3 col-sm-3 col-lg-3">
                    <button type="submit" class="btn btn-dark-green" > Salvar </button>
                </div>

            </form>


    </body>

</html>
