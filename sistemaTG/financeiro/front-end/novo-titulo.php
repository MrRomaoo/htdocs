<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        
    <title> Cadastro de Titulo </title>

    <?php include("../uteis/meta-link.php");?>
    <?php include("../uteis/popula.php");?>
    
    </head>
<body>

    <?php include("../uteis/navbar.php"); ?>

    <div class="cadtit col-xs-3 col-sm-3 col-lg-3">

        <h2 class="h2"> Cadastro de Títulos </h2>

    </div>

    <div>

        <form method="post" action="../back-end/novo-titulo.php" enctype="multipart/form-data">

            <div class="col-xs-3 col-sm-3 col-lg-3">
                <label for="desc"> Descrição </label>
                <input type="text" id="desc" name="desc" class="form-control">
            </div>

            <div class="col-xs-3 col-sm-3 col-lg-3">
                <label for="dataEmissao"> Data da Emissão </label>
                <input type="date" id="dataEmissao" name="dataEmissao"class="form-control">
            </div>	

            <div class="col-xs-3 col-sm-3 col-lg-3">
                <label for="nDoc">N º do Documento</label>
                <input type="number" id="nDoc" name="nDoc" class="form-control">
            </div>

            <div class="col-xs-3 col-sm-3 col-lg-3">
                <label for="dataVcto"> Data Vencimento</label>
                <input type="date" id="dataVcto" name="dataVcto" class="form-control">
            </div>

            <div class="col-xs-3 col-sm-3 col-lg-3">
                <label for="valor">Valor</label>
                <input type="number" step="0.01" name="valor" id="valor" class="form-control">
            </div>

            <div class="form-group col-xs-3 col-sm-3 col-lg-3">
                    <label for="fornecedor"> Fornecedor</label>

                    <select class="form-control" name="fornecedor" id="fornecedor">

                                                
                        <?php populaFornecedor() ?>

                    </select>

                </div>

            <div class="form-group col-xs-3 col-sm-3 col-lg-3">


                <label for="tipo"> Tipo </label>

                <select class="form-control" name="tipo">

                    <?php populaTipo(); ?>
                    
                </select>

            </div>

            <div class="col-xs-3 col-sm-3 col-lg-3 botaoCad" >

                <button type="submit" class="btn btn-dark-green"> Cadastrar </button>

            </div>

        </form>

    </div>

</body>

</html>
