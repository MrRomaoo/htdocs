<?php
include('conecta-banco.php');
$recebeCodPublicacao = $_GET['cod_publicacao'];

$cookie_name = str_replace(".", "", $_SERVER['REMOTE_ADDR']).'-'.$recebeCodPublicacao;
$cookie_value = str_replace(".", "", $_SERVER['REMOTE_ADDR']).'-'.$recebeCodPublicacao;

if (!isset($_COOKIE[$cookie_name])) {

    setcookie($cookie_name, $cookie_name, time() + (86400 * 1), "/");

    $sql = "UPDATE tbl_publicacoes
                  SET 
                  visualizacoes = (visualizacoes+1)
                  WHERE 
                  cod_publicacao=$recebeCodPublicacao;";

    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));

    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $stmt = $conn->prepare($sql);
    $stmt->execute();
}
?>

<html>

    <head>

        <?php
        include("head.html");

        include('mensagem-modal.php');

        $pasta_imagem = "imagem-upload-publicacoes/";
        $pasta_imagem_perfil = "imagem-upload-perfil/";
        $pasta_video_upload_publicacoes = "video-upload-publicacoes/";

        //1° Momento: faço a query que alimenta os dados da publicação
        $sql = $conn->query("SELECT
                                                    cod_publicacao,
                                                    SUM(tbl_curtidas.gostei) AS gostei,
                                                    SUM(tbl_curtidas.nao_gostei) AS nao_gostei,
                                                    tbl_publicacoes.titulo_negocio,
                                                    tbl_prestador.titulo_negocio AS titulo_negocio_perfil,
                                                       tbl_publicacoes.cod_prestador_publicacao,
                                                       tbl_publicacoes.descricao,
                                                       tbl_publicacoes.faixa_etaria,
                                                       tbl_publicacoes.rua,
                                                       tbl_publicacoes.numero,
                                                       tbl_publicacoes.complemento,
                                                      tbl_publicacoes. bairro,
                                                      tbl_publicacoes.cidade,
                                                       tbl_publicacoes.estado,
                                                       tbl_publicacoes.cep, 
                                                       tbl_publicacoes.pais,
                                                       tbl_publicacoes.referencia,
                                                       tbl_publicacoes.contato,
                                                       tbl_publicacoes.data_do_evento,
                                                       tbl_publicacoes.horario_do_evento,
                                                       tbl_publicacoes.categoria_publicacao,
                                                       tbl_publicacoes.valor,
                                                       tbl_publicacoes.valor_homem,
                                                       tbl_publicacoes.valor_mulher,
                                                       tbl_publicacoes.data_hora_publicacao,
                                                       tbl_publicacoes.link_imagem,
                                                       tbl_publicacoes.tipo_imagem,
                                                       tbl_publicacoes.link_video,
                                                       tbl_publicacoes.tipo_video,
                                                       tbl_publicacoes.visualizacoes
                                                        FROM
                                                            tbl_publicacoes
                                                                INNER JOIN 
                                                                    tbl_curtidas ON tbl_curtidas.tbl_publicacoes_cod_publicacao=tbl_publicacoes.cod_publicacao
                                                                       INNER JOIN 
                                                                       tbl_prestador ON tbl_prestador.cod_prestador =  tbl_publicacoes.cod_prestador_publicacao
                                                                    WHERE
                                                                        cod_publicacao = $recebeCodPublicacao
                                                                            ORDER BY
                                                                                data_hora_publicacao ASC");

        while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
            $recebeTituloNegocio = $row['titulo_negocio'];
            $recebeTituloNegocioPerfil = $row['titulo_negocio_perfil'];
            $recebeCodigoPrestadorPublicacao = $row['cod_prestador_publicacao'];
            $recebeDescricao = $row['descricao'];
            $recebeFaixaEtaria = $row['faixa_etaria'];
            $recebeRua = $row['rua'];
            $recebeNumero = $row['numero'];
            $recebeComplemento = $row['complemento'];
            $recebeBairro = $row['bairro'];
            $recebeCidade = $row['cidade'];
            $recebeEstado = $row['estado'];
            $recebeCep = $row['cep'];
            $recebePais = $row['pais'];
            $recebeReferencia = $row['referencia'];
            $recebeContato = $row['contato'];
            $recebeDataDoEvento = $row['data_do_evento'];
            $recebeHorarioDoEvento = $row['horario_do_evento'];
            $recebeCategoriaPublicacao = $row['categoria_publicacao'];
            $recebeValor = $row['valor'];
            $recebeValorHomem = $row['valor_homem'];
            $recebeValorMulher = $row['valor_mulher'];
            $recebeDataHoraPublicacao = $row['data_hora_publicacao'];
            $recebeLinkImagem = $row['link_imagem'];
            $recebeTipoImagem = $row['tipo_imagem'];
            $recebeLinkVideo = $row['link_video'];
            $recebeTipoVideo = $row['tipo_video'];
            $recebeVisualizacoes = $row['visualizacoes'];
            $recebeGostei = $row['gostei'];
            $recebeNaoGostei = $row['nao_gostei'];
        }

        $sql->connection = null;
        $conn->connection = null;
        //(FIM) 1° Momento: faço a query que alimenta os dados da publicação
        //2° Momento: faço a query que traz as informações do usuário
        $sql = $conn->query("SELECT 
					cod_prestador,
                                                                                                        titulo_negocio, 
                                                                                                        titulacao,
                                                                                                        link_imagem, 
                                                                                                        tipo_imagem
                                                                                                                             FROM 
                                                                                                                            tbl_prestador
                                                                                                                                WHERE 
                                                                                                                                cod_prestador = $recebeCodigoPrestadorPublicacao");

        while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
            $codigoPrestadorPerfil = $row['cod_prestador'];
            $tituloPrestador = $row['titulo_negocio'];
            $linkImagemPerfil = $row['link_imagem'];
            $tipoImagemPerfil = $row['tipo_imagem'];
            $recebeTitulacao = $row['titulacao'];
        }

        $sql->connection = null;
        $conn->connection = null;
        //(FIM) 2° Momento: faço a query que traz as informações do usuário
        //Seto os ccokies de visualizações aqui
        ?>

    </head>

    <body>

        <?php include("body-nav-bar.php"); ?>

        <div class="container quebra_linha">

            <input type="hidden" class="form-control" id="cod_prestador" name="cod_prestador" value="<?php echo $_SESSION['cod_prestador']; ?>"> 
            <input type="hidden" class="form-control" id="cod_publicacao" name="cod_publicacao" value="<?php echo $recebeCodPublicacao; ?>"> 

            <div class="row" width="100%">		
                <div class="col-lg-6">
                    <h3 class="teal-tex" ><?php echo $recebeTituloNegocio ?></h3>
                </div>
            </div>

            <div class="lds-facebook">

            </div>

            <div class="row">

                <div class="col-lg-8">
                    <!--<p><img class="img-fluid rounded mb-4" src=<?php //echo $pasta_imagem . $recebeLinkImagem. $recebeTipoImagem     ?> height="300" width="300" alt="" class="img-fluid rounded mb-4"></p>-->

                    <?php
                    $recebePastaImagemFaixaEtaria;

                    switch ($recebeFaixaEtaria) {
                        case $recebeFaixaEtaria == 'Livre':
                            $recebePastaImagemFaixaEtaria = 'imagem-site/classificacao-livre.png';
                            break;
                        case $recebeFaixaEtaria == '10 Anos':
                            $recebePastaImagemFaixaEtaria = 'imagem-site/classificacao-10.png';
                            break;
                        case $recebeFaixaEtaria == '12 Anos':
                            $recebePastaImagemFaixaEtaria = 'imagem-site/classificacao-12.png';
                            break;
                        case $recebeFaixaEtaria == '14 Anos';
                            $recebePastaImagemFaixaEtaria = 'imagem-site/classificacao-14.png';
                            break;
                        case $recebeFaixaEtaria == '16 Anos';
                            $recebePastaImagemFaixaEtaria = 'imagem-site/classificacao-16.png';
                            break;
                        case $recebeFaixaEtaria == '18 Anos';
                            $recebePastaImagemFaixaEtaria = 'imagem-site/classificacao-18.png';
                            break;
                    }
                    ?> 
                    <!--Imagem referende a Classificação escolhida-->
                    <div class="control-group form-group" id="div_img_faixa_etaria">
                        <div class="controls">
                            Classificação: <img alt="" id="img_faixa_etaria" src="<?php echo $recebePastaImagemFaixaEtaria ?>" name="img_faixa_etaria" border="3" height="50" width="50" class="rounded-circle" /> 
                        </div>
                    </div>
                    <!--(FIM) Imagem referente a Classificação escolhida-->
                       <!--<button type="button" id="btn_nao_gostei" name="btn_nao_gostei" class="btn btn-white px-3 btn-outline-danger waves-effect"><i class="fa fa-thumbs-o-down fa-2x" aria-hidden="true"></i></button><?php echo " " . $recebeNaoGostei ?>-->

                    <?php
                    if ($recebeLinkVideo != "") {
                        $styleVideoAtual = '';
                        
                        if($recebeTipoVideo == ""){
                            $pasta_video_upload_publicacoes=null;
                        }
                        
                    } else {
                        $styleVideoAtual = 'style="display:none"';
                    }
                    ?>
         
                    <div class="control-group form-group"   <?php echo $styleVideoAtual?> id="div_video_atual" name="div_video_atual">
                        <div class="controls">
                            <div class="embed-responsive embed-responsive-16by9 z-depth-4">
                                <iframe  class="embed-responsive-item"  id="video_atual" name="video_atual" border="3" src='<?php echo $pasta_video_upload_publicacoes.$recebeLinkVideo.$recebeTipoVideo ?>' width="600" height="450" allowfullscreen> </iframe>
                            </div>
                        </div>
                    </div>

                    

                    
                    
                    
                    
                    
                    

                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    

                    <div class="control-group form-group">
                        <div class="controls">
                            <p><?php echo $recebeVisualizacoes . " Visualizações" ?></p>
                        </div>
                    </div>

                    <!--Este bloco é responsável por deixar somente usuários logados curtirem as publicações-->
                    <?php
                    if (
                            $_SESSION['cod_prestador'] != '' &&
                            $_SESSION['prestador'] != '' &&
                            $_SESSION['senha'] != '') {
                        $desabilita_btn_curtir = '';
                        $mensagem_logado = '';

                        echo $mensagem_logado;
                    } else {
                        $desabilita_btn_curtir = 'disabled';
                        $mensagem_logado = '<p class="text-danger">É preciso estar logado para poder curtir as publicações</p>';

                        echo $mensagem_logado;
                    }
                    ?>

                    <div id="div_curtidas" name="div_curtidas" class="">
                        <button type="button"  <?php echo $desabilita_btn_curtir ?> id="btn_gostei" name="btn_gostei" class="btn btn-white px-3 btn-outline-primary waves-effect" onclick="gostei(<?php echo $_SESSION['cod_prestador']; ?>,<?php echo $recebeCodPublicacao; ?>,<?php echo $recebeCodigoPrestadorPublicacao; ?>)"><i class="fa fa-thumbs-o-up fa-2x" aria-hidden="true"></i><div id="div_gostei"><?php echo "   " . $recebeGostei ?></div></button>
                        <br>
                        <button type="button" <?php echo $desabilita_btn_curtir ?> id="btn_nao_gostei" name="btn_nao_gostei" class="btn btn-white px-3 btn-outline-danger waves-effect" onclick="naoGostei(<?php echo $_SESSION['cod_prestador']; ?>,<?php echo $recebeCodPublicacao; ?>,<?php echo $recebeCodigoPrestadorPublicacao; ?>)"><i class="fa fa-thumbs-o-down fa-2x" aria-hidden="true"></i><div id="div_nao_gostei"><?php echo "   " . $recebeNaoGostei ?></div></button>
                    </div>

                    <?php
                    if ($recebeRua != "" && 
                            $recebeNumero != "" && 
                            $recebeComplemento != "" && 
                            $recebeBairro != "" && 
                            $recebeRua != "" && 
                            $recebeCidade!= "" && 
                            $recebeEstado != "" && 
                            $recebeCep != "" && 
                            $recebePais != "" && 
                            $recebeReferencia != "") {
                        $styleEnderecoGoogle = '';
                    } else {
                        $styleEnderecoGoogle = 'display:none';
                    }
                    ?>
                    
                    <div id="endereco" name="endereco" style="<?php echo $styleEnderecoGoogle?>">
                        <div class="control-group form-group">
                            <div class="controls">
                                 <iframe width="730" height="450" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?q=<?php echo $recebeRua . ', ' . $recebeNumero . ', ' . $recebeComplemento . ', ' . $recebeBairro . ', ' . $recebeCidade . ', ' . $recebeEstado . ', ' . $recebeCep . ', ' . $recebePais . ', ' . $recebeReferencia ?>&key=AIzaSyCGWT2ufkFyoztTYn92bFRFFBXzypOYxBo" allowfullscreen></iframe>
                            </div>							
                        </div>
                    </div>        

                </div>

                <div class="col-lg-4">

                    Publicado por: <b><a class="primary-text" href="perfil.php?cod_prestador=<?php echo $recebeCodigoPrestadorPublicacao ?>"> <?php echo $recebeTituloNegocioPerfil ?></a></b>
                    <h2><a href="perfil.php?cod_prestador=<?php echo $recebeCodigoPrestadorPublicacao ?>"><img  class="rounded-circle" height="70" width="70" src="<?php echo $pasta_imagem_perfil . $linkImagemPerfil . $tipoImagemPerfil ?>" width="50" height="50"></a></h2>

<?php /* if ($recebeTitulacao == 'D') { */ ?>
                    <label class="text-diamond-carrossel"><b><?php /* echo "Membro Diamante" */ ?></b></label>
                    <?php /* } */ ?>

                    <?php /* if ($recebeTitulacao == 'O') { */ ?>
                    <label class="text-gold"><b><?php /* echo "Membro Ouro" */ ?></b></label>
<?php /* } */ ?>

                    <?php /* if ($recebeTitulacao == 'S') { */ ?>
                    <label ><?php /* echo "Membro comum" */ ?></label>
                    <?php /* } */ ?>

                    <br>

                    <a  href="perfil.php?cod_prestador=<?php echo $recebeCodigoPrestadorPublicacao ?>"  class="btn btn-sm btn-outline-primary waves-effect px-2"><i class="fa fa-eye" aria-hidden="true"></i> Perfil</a>

                    </br></br>

                    <?php if (trim($recebeRua) != '') { ?>
                        <p><b>Endereço: </b>
                    <?php if ($recebeRua != '') {
                        echo $recebeRua . ', ';
                    } 
                    if ($recebeNumero != '') {
                        echo $recebeNumero . ', ';
                    } 
                    if ($recebeComplemento != '') {
                        $recebeComplemento . ', ';
                    } 
                    if ($recebeBairro != '') {
                        echo $recebeBairro . ', ';
                    } 
                    if ($recebeCidade != '') {
                        echo $recebeCidade . ', ';
                    } 
                    if ($recebeEstado != '') {
                        echo $recebeEstado . ', ';
                    } 
                    if ($recebeCep != '') {
                        echo $recebeCep . ', ';
                    } 
                    if ($recebePais != '') {
                        echo $recebePais . ', ';
                    } 
                    if ($recebeReferencia != '') {
                        echo $recebeReferencia;
                    } 
                    ?></p>
                    <?php } ?>

                    <p><b>Categoria: </b><?php echo $recebeCategoriaPublicacao ?></p>

                    <?php if (trim($recebeDescricao) != '') { ?>
                        <p><b>Descrição: </b><?php echo $recebeDescricao ?></p>
                    <?php } ?>

                    <?php if (trim($recebeContato) != '') { ?>
                        <p><b>Contato: </b><?php echo $recebeContato ?></p>
                    <?php } ?>

                    <?php
                    // Tratamento do valor
                    if ($recebeValorHomem != "" && $recebeValorMulher != "") {
                        echo '<p><i class="fa fa-male fa-2x indigo-text" aria-hidden="true"></i> ' . $recebeValorHomem . '     ' . '<i class="fa fa-female fa-2x red-text" aria-hidden="true"></i> ' . $recebeValorMulher . '</p>';
                    } else {
                        
                        if ($recebeValorHomem == "" && $recebeValorMulher != "") {
                            echo '<p> <i class="fa fa-male fa-2x indigo-text" aria-hidden="true"></i> ' . 'Grátis' . '     ' . '<i class="fa fa-female fa-2x red-text" aria-hidden="true"></i> ' . $recebeValorMulher . '</p>';
                            
                        } else {

                            if ($recebeValorHomem != "" && $recebeValorMulher = "") {
                                echo '</p><i class="fa fa-male fa-2x indigo-text" aria-hidden="true"></i> ' . $recebeValorHomem . '     ' . '<i class="fa fa-female fa-2x red-text" aria-hidden="true"></i> ' . 'Grátis' . '</p>';
                            } else {
                                echo "<h6>" . "R$ " . $recebeValor . "</h6>";
                            }
                            
                        }
                    }
                    ?>

                    <p><b>Data da publicação: </b><?php echo $recebeDataHoraPublicacao ?></p>

                    <div class="control-group form-group">
                        <div class="controls">
                            <button  id="btn_denuncia" name="btn_denuncia" class="btn btn-sm btn-danger waves-effect px-2r" onclick="exibeModalDenuncia()"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Denunciar publicação</button>
                        </div>
                    </div>

                </div>

            </div>

        </div>

        <footer>
            <br>
            <br>
            <br>

<?php include("footer-sem-trava-undo.html"); ?>

            <script>

                function exibeModalDenuncia() {
                    $('#exampleModalDenuncia').modal().withTimeout(1000);
                }

                function enviaEmailModal() {

                    var nome = document.getElementById("cod_prestador").value;
                    var cod_publicacao = document.getElementById("cod_publicacao").value;
                    var assunto = 'Denuncia';
                    var mensagem = document.getElementById("mensagemDenuncia").value;

                    window.location.href = 'email-denuncia.php?cod_publicacao=' + cod_publicacao + '&nome=' + nome + '&' + 'assunto=' + assunto + '&mensagem=' + mensagem;
                    //header('Location:email-denunca.php?nome='+nome+'&'+'assunto='+assunto+'&mensagem='+mensagem);
                }

            </script>

            <script src="../baile-de-favela/assets/mdb-table-pagination/js/curtidas.js"></script>

        </footer>

    </body>