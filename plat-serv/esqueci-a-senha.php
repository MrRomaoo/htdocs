
<html>
    <head>

        <?php
        include("head.html");
        require_once('conecta-banco.php');
        include("mensagem-modal-php.php");
        ?>
        
        <?php
        
        if ($_SERVER["REQUEST_METHOD"] == "POST") {

            if (!empty($_POST["email"])) {

                $email = "";

                $email = $_POST["email"];

                $sql = $conn->query("SELECT prestador, senha, data_cadastro
						FROM tbl_prestador
						WHERE email='$email'");

                if ($sql->rowCount()>0) {

                    while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {

                        $recebePrestador = $row['prestador'];
                        $recebeSenha = $row['senha'];
                        $recebeDataCadastri = $row['data_cadastro'];
                        
                    }

                        $to = "$email";
                        $subject = "Gente cultura";
                        $txt = "Conseguimos encontrar a sua senha e usuário!\n"
                             . "Usuário: $recebePrestador \n" 
                             . "Esta é a sua senha: $recebeSenha \n\n"
                             . "Qualquer coisa estamos aí!";
                        
                        $headers = "From: gentecultura.com" . "\r\n";

                        mail($to, $subject, $txt, $headers);
                        
                        $sql->connection=null;
                        $conn->connection=null;
                        
                        mensagemModalPhp("Info", "E-mail enviado");


                } else {

                    mensagemModalPhp("Atenção", "E-mail não cadastrado ou usuário não existe");
                    
                }
                
            } else {

                   mensagemModalPhp("Atenção", "Campo de E-mail vazio");
                   
            }
            
        }
        
        ?>

    </head>

    <body>

       <?php include("body-nav-bar.php");?>

        <span style="display:block; height: 100px;"></span>

        <div class="container quebra_linha">

            <div class="row">
                <div class="col-lg-8 mb-4">

                    <form name="formLogin" id="formLogin" method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">

                        <div class="control-group form-group">
                            <div class="controls">
                                <label>Digite o e-mail cadastrado, vamos enviar seu usuário e senha.</label>
                                <label>Fique atento(a), pois o email pode chegar em seu lixo eletrônico.</label>
                                <input type="email" class="form-control" id="email" name="email" required>
                                <p class="help-block"></p>
                            </div>
                        </div>

                        <div id="success"></div>

                        <button type="submit" class="btn btn-sm btn-brown"><i class="fa fa-send-o" aria-hidden="true"></i> Enviar</button>

                        <span style="display:block; height: 250px;"></span>

                    </form> 
                    
                </div>

            </div>

        </div>

<footer>
<?php include("footer.html"); ?>
</footer>

</body>