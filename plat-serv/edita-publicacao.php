<?php
include('verifica-session.php');
?>

<html>

    <head>

        <?php
        include("head.html");
        include('conecta-banco.php');
        include("mensagem-modal-php.php");
        include("mensagem-modal.php");

        $pasta_imagem = "imagem-upload-publicacoes/";
        $pasta_video = 'video-upload-publicacoes/';

        $recebeCodPublicacao = $_GET['cod_publicacao'];

        //Preenche os inputs
        $sql = $conn->query("SELECT 
                                         cod_publicacao,
                                         titulo_negocio, 
                                        cod_prestador_publicacao,
                                        descricao,
                                        faixa_etaria,
                                        rua,
                                        numero,
                                        complemento,
                                        bairro,
                                        cidade,
                                        estado,
                                        cep,
                                        pais,
                                        referencia,
                                        contato,
                                        data_do_evento,
                                        horario_do_evento,
                                        categoria_publicacao, 
                                        valor,
                                        valor_homem,
                                        valor_mulher,
                                        data_hora_publicacao, 
                                        link_imagem,
                                        tipo_imagem,
                                        link_video,
                                        tipo_video
                                                                    FROM 
                                                                         tbl_publicacoes
                                                                            WHERE 
                                                                            cod_publicacao = $recebeCodPublicacao;");


        if ($sql) {

            while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {

                $recebeTituloNegocio = $row['titulo_negocio'];
                $recebeCodPrestadorPublicacao = $row['cod_prestador_publicacao'];
                $recebeDescricao = $row['descricao'];
                $recebeFaixaEtaria = $row['faixa_etaria'];
                $recebeRua = $row['rua'];
                $recebeNumero = $row['numero'];
                $recebeComplemento = $row['complemento'];
                $recebeBairro = $row['bairro'];
                $recebeCidade = $row['cidade'];
                $recebeEstado = $row['estado'];
                $recebeCep = $row['cep'];
                $recebePais = $row['pais'];
                $recebeReferencia = $row['referencia'];
                $recebeContato = $row['contato'];
                $recebeDataDoEvento = $row['data_do_evento'];
                $recebeHorarioDoEvento = $row['horario_do_evento'];
                $recebeCategoriaPublicacao = $row['categoria_publicacao'];
                $recebeValor = $row['valor'];
                $recebeValorHomem = $row['valor_homem'];
                $recebeValorMulher = $row['valor_mulher'];
                $recebeLinkImagem = $row['link_imagem'];
                $recebeTipoImagem = $row['tipo_imagem'];
                $recebeLinkVideo = $row['link_video'];
                $recebeTipoVideo = $row['tipo_video'];

                //Vou receber aqui o endereço completo da pasta do video upado, pois preciso para carregar o ´frame
            }
        } else {
            mensagemModalPhp('Atenção', 'Houve um erro, tente novamente.');
        }
        ?>

    </head>

    <body>

        <?php include("body-nav-bar.php"); ?>

        <div class="container quebra_linha">

            <span style="display:block; height: 15px;"></span>
            <span style="display:block; weight: 5px;"></span>

            <div class="row">
                <div class="col-lg-8 mb-4">
                    <h3 class="teal-text">Altere ou exclua sua publicação</h3>

                    <form name="sentMessage" id="contactForm"  method="post" action="confirma-edita-publicacao.php" enctype="multipart/form-data">

                        <input type="hidden" class="form-control" id="cod_usu_publicacao" name="cod_prestador_publicacao" value="<?php echo $recebeCodPrestadorPublicacao; ?>">
                        <input type="hidden" class="form-control" id="cod_publicacao" name="cod_publicacao" value="<?php echo $recebeCodPublicacao; ?>">
                        <input type="hidden" class="form-control" id="tipo_imagem_post" name="tipo_imagem_post" value="<?php echo $recebeTipoImagem; ?>">
                        <input type="hidden" class="form-control" id="pasta_video" name="pasta_video"  value="<?php echo $pasta_video; ?>">
                        <input type="hidden" class="form-control" id="link_video" name="link_video"  value="<?php echo $recebeLinkVideo; ?>">
                        <input type="hidden" class="form-control" id="tipo_video" name="tipo_video"  value="<?php echo $recebeTipoVideo; ?>">

                        <div class="control-group form-group">
                            <div class="controls">
                                <label>Campos obrigatórios possuem: <span style="color:red;font-weight:bold">*</span></label>
                            </div>
                        </div>

                        <!--ítulo-->
                        <div class="control-group form-group">
                            <div class="controls">
                                <label class="blue-text font-weight-bold">Título da publicação: <span style="color:red;font-weight:bold">*</span></label>
                                <input type="text" class="form-control" name="titulo_negocio" id="titulo_negocio" value="<?php echo $recebeTituloNegocio; ?>" maxlength="70" required>
                                <p class="help-block"></p>
                            </div>
                        </div>
                        <!--(FIM) Titulo-->

                        <br>

                        <!-- Categoria-->
                        <div class="control-group form-group">
                            <div class="controls">
                                <label class="blue-text font-weight-bold" >Categoria: <span style="color:red;font-weight:bold">*</span></label>
                                <select class="form-control" id="categoria_publicacao" name="categoria_publicacao" required>
                                    <option><?php echo $recebeCategoriaPublicacao; ?></option>
                                    <?php
                                    include("categorias.php");
                                    ?>
                                </select>	
                            </div>							
                        </div>
                        <!--(FIM) Categoria-->

                        <br>

                        <!-- Classificação-->
                        <div class="control-group form-group">
                            <div class="controls">
                                <label class="blue-text font-weight-bold" >Classificação do evento: <span style="color:red;font-weight:bold">*</span> </label>
                                <select class="form-control" id="faixa_etaria" name="faixa_etaria">
                                    <option><?php echo $recebeFaixaEtaria; ?></option>
                                    <?php
                                    include("classificacao.php");
                                    ?>
                                </select>	
                            </div>							
                        </div>
                        <!--(FIM) Classificação-->

                        <?php
                        $recebePastaImagemFaixaEtaria;

                        switch ($recebeFaixaEtaria) {
                            case $recebeFaixaEtaria == 'Livre':
                                $recebePastaImagemFaixaEtaria = 'imagem-site/classificacao-livre.png';
                                break;
                            case $recebeFaixaEtaria == '10 Anos':
                                $recebePastaImagemFaixaEtaria = 'imagem-site/classificacao-10.png';
                                break;
                            case $recebeFaixaEtaria == '12 Anos':
                                $recebePastaImagemFaixaEtaria = 'imagem-site/classificacao-12.png';
                                break;
                            case $recebeFaixaEtaria == '14 Anos';
                                $recebePastaImagemFaixaEtaria = 'imagem-site/classificacao-14.png';
                                break;
                            case $recebeFaixaEtaria == '16 Anos';
                                $recebePastaImagemFaixaEtaria = 'imagem-site/classificacao-16.png';
                                break;
                            case $recebeFaixaEtaria == '18 Anos';
                                $recebePastaImagemFaixaEtaria = 'imagem-site/classificacao-18.png';
                                break;
                        }
                        ?> 
                        <!--Imagem referende a Classificação escolhida-->
                        <div class="control-group form-group" id="div_img_faixa_etaria">
                            <div class="controls">
                                Classificação: <img alt="" id="img_faixa_etaria" src="<?php echo $recebePastaImagemFaixaEtaria ?>" name="img_faixa_etaria" border="3" height="50" width="50" class="rounded-circle" /> 
                            </div>
                        </div>
                        <!--(FIM) Imagem referende a Classificação escolhida-->

                        <br>

                        <label class="blue-text font-weight-bold" >Valor do evento</label>     

                        <br>

                        <!-- Radio buttons responsáveis pela JQuery do valor-->
                        <div class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input" name="valor_radio" value="valor_fixo" id="valor_fixo" >
                            <label class="custom-control-label" for="valor_fixo">O mesmo valor para todos os sexos</label>
                        </div>

                        <div class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input" name="valor_radio" value="valor_diferenciado" id="valor_diferenciado">
                            <label class="custom-control-label" for="valor_diferenciado">Valor diferenciado para os sexos</label>
                        </div>

                        <div class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input" name="valor_radio" value="Valor a combinar" id="valor_combinar">
                            <label class="custom-control-label" for="valor_combinar">Valor a combinar</label>
                        </div>

                        <div class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input" name="valor_radio" value="Grátis" id="valor_gratis">
                            <label class="custom-control-label" for="valor_gratis">Grátis</label>
                        </div>

                        <br>

                        <?php
                        if (trim($recebeValor) != "") {
                            $displayValor = '';
                        } else {
                            $displayValor = 'style="display: none"';
                        }
                        ?>
                        <!-- Este bloco aparece quando o valor fixo é selecionado.-->
                        <div name="valor_fixo_div" id="valor_fixo_div" class="control-group form-group" <?php echo $displayValor ?>>
                            <div class="controls">
                                <label>Valor(R$) </label>
                                <input type="number" class="form-control" id="valor" name="valor" value="<?php echo $recebeValor; ?>" maxlength="10">
                            </div>
                        </div>
                        <!-- (FIM) Este bloco aparece quando o valor fixo é selecionado.-->

                        <!-- Este bloco aparece quando o valor diferenciado é selecionado-->
                        <?php
                        if (trim($recebeValorHomem) != "" || trim($recebeValorHomem) != "") {
                            $displayValorDiferenciado = '';
                        } else {
                            $displayValorDiferenciado = 'style="display: none"';
                        }
                        ?>
                        <div name="valor_diferenciado_div" id="valor_diferenciado_div" class="control-group form-group" <?php echo $displayValorDiferenciado ?>>
                            <!-- Grid row -->
                            <div class="row">
                                <!-- Grid column -->
                                <div class="col">
                                    <!-- Default input -->
                                    <i class="fa fa-female fa-3x red-text" aria-hidden="true"></i>
                                    <input type="number" class="form-control" id="valor_mulher" name="valor_mulher" placeholder="Mulher" value="<?php echo $recebeValorMulher; ?>" maxlength="10">
                                </div>
                                <!-- Grid column -->

                                <!-- Grid column -->
                                <div class="col">
                                    <!-- Default input -->
                                    <i class="fa fa-male fa-3x blue-text" aria-hidden="true"></i>
                                    <input type="number" class="form-control" id="valor_homem" name="valor_homem" placeholder="Homem" value="<?php echo $recebeValorHomem; ?>" maxlength="10">
                                </div>
                                <!-- Grid column -->
                            </div>
                            <!-- Grid row -->
                        </div>
                        <!-- Default form grid -->
                        <!-- (FIM) Este bloco aparece quando o valor diferenciado é selecionado-->

                        <br>

                        <!-- Endereço-->
                        <label class="blue-text font-weight-bold" >Endereço do  seu evento </label>
                        <label class="red-text" >Atenção, preencha os campos do endereço, pois assim o mapa do Google vai ser gerado para o seu cliente! </label>

                        <!-- Rua-->
                        <div class="control-group form-group">
                            <div class="controls">
                                <label class="blue-text font-weight-bold" > Rua </label>
                                <input type="text" class="form-control" id="rua" name="rua" onkeypress="return carregaMapaGoogleEnter(event)"  onfocusout="carregaMapaGoogle()" value="<?php echo $recebeRua ?>" maxlength="200">                          
                            </div>							
                        </div>
                        <!--(FIM)  Rua-->

                        <!-- Número-->
                        <div class="control-group form-group">
                            <div class="controls">
                                <label class="blue-text font-weight-bold" > Número </label>
                                <input type="text" class="form-control" id="numero" name="numero" onkeypress="return carregaMapaGoogleEnter(event)"  onfocusout="carregaMapaGoogle()" value="<?php echo $recebeNumero ?>" maxlength="20">                          
                            </div>							
                        </div>
                        <!--(FIM)  Número-->

                        <!-- Complemento-->
                        <div class="control-group form-group">
                            <div class="controls">
                                <label class="blue-text font-weight-bold" > Complemento (Quadra, lote etc.)</label>
                                <input type="text" class="form-control" id="complemento" name="complemento" onkeypress="return carregaMapaGoogleEnter(event)"  onfocusout="carregaMapaGoogle()" value="<?php echo $recebeComplemento ?>" maxlength="200">                          
                            </div>							
                        </div>
                        <!--(FIM)  Complemento-->

                        <!-- Bairro-->
                        <div class="control-group form-group">
                            <div class="controls">
                                <label class="blue-text font-weight-bold" > Bairro </label>
                                <input type="text" class="form-control" id="bairro" name="bairro" onkeypress="return carregaMapaGoogleEnter(event)"  onfocusout="carregaMapaGoogle()"  value="<?php echo $recebeBairro ?>" maxlength="100">                          
                            </div>							
                        </div>
                        <!--(FIM)  Bairro-->

                        <!-- Cidade-->
                        <div class="control-group form-group">
                            <div class="controls">
                                <label class="blue-text font-weight-bold" > Cidade </label>
                                <input type="text" class="form-control" id="cidade" name="cidade" onkeypress="return carregaMapaGoogleEnter(event)"  onfocusout="carregaMapaGoogle()"  value="<?php echo $recebeCidade ?>" maxlength="100">                          
                            </div>							
                        </div>
                        <!--(FIM)  Cidade-->

                        <!-- Estado-->
                        <div class="control-group form-group">
                            <div class="controls">
                                <label class="blue-text font-weight-bold" > Estado </label>
                                <input type="text" class="form-control" id="estado" name="estado" onkeypress="return carregaMapaGoogleEnter(event)"  onfocusout="carregaMapaGoogle()"  value="<?php echo $recebeEstado ?>" maxlength="100">                          
                            </div>							
                        </div>
                        <!--(FIM)  Estado-->

                        <!-- CEP-->
                        <div class="control-group form-group">
                            <div class="controls">
                                <label class="blue-text font-weight-bold" > CEP </label>
                                <input type="text" class="form-control" id="cep" name="cep" onkeypress="return carregaMapaGoogleEnter(event)"  onfocusout="carregaMapaGoogle()"  value="<?php echo $recebeCep ?>" maxlength="100">                          
                            </div>							
                        </div>
                        <!--(FIM)  CEP-->

                        <!-- Pais-->
                        <div class="control-group form-group">
                            <div class="controls">
                                <label class="blue-text font-weight-bold" > Pais </label>
                                <input type="text" class="form-control" id="pais" name="pais" onkeypress="return carregaMapaGoogleEnter(event)"  onfocusout="carregaMapaGoogle()"  value="<?php echo $recebePais ?>" maxlength="60">                          
                            </div>							
                        </div>
                        <!--(FIM)  Pais-->

                        <!-- Referência-->
                        <div class="control-group form-group">
                            <div class="controls">
                                <label class="blue-text font-weight-bold" > Referência </label>
                                <input type="text" class="form-control" id="referencia" name="referencia" onkeypress="return carregaMapaGoogleEnter(event)"  onfocusout="carregaMapaGoogle()"  value="<?php echo $recebeReferencia ?>" maxlength="150">                          
                            </div>							
                        </div>
                        <!--(FIM)  Referência-->

                        <!--(FIM) Endereço-->


                        <!--Tratamento do endereço que aguarda o AJAX- para trazer o mapa do Google -->
                        <div id="endereco" name="endereco">

                        </div>                
                        <!--(FIM) Tratamento do endereço que aguarda o AJAX para trazer o mapa do Google-->

                        <br>

                        <!-- Contato-->
                        <div class="control-group form-group">
                            <div class="controls">
                                <label class="blue-text font-weight-bold" >Forma de contato, exemplo: Telefone, email, site etc.</label>
                                <input type="text" class="form-control" id="contato" name="contato" value="<?php echo $recebeContato ?>" maxlength="100">
                            </div>
                        </div>
                        <!--(FIM) Contato-->

                        <br>

                        <!-- Data do evento-->
                        <div class="control-group form-group">
                            <div class="controls">
                                <label class="blue-text font-weight-bold" >Data do evento.</label>
                                <input type="date" class="form-control" id="data_do_evento" name="data_do_evento" value="<?php echo $recebeDataDoEvento ?>" maxlength="20">
                            </div>
                        </div>
                        <!--(FIM) Data do evento-->

                        <br>

                        <!-- Horário do evento-->
                        <div class="control-group form-group">
                            <div class="controls">
                                <label class="blue-text font-weight-bold" >Horário do evento</label>
                                <input type="time" class="form-control" id=" horario_do_evento" name=" horario_do_evento" value="<?php echo $recebeHorarioDoEvento ?>" maxlength="45">
                            </div>
                        </div>
                        <!--(FIM) Horário do evento-->

                        <br>

                        <label class="blue-text font-weight-bold" >Escolha uma imagem para a sua publicação:</label>

                        <!--Tratamento de Imagem-->

                        <div class="control-group form-group">
                            <div class="controls">
                                <img  alt="" id="imagem_atual" name="imagem_atual"  src="<?php echo $pasta_imagem . $recebeLinkImagem . $recebeTipoImagem; ?>"  border="3" height="200" width="200" class="img-fluid rounded mb-4" />
                            </div>
                        </div>		

                        <div class="control-group form-group">		
                            <div class="controls">
                                <span class="btn btn-sm btn-mdb-color" onclick="$(this).parent().find('input[type=file]').click();"><i class="fa fa-camera-retro" aria-hidden="true"></i>  Por uma Foto</span>
                                <input name="imagem_upload" id="imagem_upload" onchange="$(this).parent().parent().find('.form-control').html($(this).val().split(/[\\|/]/).pop());" style="display: none;" type="file" accept="image/*">
                            </div>
                        </div>
                        <!--(FIM) Tratamento de imagem -->

                        <br>

                        <!-- Botão excluir imagem-->
                        <div class="control-group form-group">		
                            <div class="controls">
                                <span class="btn btn-sm btn-danger" id="btn_deleta_imagem" name="btn_deleta_imagem"  style="display: none;"><i class="fa fa-trash" aria-hidden="true"></i>  Excluir esta foto</span>
                            </div>
                        </div>
                        <!-- (FIM) Botão excluir imagem-->

                        <br>

                        <!--Tratamento de vídeo-->
                        <label class="blue-text font-weight-bold" >Escolha um video para a sua publicação:</label>

                        <!-- Radio buttons responsáveis pela JQuery do valor-->
                        <div class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input" name="video_radio" value="radio_video_pc_cel" id="radio_video_pc_cel">
                            <label class="custom-control-label" for="radio_video_pc_cel">Video do seu computador ou celular </label>
                        </div>

                        <div class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input" name="video_radio" value="radio_video_youtube" id="radio_video_youtube">
                            <label class="custom-control-label" for="radio_video_youtube">Video do youtube </label>
                        </div>

                        <div class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input" name="video_radio" value="radio_sem_video" id="radio_sem_video">
                            <label class="custom-control-label" for="radio_sem_video">Sem vídeo </label>
                        </div>
                        <!-- (FIM) Radio buttons responsáveis pela JQuery do valor-->

                        <br>

                        <!--(Tratamento de vídeo -->
                        <!--Trata o envio de vídeos do computador ou celular-->
                        <div id="video_pc_cel_div" style="display: none">
                            <div class="control-group form-group">		
                                <div class="controls">
                                    <span class="btn btn-sm btn-deep-orange" onclick="$(this).parent().find('input[type=file]').click();"><i class="fa fa-video-camera" aria-hidden="true"></i>  Por um video</span>
                                    <input name="video_upload" id="video_upload" onchange="$(this).parent().parent().find('.form-control').html($(this).val().split(/[\\|/]/).pop());" style="display: none;" type="file" accept="video/*">
                                </div>
                            </div>
                        </div>

                        <!-- (FIM) Trata o envio de vídeos do computador ou celular-->

                        <!--Trata o envio de vídeos para o Youtube-->
                        <div id="video_youtube_div" style="display: none">
                            <i class="fa fa-youtube-square fa-2x red-text" aria-hidden="true"></i>
                            <div class="control-group form-group">		
                                <div class="controls">
                                    <input class="form-control"  name="video_youtube" id="video_youtube"  placeholder="Cole seu link aqui" onchange="aplicaVideoYoutubeIframe()" onfocusout="aplicaVideoYoutubeIframe()" onpaste="aplicaVideoYoutubeIframe()" onkeypress="return aplicaVideoYoutubeIframe()"  onchange="aplicaVideoYoutubeIframe()" >
                                </div>
                            </div>
                            <!--Este botão está na mesma div para ficar invisível, também, ele carrega as URLS coladas, pois os eventos não funcionam por completo, então ele gera o vídeo dentro do visualizador-->
                            <span class="btn btn-sm btn-danger" id="btn_aplica_link_youtube" name="btn_aplica_link_youtube" onclick="aplicaVideoYoutubeIframe()"><i class="fa fa-youtube fa-2x" aria-hidden="true"></i>  Visualizar link</span>
                        </div>

                        <!--(FIM) Trata o envio de vídeos para o Youtube-->

                        <?php
                        $pasta_video_upload_publicacoes = "video-upload-publicacoes/";
                        if ($recebeLinkVideo != "") {
                            $styleVideoAtual = '';

                            if ($recebeTipoVideo == "") {
                                $pasta_video_upload_publicacoes = null;
                            }
                        } else {
                            $styleVideoAtual = 'style="display:none"';
                        }
                        ?>
                        <div class="control-group form-group"   <?php echo $styleVideoAtual ?> id="div_video_atual" name="div_video_atual">
                            <div class="controls">
                                <div class="embed-responsive embed-responsive-16by9 z-depth-4">
                                    <iframe  class="embed-responsive-item"  id="video_atual" name="video_atual" border="3" src='<?php echo $pasta_video_upload_publicacoes . $recebeLinkVideo . $recebeTipoVideo ?>'  width="600" height="450" allowfullscreen> </iframe>
                                </div>
                            </div>
                        </div>

                        

                        <!--(FIM) Tratamento de vídeo -->

                        <br>

                        <!--Descrição -->
                        <div class="control-group form-group">
                            <div class="controls">
                                <label class="blue-text font-weight-bold" >Descrição:</label>
                                <textarea rows="10" cols="100" class="form-control" id="descricao" name="descricao"  maxlength="1500"  style="resize:true" ><?php echo $recebeDescricao ?></textarea>
                            </div>
                        </div>
                        <!--(FIM)  Descrição -->

                        <label class="text-danger" >Algumas alterações nas publicações (principalmente fotos) só são exibidas ao relogar.</label>	
                        <!-- Botão Gravar -->
                        <div class="control-group form-group">
                            <div class="controls">
                                <!-- For success/fail messages -->
                                <button id="gravar" type="submit" value="send" class="btn btn-sm btn-dark-green"  onclick=" if (document.getElementById('titulo_negocio').value != '' && document.getElementById('categoria_publicacao').value != '' && document.getElementById('faixa_etaria').value != '') {
                                    waitingDialog.show();
                                    setTimeout(function () {
                                    waitingDialog.hide();
                                    }, 100000000000)
                                    }">
                                    <i class="fa fa-save" aria-hidden="true"></i> Gravar</button>
                            </div>
                        </div>	
                        <!-- Botão Gravar -->

                    </form>

                    <div class="control-group form-group">
                        <div class="controls">          
                            <button id="excluir" class="btn btn-sm btn-red" onclick="deletaPublicacao(<?php echo $recebeCodPublicacao ?>, '<?php echo $recebeTipoImagem ?>')"><i class="fa fa-trash" aria-hidden="true"></i> Excluir</button>
                        </div>
                    </div>	

                </div>

            </div> 

        </div>

        <footer>
            <?php include("footer-sem-trava-undo.html"); ?>

            <!--Modal Loading-->    
            <script src="../baile-de-favela/assets/mdb-table-pagination/js/loading-modal.js"></script>
            <!--Carrego aqui o mapa do google-->
            <script src="../baile-de-favela/assets/mdb-table-pagination/js/carrega-mapa-google.js"></script>

            <!--Carrego o mapa logo que a página fica pronta-->
            <script>
                                window.onload = function() {
                                carregaMapaGoogle();
                                };
            </script>
            <!--(FIM)Carrego o mapa logo que a página fica pronta-->

            <!-- Métodos da classe-->
            <script>
                var recebeVal1 = null;
                var recebeVal2 = null;
                function deletaPublicacao(val1, val2) {
                recebeVal1 = val1;
                recebeVal2 = val2;
                $('#exampleModalExcluir').modal().withTimeout(1000);
                }
            </script>

            <script>
                function deletaPublicacao2() {
                window.location.href = 'deleta-publicacao.php?cod_publicacao=' + recebeVal1 + '&tipo_imagem=' + recebeVal2;
                }
            </script>
            <!--(FIM)Métodos da classe-->

            <!--Ttratamentos de imagem-->
            <script>
                //Tratamento para imagem 
                var fileToRead = document.getElementById("imagem_upload");
                fileToRead.addEventListener("change", function (event) {
                document.getElementById('imagem_atual').src = window.URL.createObjectURL(this.files[0]);
                $("#imagem_atual").show("slow");
                $("#btn_deleta_imagem").show("slow");
                }, false);
                //(FIM) Tratamento para imagem
            </script>

            <script>
//Botão deleta imagem
                $("#btn_deleta_imagem").click(function () {
                $("#btn_deleta_imagem").hide("slow");
                $("#imagem_atual").hide("slow");
                $('#imagem_atual').val("");
                $('#imagem_upload').val("");
                //Este bloco reseta todas as propriedades do vídeo, assim o vídeo perde a mini imagem
                var imagemElement = document.getElementById('imagem_atual');
                imagem.removeAttribute('src');
                imagemt.load();
                });
//(FIM)Botão deleta imagem
            </script>
            <!--(FIM) Ttratamentos de imagem-->

            <!--Ttratamentos de vídeo-->
            <script>
                //Tratamento para o video upload do pc
                var fileToRead = document.getElementById("video_upload");
                fileToRead.addEventListener("change", function (event) {
                document.getElementById('video_atual').src = window.URL.createObjectURL(this.files[0]);
                //$("#video_atual").show("slow");
                }, false);
                //(FIM) Tratamento para o video upload do pc
            </script>

            <script>
                //Tratamento para o video upload do pc
                var fileToRead = document.getElementById("video_youtube");
                fileToRead.addEventListener("change", function (event) {
                document.getElementById('video_atual').src = window.URL.createObjectURL(this.files[0]);
                //$("#video_atual").show("slow");
                }, false);
                //(FIM) Tratamento para o video upload do pc
            </script>

            <script >
                //Este método altera essas literais do youtube pois elas não permitem que o vídeo fique embutido
                function aplicaVideoYoutubeIframe() {
                var recebeLinkYoutube;
                recebeLinkYoutube = document.getElementById("video_youtube").value;
                var retira_tokens = recebeLinkYoutube.replace("/watch?v=", "/embed/");
                document.getElementById('video_atual').src = retira_tokens;
                document.getElementById('video_youtube').value = retira_tokens;
                }
                //(FIM)Este método altera essas literais do youtube pois elas não permitem que o vídeo fique embutido
            </script>

            <script>
                //Este método aplica a URL no IFrame ao apertar ENTER
                function aplicaVideoYoutubeIframeEnter(e) {
                if (characterCode == 13)
                {
                aplicaVideoYoutubeIframeEnter();
                }
                }
                //(FIM)Este método aplica a URL no IFrame ao apertar ENTER
            </script>

            <script >
                //Este método altera essas literais do youtube pois elas não permitem que o vídeo fique embutido
                function aplicaVideoframe() {
                var recebePastaVideo = 'video_upload_publicacoes/';
                var recebeLinkVideo;
                var recebeTipoVideo;
                //variável somente usada para o Youtube
                var retiraTokenYoutube = recebeLink.replace("/watch?v=", "/embed/");
                recebeLinkVideo = document.getElementById("link_video").value;
                recebeTipoVideo = document.getElementById("tipo_video").value;
                if (recebeLinkVideo != '' && recebeTipoVideo = ''){

                document.getElementById('video_atual').src = retiraTokenYoutube;
                }

                if (recebeLinkVideo != '' && recebeTipoVideo != ''){

                document.getElementById('video_atual').src = recebePastaVideo + recebeLinkVideo + recebeTipoVideo;
                }

                document.getElementById('video_atual').src = retiraTokenYo  utube;
                document.getElementById('video_youtube').value = retira_tokens;
                }
                //(FIM)Este método altera essas literais do youtube pois elas não permitem que o vídeo fique embutido
            </script>            

            <script>
                $(document).ready(function() {
                aplicaVideoYoutubeIframe();
                });
            </script>


            <script>
                //Controlo aqui os valores dos radios
                $('#valor_fixo').click(function () {
                $("#valor_fixo_div").show("slow");
                $("#valor_diferenciado_div").hide("slow");
                $('#valor_mulher').val("");
                $('#valor_homem').val("");
                });
                $('#valor_diferenciado').click(function () {
                $("#valor_diferenciado_div").show("slow");
                $("#valor_fixo_div").hide("slow");
                $('#valor').val("");
                });
                $('#valor_gratis').click(function () {
                $("#valor_diferenciado_div").hide("slow");
                $("#valor_fixo_div").hide("slow");
                $('#valor_mulher').val("");
                $('#valor_homem').val("");
                $('#valor').val("");
                });
                $('#valor_combinar').click(function () {
                $("#valor_diferenciado_div").hide("slow");
                $("#valor_fixo_div").hide("slow");
                $('#valor_mulher').val("");
                $('#valor_homem').val("");
                $('#valor').val("");
                });
                $('#radio_video_pc_cel').click(function () {
                $("#video_pc_cel_div").show("slow");
                $("#video_youtube_div").hide("slow");
                $('#video_youtube').val("");
                $('#div_video_atual').show("slow");
                });
                $('#radio_video_youtube').click(function () {
                $("#video_youtube_div").show("slow");
                $("#video_pc_cel_div").hide("slow");
                $("#div_video_atual").show("slow");
                $('#video_upload').val("");
                $('#video_atual').val("");
                //Este trecho faz com que ele se esconda e ocupe o espaço confortável novamente
                //$("#video_atual").css("display", "none");

                //Este bloco reseta todas as propriedades do vídeo, assim o vídeo perde a mini imagem
                var videoElement = document.getElementById('video_atual');
                videoElement.pause();
                videoElement.removeAttribute('src');
                videoElement.load();
                });
                //Aqui controlo o caso de o usuário não querer vídeo ou nenhum link
                $('#radio_sem_video').click(function () {
                $("#video_pc_cel_div").hide("slow");
                $("#video_youtube_div").hide("slow");
                $('#video_youtube').val("");
                $('#div_video_atual').hide("slow");
                $('#video_upload').val("");
                $('#video_atual').val("");
                //Este bloco reseta todas as propriedades do vídeo, assim o vídeo perde a mini imagem
                var videoElementSemVideo = document.getElementById('video_atual');
                videoElementSemVideo.pause();
                videoElementSemVideo.removeAttribute('src');
                videoElementSemVideo.load();
                });
                //Aqui controlo o caso de o usuário não querer vídeo ou nenhum link

                //(FIM) Controlo aqui os valores dos radios
            </script>

            <script>
                $('#faixa_etaria').on('change', function () {
                var valor_selecionado;
                valor_selecionado = this.value;
                if (valor_selecionado == 'Livre') {
                $('#img_faixa_etaria').attr('src', 'imagem-site/classificacao-livre.png');
                $('#div_img_faixa_etaria').hide("slow");
                $('#div_img_faixa_etaria').show("slow");
                }

                if (valor_selecionado == '10 Anos') {
                $('#img_faixa_etaria').attr('src', 'imagem-site/classificacao-10.png');
                $('#div_img_faixa_etaria').hide("slow");
                $('#div_img_faixa_etaria').show("slow");
                }

                if (valor_selecionado == '12 Anos') {
                $('#img_faixa_etaria').attr('src', 'imagem-site/classificacao-12.png');
                $('#div_img_faixa_etaria').hide("slow");
                $('#div_img_faixa_etaria').show("slow");
                }

                if (valor_selecionado == '14 Anos') {
                $('#img_faixa_etaria').attr('src', 'imagem-site/classificacao-14.png');
                $('#div_img_faixa_etaria').hide("slow");
                $('#div_img_faixa_etaria').show("slow");
                }

                if (valor_selecionado == '16 Anos') {
                $('#img_faixa_etaria').attr('src', 'imagem-site/classificacao-16.png');
                $('#div_img_faixa_etaria').hide("slow");
                $('#div_img_faixa_etaria').show("slow");
                }

                if (valor_selecionado == '18 Anos') {
                $('#img_faixa_etaria').attr('src', 'imagem-site/classificacao-18.png');
                $('#div_img_faixa_etaria').hide("slow");
                $('#div_img_faixa_etaria').show("slow");
                }
                });
            </script>

        </footer>

    </body>

</html>
