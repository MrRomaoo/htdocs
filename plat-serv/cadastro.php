<?php
session_start();
?>

<html >

    <head>

        <?php
        include("head.html");
        include("mensagem-modal-php.php");
        include('conecta-banco.php');

        if ($_SERVER["REQUEST_METHOD"] == "POST") {

            $titulo_negocio = $_POST['titulo_negocio'];
            $prestador = $_POST['prestador'];
            $senha = $_POST['senha'];
            $repita_senha = $_POST['repita_senha'];
            $email = $_POST['email'];
            $telefone = $_POST['telefone'];

            if (strcasecmp($senha, $repita_senha) == 0) {

                $testaEmailExiste = "";

                $sql = $conn->query("SELECT email
	FROM tbl_prestador
	WHERE 
	email='$email'");

                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {

                    $testaEmailExiste = $row['email'];
                }

                if ($testaEmailExiste == $email) {

                    mensagemModalPhp('Atenção', 'O email "' . $testaEmailExiste . '" já existe.');

                    $sql->connection = null;
                    $conn->connection = null;
                } else {

                    $sql = $conn->query("SELECT prestador
	FROM tbl_prestador
	WHERE 
	prestador='$prestador'");

                    while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {

                        $testaPrestadorExiste = $row['prestador'];
                    }

                    if ($testaPrestadorExiste == $prestador) {

                        mensagemModalPhp("Atenção", 'O usuário "' . $testaPrestadorExiste . '" já existe.');
                        $sql->connection = null;
                        $conn->connection = null;
                    } else {

                        $sql->connection = null;
                        $conn->connection = null;

                        $sql = "INSERT INTO tbl_prestador 
                                    (cod_prestador, titulo_negocio, prestador, senha, email, telefone, link_imagem, tipo_imagem, data_cadastro) 
                                     VALUES 
                                     (NULL, '$titulo_negocio', '$prestador', '$senha', '$email', '$telefone', 'sem_foto', '.png', CURRENT_TIMESTAMP)";

                        $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));

                        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                        $stmt = $conn->prepare($sql);
                        $stmt->execute();

                        //Recupera o último registro
                        $ultimo_registro = $conn->lastInsertId();

                        $stmt->connection = null;

                        if (!empty($email) && $sql) {

                            $to = "$email";
                            $subject = "Gente cultura";
                            $txt = "Bem vindo ao Baile de favela!\n Seu login foi criado com sucesso!\n Este é o seu usuário: " . $prestador . "\n" . "Esta é a sua senha: " . $senha . "\n" . "Muito obrigado pelo seu cadastro! A equipe do GenteCultura agradece!";
                            $headers = "From: gentecultura.com" . "\r\n";

                            mail($to, $subject, $txt, $headers);
                        }

                        $sql->connection = null;
                        $conn->connection = null;

                        mensagemModalPhpCadastroSucesso("Deu tudo certo!", "Cadastro criado com sucesso! Acesse a página de login para entrar no sistema");
                    }
                }
            } else {

                mensagemModalPhp("Atenção", "Os campos da senha precisam ser iguais.");
                $sql->connection = null;
                $conn->connection = null;
            }
        }
        ?>

    </head>

    <body>

        <?php include("body-nav-bar.php"); ?>

        <!-- Page Content -->
        <div class="container quebra_linha">

            <span style="display:block; height: 15px;"></span>
            <span style="display:block; weight: 5px;"></span>

            <!-- Contact Form -->
            <!-- In order to set the email address and subject line for the contact form go to the bin/contact_me.php file. -->
            <div class="row">
                <div class="col-lg-8 mb-4">
                    <h3>Crie sua conta!</h3>

                    <form name="formAdd" id="formAdd" method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">

                        <div class="control-group form-group">
                            <div class="controls">
                                <label>Campos obrigat&oacuterios possuem: <span style="color:red;font-weight:bold">*</span></label>
                            </div>
                        </div>

                        <div class="control-group form-group"> 
                            <div class="controls">
                                <label>Nome do seu perfil (Aparece em todas as publicações)<span style="color:red;font-weight:bold">*</span></label>
                                <input type="text" class="form-control"  value="<?php if (!empty($_POST['titulo_negocio'])) {
            echo $_POST['titulo_negocio'];
        } ?>" name="titulo_negocio" id="titulo_negocio" minlength="5" maxlength="15" required>
                                <p class="help-block"></p>
                            </div>
                        </div>

                        <div class="control-group form-group">
                            <div class="controls">
                                <label>Login<span style="color:red;font-weight:bold">*</span></label>
                                <input type="text" class="form-control" value="<?php if (!empty($_POST['prestador'])) {
            echo $_POST['prestador'];
        } ?>" id="prestador" name="prestador" minlength="5" maxlength="20" required> 
                            </div>
                        </div>

                        <div class="control-group form-group">
                            <div class="controls">
                                <label>Senha<span style="color:red;font-weight:bold">*</span></label>
                                <input type="password" class="form-control" value="<?php if (!empty($_POST['senha'])) {
            echo $_POST['senha'];
        } ?>" id="senha" name="senha" minlength="5" maxlength="20" required>
                            </div>
                        </div>

                        <div class="control-group form-group">
                            <div class="controls">
                                <label>Repita a senha<span style="color:red;font-weight:bold">*</span></label>
                                <input type="password" class="form-control" value="<?php if (!empty($_POST['repita_senha'])) {
            echo $_POST['repita_senha'];
        } ?>"  id="repita_senha" name="repita_senha" minlength="5" maxlength="20" required>
                            </div>
                        </div>

                        <div class="control-group form-group">
                            <div class="controls">
                                <label>E-mail (Para recuperação de senha)</label>
                                <input type="text" class="form-control"value="<?php if (!empty($_POST['email'])) {
            echo $_POST['email'];
        } ?>"  id="email" maxlength="60" name="email">
                            </div>
                        </div>

                        <div class="control-group form-group">
                            <div class="controls">
                                <label>Telefone (N&atildeo esque&ccedila o DDD)</label>
                                <input type="text" class="form-control"value="<?php if (!empty($_POST['telefone'])) {
            echo $_POST['telefone'];
        } ?>"  id="telefone" name="telefone" maxlength="25">
                            </div>
                        </div>

                        <div class="control-group form-group">
                            <div class="controls">
                                <button  type="submit" value="send"  class="btn btn-sm btn-green"><i class="fa fa-check-square-o" aria-hidden="true"></i><b>  Cadastrar</b></button>
                            </div>
                        </div>	

                    </form>

                </div>

            </div>
            <!-- /.row -->

        </div>
        <!-- /.container -->

        <!-- Footer -->
        <footer>
<?php include("footer.html"); ?>

        </footer>

    </body>

</html>