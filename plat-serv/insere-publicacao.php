﻿<?php
include('verifica-session.php');
?>

<!DOCTYPE html>
<html>

    <head>

        <?php
        include("head.html");
        include ('conecta-banco.php');
        include ('mensagem-modal-php.php');

        //Aqui verifico quantas publicações o usuário tem
        /* $sql = $conn->query("SELECT qtd_publicacoes
          FROM
          tbl_prestador
          WHERE
          cod_prestador=".$_SESSION['cod_prestador']);

          while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
          $recebeQtdPublicacoes = $row['qtd_publicacoes'];
          }

          $maxPublicacoes=0; */

        $recebeTitulacao = $_SESSION['titulacao'];

        if ($recebeTitulacao == "D") {
            $maxPublicacoes = 25;
            /* $publicacoesRestantes =$maxPublicacoes-intval($recebeQtdPublicacoes); */
        }

        if ($recebeTitulacao == "O") {
            $maxPublicacoes = 15;
            /* $publicacoesRestantes = $maxPublicacoes-intval($recebeQtdPublicacoes); */
        }

        if ($recebeTitulacao == "S") {
            $maxPublicacoes = 5;
            /* $publicacoesRestantes = $maxPublicacoes-intval($recebeQtdPublicacoes); */
        }

        $sql->connection = null;
        $conn->connection = null;

        if ($_SERVER["REQUEST_METHOD"] == "POST") {

            /* if($publicacoesRestantes>0){ */

            if ($_POST['valor'] == 0 || empty($_POST['valor'])) {

                if (($_POST['valor_homem'] == 0 || empty($_POST['valor_homem'])) && ($_POST['valor_mulher'] == 0 || empty($_POST['valor_mulher']))) {

                    $_POST['valor'] = 'Grátis';

                    $_POST['valor_homem'] = '';
                    $_POST['valor_mulher'] = '';
                    
                } else {
                    
                    $_POST['valor'] = '';
                    
                }
            }

            /*
              if ($_POST['valor'] == 0 || empty($_POST['valor'])) {

              $_POST['valor'] = '0';
              } */

                if (!empty($_SESSION['prestador']) && !empty($_SESSION['senha']) && !empty($_SESSION['cod_prestador'])) {

                    $cod_prestador = $_SESSION['cod_prestador'];
                    $titulo_negocio = $_POST['titulo_negocio'];
                    $descricao = $_POST['descricao'];
                    $faixa_etaria = $_POST['faixa_etaria'];
                    $rua = $_POST['rua'];
                    $numero = $_POST['numero'];
                    $complemento = $_POST['complemento'];
                    $bairro = $_POST['bairro'];
                    $cidade = $_POST['cidade'];
                    $estado = $_POST['estado'];
                    $cep = $_POST['cep'];
                    $pais = $_POST['pais'];
                    $referencia = $_POST['referencia'];
                    $contato = $_POST['contato'];
                    $data_do_evento = $_POST['data_do_evento'];
                    $horario_do_evento = $_POST['horario_do_evento'];
                    $categoria_publicacao = $_POST['categoria_publicacao'];
                    $valor = $_POST['valor'];
                    $valor_homem = $_POST['valor_homem'];
                    $valor_mulher = $_POST['valor_mulher'];
                    $link_imagem = "";
                    $tipo_imagem = "";
                    $link_video = "";
                    $tipo_video = "";

                    $sql = "INSERT INTO tbl_publicacoes
                                        (cod_publicacao,
                                         titulo_negocio, 
                                        cod_prestador_publicacao,
                                        descricao,
                                        faixa_etaria,
                                        rua,
                                        numero,
                                        complemento,
                                        bairro,
                                        cidade,
                                        estado,
                                        cep,
                                        pais,
                                        referencia,
                                        contato,
                                        data_do_evento,
                                        horario_do_evento,
                                        categoria_publicacao, 
                                        valor,
                                        valor_homem,
                                        valor_mulher,
                                        data_hora_publicacao, 
                                        link_imagem,
                                        tipo_imagem,
                                        link_video,
                                        tipo_video)
                                        VALUES (
                                                NULL, 
                               '$titulo_negocio',"
                            . "'$cod_prestador', "
                            . "'$descricao',"
                            . "'$faixa_etaria',"
                            . "'$rua',"
                            . "'$numero',"
                            . "'$complemento',"
                            . "'$bairro',"
                            . "'$cidade',"
                            . "'$estado',"
                            . "'$cep',"
                            . "'$pais',"
                            . "'$referencia',"
                            . "'$contato',"
                            . "'$data_do_evento',"
                            . " '$horario_do_evento',"
                            . " '$categoria_publicacao',"
                            . "'$valor', "
                            . "'$valor_homem',"
                            . "'$valor_mulher',"
                            . " CURRENT_TIMESTAMP,"
                            . "'$linkImagem',"
                            . "'$tipo_imagem',"
                            . "'$link_video',"
                            . "'$tipo_video')";

                    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
                    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                    $stmt = $conn->prepare($sql);
                    $stmt->execute();
                    $ultimo_registro = $conn->lastInsertId();

                    $conn->connection = null;
                    $stmt->connection = null;
                
                    //Insiro aqui um registro de curtida para inicializar a contagem, pois quando o JOIN com a tabela curtida
                    // é feito, ele obviamente só retorna os registros que tem vínculo com a tabela curtidas. Desta forma eu
                    // digo que todo registro inserido já será inicializado com um gostei e nao gostei zerados.
                    $recebeCodPrestador=$_SESSION['cod_prestador'];
                    
                    $sql="INSERT INTO tbl_curtidas 
                        (cod_curtidas,
                        tbl_publicacoes_cod_publicacao, 
                        tbl_prestador_cod_prestador_curtiu,
                        tbl_prestador_cod_dono_publicacao,
                        gostei,
                        nao_gostei)
                        VALUES
                        (null, $ultimo_registro, $recebeCodPrestador,$recebeCodPrestador,0,0)";

                    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
                    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                    $stmt = $conn->prepare($sql);
                    $stmt->execute();
               
                    
                    //Tratamento da imagem
                    if ($_FILES['imagem_upload']['size'] > 0 && $_FILES['imagem_upload']['size'] < 5971520) {

                        //Estre tratamento é nescessário, caso haja um caso muito específico de um usuário upar uma imagem com o mesmo nome da que está como padrão do serviror para um user sem imagem.
                        if ($_FILES['imagem_upload']['name'] == "sem_foto.png") {
                            $_FILES['imagem_upload']['name'] = "prestador_sem_foto.png";
                        }

                        $tipo_imagem = '.' . strtolower(end(explode('.', $_FILES['imagem_upload']['name'])));

                        $sql = "UPDATE tbl_publicacoes 
                                    SET link_imagem= '$ultimo_registro', tipo_imagem='$tipo_imagem'
                                    WHERE 
                                    cod_publicacao = $ultimo_registro";

                        $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
                        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                        $stmt = $conn->prepare($sql);
                        $stmt->execute();

                        $image_name = $_FILES['imagem_upload']['name'];
                        $image_tmp_name = $_FILES['imagem_upload']['tmp_name'];

                        $pasta_imagem = 'imagem-upload-publicacoes/';
                        $location = $pasta_imagem . $image_name;
                        move_uploaded_file($image_tmp_name, $location);
                        rename($pasta_imagem . $image_name, $pasta_imagem . $ultimo_registro . $tipo_imagem);

                        $sql->connection = null;
                        $conn->connection = null;
                        $stmt->connection = null;
                        
                    } else {

                        $link_imagem = 'sem_foto';
                        $tipo_imagem = '.png';

                        $sql = "UPDATE tbl_publicacoes 
                                    SET link_imagem= '$link_imagem', tipo_imagem='$tipo_imagem'
                                    WHERE 
                                    cod_publicacao = $ultimo_registro";

                        $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
                        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                        $stmt = $conn->prepare($sql);
                        $stmt->execute();

                        $sql->connection = null;
                        $conn->connection = null;
                        $stmt->connection = null;
                    }
                    //(FIM)Tratamento da imagem
                    
                    //Tratamento do video
                    if ($_FILES['video_upload']['size'] > 0) {

                        //Estre tratamento é nescessário, cao haja um caso muito específico de um usuário upar uma imagem com o mesmo nome da que está como padrão do serviror para um user sem imagem.
                        if ($_FILES['video_upload']['name'] == "sem_video.mp4") {
                            $_FILES['video_upload']['name'] = "prestador_sem_video.mp4";
                        }
                        
                       //Neste Caso o tipo do vídeo é retirado com o método explode
                        $tipo_video = '.' . strtolower(end(explode('.', $_FILES['video_upload']['name'])));

                        $sql = "UPDATE tbl_publicacoes 
                                    SET link_video= '$ultimo_registro', tipo_video='$tipo_video'
                                    WHERE 
                                    cod_publicacao = $ultimo_registro";

                        $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
                        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                        $stmt = $conn->prepare($sql);
                        $stmt->execute();

                        $video_name = $_FILES['video_upload']['name'];
                        $video_tmp_name = $_FILES['video_upload']['tmp_name'];

                        $pasta_video = 'video-upload-publicacoes/';
                        $location = $pasta_video . $video_name;
                        move_uploaded_file($video_tmp_name, $location);
                        rename($pasta_video . $video_name, $pasta_video . $ultimo_registro . $tipo_video);

                        $sql->connection = null;
                        $conn->connection = null;
                        $stmt->connection = null;
                        
                    } else {

                        if ($_POST['video_youtube'] != '' || !empty($_POST['video_youtube'])) {

                            $link_video = str_replace("/watch?v=", "/embed/",$_POST['video_youtube']);
                            $tipo_video = '';

                            $sql = "UPDATE tbl_publicacoes 
                                    SET link_video= '$link_video', tipo_video='$tipo_video'
                                    WHERE 
                                    cod_publicacao = $ultimo_registro";

                            $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
                            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                            $stmt = $conn->prepare($sql);
                            $stmt->execute();

                            $sql->connection = null;
                            $conn->connection = null;
                            $stmt->connection = null;
                            
                        } else {

                            $sql = "UPDATE tbl_publicacoes 
                                    SET link_video= '', tipo_video=''
                                    WHERE 
                                    cod_publicacao = $ultimo_registro";

                            $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
                            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                            $stmt = $conn->prepare($sql);
                            $stmt->execute();

                            $sql->connection = null;
                            $conn->connection = null;
                            $stmt->connection = null;
                        }
                    }
                    //(FIM)Tratamento do video

                    if (!empty($_SESSION['prestador']) && !empty($_SESSION['senha']) && !empty($_SESSION['cod_prestador'])) {

                        header('Location: prestador-login.php?cod_prestador=' . $_SESSION['cod_prestador'] . '&' . 'prestador=' . $_SESSION['prestador']);
                    } else {

                        header('Location:index.php?');
                    }
                } else {

                    header('Location:index.php?');
                }
   
            /* }else{

              mensagemModalPhp('Atenção!', 'O limite máximo de publicações foi atingido, consulte nossos planos para aumentar seus benefícios ou exclua algumas publicações para postar novas.');

              } */
        }
        ?>

    </head>

    <body>

        <?php include("body-nav-bar.php"); ?>

        <!-- Conteúdo da página -->
        <div class="container quebra_linha">

            <span style="display:block; height: 15px;"></span>
            <span style="display:block; weight: 5px;"></span>

            <div class="row">
                <div class="col-lg-8 mb-4">
                    <h3 class="teal-text">Insira uma nova publicação </h3>
                      <!--<h3> <?php /* echo "(".$publicacoesRestantes." publicações restantes de ".$maxPublicacoes.")" */ ?> </h3>-->

                    <form name="formAdd" id="formAdd" method="post" enctype="multipart/form-data" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">

                        <div class="control-group form-group">
                            <div class="controls">
                                <label class="font-weight-bold">Campos obrigatórios possuem: <span style="color:red;font-weight:bold">*</span></label>
                            </div>
                        </div>

                        <!--ítulo-->
                        <div class="control-group form-group">
                            <div class="controls">
                                <label class="blue-text font-weight-bold">Título da publicação: <span style="color:red;font-weight:bold">*</span></label>
                                <input type="text" class="form-control" name="titulo_negocio" id="titulo_negocio" maxlength="70" required>
                                <p class="help-block"></p>
                            </div>
                        </div>
                        <!--(FIM) Titulo-->

                        <br>

                        <!-- Categoria-->
                        <div class="control-group form-group">
                            <div class="controls">
                                <label class="blue-text font-weight-bold" >Categoria: <span style="color:red;font-weight:bold">*</span></label>
                                <select class="form-control" id="categoria_publicacao" name="categoria_publicacao" required>
                                    <option> </option>
                                    <?php
                                    include("categorias.php");
                                    ?>
                                </select>	
                            </div>							
                        </div>
                        <!--(FIM) Categoria-->

                        <br>

                        <!-- Classificação-->
                        <div class="control-group form-group">
                            <div class="controls">
                                <label class="blue-text font-weight-bold" >Classificação do evento: <span style="color:red;font-weight:bold">*</span></label>
                                <select class="form-control" id="faixa_etaria" name="faixa_etaria" required>
                                    <option> </option>
                                    <?php
                                    include("classificacao.php");
                                    ?>
                                </select>	
                            </div>							
                        </div>
                        <!--(FIM) Classificação-->

                        <!--Imagem referende a Classificação escolhida-->
                        <div class="control-group form-group" id="div_img_faixa_etaria" style="display: none;">
                            <div class="controls">
                                Classificação: <img alt="" id="img_faixa_etaria" name="img_faixa_etaria" border="3" height="50" width="50" class="rounded-circle" required /> 
                            </div>
                        </div>
                        <!--(FIM) Imagem referende a Classificação escolhida-->

                        <br>

                        <label class="blue-text font-weight-bold" >Valor do evento</label>     

                        <br>

                        <!-- Radio buttons responsáveis pela JQuery do valor-->
                        <div class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input" name="valor_radio" value="valor_fixo" id="valor_fixo" >
                            <label class="custom-control-label" for="valor_fixo">O mesmo valor para todos os sexos</label>
                        </div>

                        <div class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input" name="valor_radio" value="valor_diferenciado" id="valor_diferenciado">
                            <label class="custom-control-label" for="valor_diferenciado">Valor diferenciado para os sexos</label>
                        </div>

                        <div class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input" name="valor_radio" value="Valor a combinar" id="valor_combinar">
                            <label class="custom-control-label" for="valor_combinar">Valor a combinar</label>
                        </div>

                        <div class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input" name="valor_radio" value="Grátis" id="valor_gratis">
                            <label class="custom-control-label" for="valor_gratis">Grátis</label>
                        </div>

                        <br>

                        <!-- Este bloco aparece quando o valor fixo é selecionado.-->
                        <div name="valor_fixo_div" id="valor_fixo_div" class="control-group form-group" style="display: none">
                            <div class="controls">
                                <label>Valor(R$) </label>
                                <input type="number" class="form-control" id="valor" name="valor" maxlength="10">
                            </div>
                        </div>
                        <!-- (FIM) Este bloco aparece quando o valor fixo é selecionado.-->

                        <!-- Este bloco aparece quando o valor diferenciado é selecionado-->
                        <div name="valor_diferenciado_div" id="valor_diferenciado_div" class="control-group form-group" style="display: none">
                            <!-- Grid row -->
                            <div class="row">

                              <!-- Grid column -->
                                <div class="col">
                                    <!-- Default input -->
                                    <i class="fa fa-male fa-3x blue-text" aria-hidden="true"></i>
                                    <input type="number" class="form-control" id="valor_homem" name="valor_homem" placeholder="Homem" maxlength="1000000000">
                                </div>
                                <!-- Grid column -->
                                
                                <!-- Grid column -->
                                <div class="col">
                                    <!-- Default input -->
                                    <i class="fa fa-female fa-3x red-text" aria-hidden="true"></i>
                                    <input type="number" class="form-control" id="valor_mulher" name="valor_mulher" placeholder="Mulher" maxlength="1000000000">
                                </div>
                                <!-- Grid column -->

                            </div>
                            <!-- Grid row -->
                        </div>
                        <!-- Default form grid -->
                        <!-- (FIM) Este bloco aparece quando o valor diferenciado é selecionado-->

                        <br>

                        <!-- Endereço-->
                        <label class="blue-text font-weight-bold" >Endereço do  seu evento </label>
                        <label class="red-text" >Atenção, preencha os campos do endereço, pois assim o mapa do Google vai ser gerado para o seu cliente! </label>

                        <!-- Rua-->
                        <div class="control-group form-group">
                            <div class="controls">
                                <label class="blue-text font-weight-bold" > Rua </label>
                                <input type="text" class="form-control" id="rua" name="rua" onkeypress="return carregaMapaGoogleEnter(event)"  onfocusout="carregaMapaGoogle()" maxlength="200">                          
                            </div>							
                        </div>
                        <!--(FIM) Rua-->

                        <!-- Número-->
                        <div class="control-group form-group">
                            <div class="controls">
                                <label class="blue-text font-weight-bold" > Número </label>
                                <input type="text" class="form-control" id="numero" name="numero" onkeypress="return carregaMapaGoogleEnter(event)"  onfocusout="carregaMapaGoogle()" maxlength="20">                          
                            </div>							
                        </div>
                        <!--(FIM) Número-->

                        <!-- Complemento-->
                        <div class="control-group form-group">
                            <div class="controls">
                                <label class="blue-text font-weight-bold" > Complemento (Quadra, lote etc.)</label>
                                <input type="text" class="form-control" id="complemento" name="complemento" onkeypress="return carregaMapaGoogleEnter(event)"  onfocusout="carregaMapaGoogle()" maxlength="200">                          
                            </div>							
                        </div>
                        <!--(FIM) Complemento-->

                        <!-- Bairro-->
                        <div class="control-group form-group">
                            <div class="controls">
                                <label class="blue-text font-weight-bold" > Bairro </label>
                                <input type="text" class="form-control" id="bairro" name="bairro" onkeypress="return carregaMapaGoogleEnter(event)"  onfocusout="carregaMapaGoogle()"  maxlength="100">                          
                            </div>							
                        </div>
                        <!--(FIM) Bairro-->

                        <!-- Cidade-->
                        <div class="control-group form-group">
                            <div class="controls">
                                <label class="blue-text font-weight-bold" > Cidade </label>
                                <input type="text" class="form-control" id="cidade" name="cidade" onkeypress="return carregaMapaGoogleEnter(event)"  onfocusout="carregaMapaGoogle()"  maxlength="100">                          
                            </div>							
                        </div>
                        <!--(FIM) Cidade-->

                        <!-- Estado-->
                        <div class="control-group form-group">
                            <div class="controls">
                                <label class="blue-text font-weight-bold" > Estado </label>
                                <input type="text" class="form-control" id="estado" name="estado" onkeypress="return carregaMapaGoogleEnter(event)"  onfocusout="carregaMapaGoogle()"  maxlength="100">                          
                            </div>							
                        </div>
                        <!--(FIM)  Estado-->

                        <!-- CEP-->
                        <div class="control-group form-group">
                            <div class="controls">
                                <label class="blue-text font-weight-bold" > CEP </label>
                                <input type="text" class="form-control" id="cep" name="cep" onkeypress="return carregaMapaGoogleEnter(event)"  onfocusout="carregaMapaGoogle()"  maxlength="100">                          
                            </div>							
                        </div>
                        <!--(FIM) CEP->

                        <!-- Pais-->
                        <div class="control-group form-group">
                            <div class="controls">
                                <label class="blue-text font-weight-bold" > Pais </label>
                                <input type="text" class="form-control" id="pais" name="pais" onkeypress="return carregaMapaGoogleEnter(event)"  onfocusout="carregaMapaGoogle()"  maxlength="60">                          
                            </div>							
                        </div>
                        <!--(FIM)  Pais-->

                        <!-- Cidade-->
                        <div class="control-group form-group">
                            <div class="controls">
                                <label class="blue-text font-weight-bold" > Referência </label>
                                <input type="text" class="form-control" id="referencia" name="referencia" onkeypress="return carregaMapaGoogleEnter(event)"  onfocusout="carregaMapaGoogle()"  maxlength="150">                          
                            </div>							
                        </div>
                        <!--(FIM)  Cidade-->

                        <!--(FIM) Endereço-->


                        <!--Tratamento do endereço que aguarda o AJAX-->
                        <div id="endereco" name="endereco">
                        </div>                
                        <!--(FIM) Tratamento do endereço que aguarda o AJAX-->

                        <br>

                        <!-- Contato-->
                        <div class="control-group form-group">
                            <div class="controls">
                                <label class="blue-text font-weight-bold" >Forma de contato, exemplo: Telefone, email, site etc.</label>
                                <input type="text" class="form-control" id="contato" name="contato" maxlength="100">
                            </div>
                        </div>
                        <!--(FIM) Contato-->

                        <br>

                        <!-- Data do evento-->
                        <div class="control-group form-group">
                            <div class="controls">
                                <label class="blue-text font-weight-bold" >Data do evento.</label>
                                <input type="date" class="form-control" id="data_do_evento" name="data_do_evento" maxlength="20">
                            </div>
                        </div>
                        <!--(FIM) Data do evento-->

                        <br>

                        <!-- Horário do evento-->
                        <div class="control-group form-group">
                            <div class="controls">
                                <label class="blue-text font-weight-bold" >Horário do evento</label>
                                <input type="time" class="form-control" id=" horario_do_evento" name=" horario_do_evento" maxlength="45">
                            </div>
                        </div>
                        <!--(FIM) Horário do evento-->

                        <br>

                        <label class="blue-text font-weight-bold" >Escolha uma imagem para a sua publicação:</label>

                        <!--Tratamento de Imagem-->
                        <div class="control-group form-group">
                            <div class="controls">
                                <img  alt="" id="imagem_atual" style="display: none;" name="imagem_atual" border="3" height="200" width="200" class="img-fluid rounded mb-4" />
                            </div>
                        </div>		

                        <div class="control-group form-group">		
                            <div class="controls">
                                <span class="btn btn-sm btn-mdb-color" onclick="$(this).parent().find('input[type=file]').click();"><i class="fa fa-camera-retro" aria-hidden="true"></i>  Por uma Foto</span>
                                <input name="imagem_upload" id="imagem_upload" onchange="$(this).parent().parent().find('.form-control').html($(this).val().split(/[\\|/]/).pop());" style="display: none;" type="file" accept="image/*">
                            </div>
                        </div>
                        <!--(FIM) Tratamento de imagem -->

                        <!-- Botão excluir imagem-->
                        <div class="control-group form-group">		
                            <div class="controls">
                                <span class="btn btn-sm btn-danger" id="btn_deleta_imagem" name="btn_deleta_imagem"  style="display: none;"><i class="fa fa-trash" aria-hidden="true"></i>  Excluir esta foto</span>
                            </div>
                        </div>
                        <!-- (FIM) Botão excluir imagem-->
                        <br>

                        <!--Tratamento de vídeo-->
                        <label class="blue-text font-weight-bold" >Escolha um video para a sua publicação:</label>

                        <!-- Radio buttons responsáveis pela JQuery do valor-->
                        <div class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input" name="video_radio" value="radio_video_pc_cel" id="radio_video_pc_cel">
                            <label class="custom-control-label" for="radio_video_pc_cel">Video do seu computador ou celular </label>
                        </div>

                        <div class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input" name="video_radio" value="radio_video_youtube" id="radio_video_youtube">
                            <label class="custom-control-label" for="radio_video_youtube">Video do youtube </label>
                        </div>

                        <div class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input" name="video_radio" value="radio_sem_video" id="radio_sem_video">
                            <label class="custom-control-label" for="radio_sem_video">Sem vídeo </label>
                        </div>
                        <!-- (FIM) Radio buttons responsáveis pela JQuery do valor-->

                        <br>

                        <!--(Tratamento de vídeo -->
                        <!--Trata o envio de vídeos do computador ou celular-->

                        <div id="video_pc_cel_div" style="display: none;">
                            <div class="control-group form-group">		
                                <div class="controls">
                                    <span class="btn btn-sm btn-deep-orange" onclick="$(this).parent().find('input[type=file]').click();"><i class="fa fa-video-camera" aria-hidden="true"></i>  Por um video</span>
                                    <input name="video_upload" id="video_upload" onchange="$(this).parent().parent().find('.form-control').html($(this).val().split(/[\\|/]/).pop());" style="display: none;" type="file" accept="video/*">
                                </div>
                            </div>
                        </div>
                        <!-- (FIM) Trata o envio de vídeos do computador ou celular-->

                        <!--Trata o envio de vídeos para o Youtube-->
                        <div id="video_youtube_div" style="display: none;">
                            <i class="fa fa-youtube-square fa-2x red-text" aria-hidden="true"></i>
                            <div class="control-group form-group">		
                                <div class="controls">
                                    <input class="form-control"  name="video_youtube" id="video_youtube" placeholder="Cole seu link aqui" onchange="aplicaVideoYoutubeIframe()" onfocusout="aplicaVideoYoutubeIframe()" onpaste="aplicaVideoYoutubeIframe()" onkeypress="return aplicaVideoYoutubeIframe()"  onchange="aplicaVideoYoutubeIframe()" >
                                </div>
                            </div>
                            <!--Este botão está na mesma div para ficar invisível, também, ele carrega as URLS coladas, pois os eventos não funcionam por completo, então ele gera o vídeo dentro do visualizador-->
                           <span class="btn btn-sm btn-danger" id="btn_aplica_link_youtube" name="btn_aplica_link_youtube" onclick="aplicaVideoYoutubeIframe()"><i class="fa fa-youtube fa-2x" aria-hidden="true"></i>  Visualizar link</span>
                        </div>
                        <!--(FIM) Trata o envio de vídeos para o Youtube-->
                   
                        <div class="control-group form-group" style="display: none;" id="div_video_atual" name="div_video_atual">
                            <div class="controls">
                                <div class="embed-responsive embed-responsive-16by9 z-depth-4">
                                    <iframe  class="embed-responsive-item" id="video_atual" name="video_atual" border="3" width="150" height="150" allowfullscreen> </iframe>
                                </div>
                            </div>
                        </div>

                        <!--(FIM)Trata o envio de vídeos para o Youtube-->
                        <!--(FIM) Tratamento de vídeo -->

                        <br>

                        <!--Descrição -->
                        <div class="control-group form-group">
                            <div class="controls">
                                <label class="blue-text font-weight-bold" >Descrição:</label>									
                                <textarea rows="10"  cols="100" class="form-control" id="descricao" name="descricao" style="resize:none" maxlength="1500"></textarea>
                            </div>
                        </div>
                        <!--(FIM)  Descrição -->

                        <!--(FIM) Botão Gravar -->
                        <div class="control-group form-group">
                            <div class="controls">
                                <button id="gravar" type="submit" value="send" class="btn btn-sm btn-green" 
                                        onclick=" if (document.getElementById('titulo_negocio').value != '' && document.getElementById('categoria_publicacao').value != '' && document.getElementById('faixa_etaria').value != '') {
                                                    waitingDialog.show();
                                                    setTimeout(function () {
                                                        waitingDialog.hide();
                                                    }, 100000000000)
                                                }">
                                    <i class="fa fa-save" aria-hidden="true"></i> Gravar</button>
                            </div>
                        </div>	
                        <!--(FIM) Botão Grava -->

                    </form>

                </div>

            </div>

        </div>

        <footer>
            <?php include("footer.html"); ?>

            <!--Modal Loading-->    
            <script src="../baile-de-favela/assets/mdb-table-pagination/js/loading-modal.js"></script>
            <!--Carrego aqui o mapa do google-->
            <script src="../baile-de-favela/assets/mdb-table-pagination/js/carrega-mapa-google.js"></script>

            <!--Ttratamentos de imagem-->
            <script>
                //Tratamento para imagem 
                var fileToRead = document.getElementById("imagem_upload");
                fileToRead.addEventListener("change", function (event) {
                    document.getElementById('imagem_atual').src = window.URL.createObjectURL(this.files[0]);
                    $("#imagem_atual").show("slow");
                    $("#btn_deleta_imagem").show("slow");
                }, false);
                //(FIM) Tratamento para imagem
            </script>

            <script>
//Botão deleta imagem
                $("#btn_deleta_imagem").click(function () {
                    $("#btn_deleta_imagem").hide("slow");
                    $("#imagem_atual").hide("slow");
                    $('#imagem_atual').val("");
                    $('#imagem_upload').val("");
                    //Este bloco reseta todas as propriedades do vídeo, assim o vídeo perde a mini imagem
                    var imagemElement = document.getElementById('imagem_atual');
                    imagem.removeAttribute('src');
                    imagemt.load();
                   
                });
//(FIM)Botão deleta imagem
            </script>

            <!--(FIM) Ttratamentos de imagem-->
            
           <!--Ttratamentos de vídeo-->
            <script>
                //Tratamento para o video upload do pc
                var fileToRead = document.getElementById("video_upload");
                fileToRead.addEventListener("change", function (event) {
                    document.getElementById('video_atual').src = window.URL.createObjectURL(this.files[0]);
                    //$("#video_atual").show("slow");
                }, false);
                //(FIM) Tratamento para o video upload do pc
            </script>

            <script>
                //Tratamento para o video upload do pc
                var fileToRead = document.getElementById("video_youtube");
                fileToRead.addEventListener("change", function (event) {
                    document.getElementById('video_atual').src = window.URL.createObjectURL(this.files[0]);
                    //$("#video_atual").show("slow");
                }, false);
                //(FIM) Tratamento para o video upload do pc
            </script>

            <script >
                //Este método altera essas literais do youtube pois elas não permitem que o vídeo fique embutido
                function aplicaVideoYoutubeIframe() {
                    var recebeLinkYoutube;
                    recebeLinkYoutube = document.getElementById("video_youtube").value;
                    var retira_tokens = recebeLinkYoutube.replace("/watch?v=", "/embed/");

                    document.getElementById('video_atual').src = retira_tokens;
                    document.getElementById('video_youtube').value = retira_tokens;
                }
                //(FIM)Este método altera essas literais do youtube pois elas não permitem que o vídeo fique embutido
            </script>

            <!-- Método Original para recuperar o onpast, copy e oncut, abaixo está somente o onpast-->
           <!--<script>
             input.oncut = input.oncopy = input.onpaste = function(event) {
               alert(event.type + ' - ' + event.clipboardData.getData('text/plain'));
               return false;
             };
           </script>-->
       
            <script>
                //Este método aplica a URL no IFrame ao apertar ENTER
                function aplicaVideoYoutubeIframeEnter(e) {
                    if (characterCode == 13)
                    {
                        aplicaVideoYoutubeIframeEnter();
                    }
                }
                //(FIM)Este método aplica a URL no IFrame ao apertar ENTER
            </script>

            <script>
                //Controlo aqui os valores dos radios
                $('#valor_fixo').click(function () {
                    $("#valor_fixo_div").show("slow");
                    $("#valor_diferenciado_div").hide("slow");
                    $('#valor_mulher').val("");
                    $('#valor_homem').val("");
                });

                $('#valor_diferenciado').click(function () {
                    $("#valor_diferenciado_div").show("slow");
                    $("#valor_fixo_div").hide("slow");
                    $('#valor').val("");
                });

                $('#valor_gratis').click(function () {
                    $("#valor_diferenciado_div").hide("slow");
                    $("#valor_fixo_div").hide("slow");
                    $('#valor_mulher').val("");
                    $('#valor_homem').val("");
                    $('#valor').val("");
                });

                $('#valor_combinar').click(function () {
                    $("#valor_diferenciado_div").hide("slow");
                    $("#valor_fixo_div").hide("slow");
                    $('#valor_mulher').val("");
                    $('#valor_homem').val("");
                    $('#valor').val("");
                });

                $('#radio_video_pc_cel').click(function () {
                    $("#video_pc_cel_div").show("slow");
                    $("#video_youtube_div").hide("slow");
                    $('#video_youtube').val("");
                    $('#div_video_atual').show("slow");
                });

                $('#radio_video_youtube').click(function () {
                    $("#video_youtube_div").show("slow");
                    $("#video_pc_cel_div").hide("slow");
                    $("#div_video_atual").show("slow");
                    $('#video_upload').val("");
                    $('#video_atual').val("");
                    //Este trecho faz com que ele se esconda e ocupe o espaço confortável novamente
                    //$("#video_atual").css("display", "none");

                    //Este bloco reseta todas as propriedades do vídeo, assim o vídeo perde a mini imagem
                    var videoElement = document.getElementById('video_atual');
                    videoElement.pause();
                    videoElement.removeAttribute('src');
                    videoElement.load();
                });

                //Aqui controlo o caso de o usuário não querer vídeo ou nenhum link
                $('#radio_sem_video').click(function () {
                    $("#video_pc_cel_div").hide("slow");
                    $("#video_youtube_div").hide("slow");
                    $('#video_youtube').val("");
                    $('#div_video_atual').hide("slow");
                    $('#video_upload').val("");
                    $('#video_atual').val("");

                    //Este bloco reseta todas as propriedades do vídeo, assim o vídeo perde a mini imagem
                    var videoElementSemVideo = document.getElementById('video_atual');
                    videoElementSemVideo.pause();
                    videoElementSemVideo.removeAttribute('src');
                    videoElementSemVideo.load();
                });
                //Aqui controlo o caso de o usuário não querer vídeo ou nenhum link

                //(FIM) Controlo aqui os valores dos radios
            </script>

            <script>
                $('#faixa_etaria').on('change', function () {
                    var valor_selecionado;
                    valor_selecionado = this.value;

                    if (valor_selecionado == 'Livre') {
                        $('#img_faixa_etaria').attr('src', 'imagem-site/classificacao-livre.png');
                        $('#div_img_faixa_etaria').hide("slow");
                        $('#div_img_faixa_etaria').show("slow");
                    }

                    if (valor_selecionado == '10 Anos') {
                        $('#img_faixa_etaria').attr('src', 'imagem-site/classificacao-10.png');
                        $('#div_img_faixa_etaria').hide("slow");
                        $('#div_img_faixa_etaria').show("slow");
                    }

                    if (valor_selecionado == '12 Anos') {
                        $('#img_faixa_etaria').attr('src', 'imagem-site/classificacao-12.png');
                        $('#div_img_faixa_etaria').hide("slow");
                        $('#div_img_faixa_etaria').show("slow");
                    }

                    if (valor_selecionado == '14 Anos') {
                        $('#img_faixa_etaria').attr('src', 'imagem-site/classificacao-14.png');
                        $('#div_img_faixa_etaria').hide("slow");
                        $('#div_img_faixa_etaria').show("slow");
                    }

                    if (valor_selecionado == '16 Anos') {
                        $('#img_faixa_etaria').attr('src', 'imagem-site/classificacao-16.png');
                        $('#div_img_faixa_etaria').hide("slow");
                        $('#div_img_faixa_etaria').show("slow");
                    }

                    if (valor_selecionado == '18 Anos') {
                        $('#img_faixa_etaria').attr('src', 'imagem-site/classificacao-18.png');
                        $('#div_img_faixa_etaria').hide("slow");
                        $('#div_img_faixa_etaria').show("slow");
                    }
                });
            </script>

        </footer>

    </body>

</html>
