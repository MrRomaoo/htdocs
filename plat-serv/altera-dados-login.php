<?php
include('verifica-session.php');
?>

<html>

    <head>

        <?php
        include("head.html");

        $recebePrestador = $_SESSION['prestador'];
        $recebeSenhaPrestador = $_SESSION['senha'];
        ?>

    </head>

    <body>

        <?php include("body-nav-bar.php"); ?>

        <div class="container quebra_linha">

            <span style="display:block; height: 15px;"></span>
            <span style="display:block; weight: 5px;"></span>

            <div class="row">
                <div class="col-lg-8 mb-4">

                    <form name="sentMessage" id="contactForm"  method="post" action="confirma-altera-dados-login.php">

                        <label><b>Alterar minhas informações de login:</b></label>

                        <label class="text-danger">Em alguns casos é preciso relogar para que as alterações tenham efeito.</label>

                        <div class="control-group form-group">
                            <div class="controls">
                                <label>Login: <span style="color:red;font-weight:bold">*</span></label>
                                <input type="text" class="form-control" id="prestador" name="prestador" value="<?php echo $recebePrestador; ?>" required>
                            </div>
                        </div>

                        <div class="control-group form-group">
                            <div class="controls">
                                <label>Senha: <span style="color:red;font-weight:bold">*</span></label>
                                <input type="password" class="form-control" id="senha" name="senha" value="<?php echo $recebeSenhaPrestador; ?>" required>
                            </div>
                        </div>

                        <div class="control-group form-group">
                            <div class="controls">
                                <button  id="gravar" name="gravar" type="submit" value="send" class="btn btn-dark-green btn-sm"
                                         onclick=" if (document.getElementById('titulo_negocio').value != '' && document.getElementById('prestador_perfil').value != '' && document.getElementById('senha').value != '') {
                                                     waitingDialog.show();
                                                     setTimeout(function () {
                                                         waitingDialog.hide();
                                                     }, 100000000000)
                                                 }">
                                    <i class="fa fa-save" aria-hidden="true"></i> Gravar</button>
                            </div>
                        </div>	

                    </form>

                </div>

            </div>

        </div>

        <footer>

            <?php include("footer.html"); ?>

            <script src="../baile-de-favela/assets/mdb-table-pagination/js/loading-modal.js"></script>

        </footer>

    </body>

</html>
