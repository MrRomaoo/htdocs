        $(document).ready(function () {

            $('#myTable').DataTable({

                "sorting": false, // false to disable sorting (or any other option)

                "language": {
                    "sProcessing": "Carregando...",
                    "sLengthMenu": "Mostrar até _MENU_ registros",
                    "sZeroRecords": "Nenhum resultado",
                    "sEmptyTable": "Nenhum registro encontrado",
                    "sInfo": "Mostrando registros de _START_ a _END_ de um total de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando registros de 0 a 0 de um total de 0 registros",
                    "sInfoFiltered": "(filtrado de um total de _MAX_ registros)",
                    "sInfoPostFix": "",
                    "sSearch": "Refinar resultados:",
                    "sUrl": "",
                    "sInfoThousands": ",",
                    "sLoadingRecords": "Carregando...",
                    "oPaginate": {
                        "sFirst": "Primero",
                        "sLast": "Último",
                        "sNext": "Seguinte",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending": ": Ativar para ordenar a columna de maneira ascendente",
                        "sSortDescending": ": Ativar para ordenar a columna de maneira descendente"
                    }
                },

                //Esta função é responsável por fazer a rolagem de todo o conteúdo, para cima quando o item da página é clicado
                drawCallback: function () {
                    $('#myTable').on('page.dt', function () {
                        $('html, body').animate({
                            'scrollTop': $("#divTable").position().top
                        });
                    });
                }
            });

            $('.dataTables_length').addClass('bs-select');

        });

                $(document).ready(function () {
                    $('#myTable').dataTable();
                });