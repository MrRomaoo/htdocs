<html>

<?php 
$textoModal="";

function mensagemModalPhp($recebeTituloModal,$recebeTextoModal){
    
    echo '<div class="modal fade" id="exampleModalPhp" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">'.$recebeTituloModal.'</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
            '.$recebeTextoModal.'
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-blue-grey" data-dismiss="modal">'."Fechar".'</button>
            </div>
        </div>
    </div>
</div>';
    
  echo "<script>
           $('#exampleModalPhp').modal().withTimeout(1000);
            </script>";
}


function mensagemModalPhpPrestadorEmailExiste($recebeTituloModal,$recebeTextoModal){
    
    echo '<div class="modal fade" id="exampleModalPhp" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">'.$recebeTituloModal.'</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
            '.$recebeTextoModal.'
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-blue-grey" data-dismiss="modal" onclick="voltaAlteraDadosLogin()" >'."Fechar".'</button>
            </div>
        </div>
    </div>
</div>';
    
  echo "<script>
           $('#exampleModalPhp').modal().withTimeout(1000);
            </script>";
}


function mensagemModalPhpEmailExiste($recebeTituloModal,$recebeTextoModal){
    
    echo '<div class="modal fade" id="exampleModalPhp" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">'.$recebeTituloModal.'</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
            '.$recebeTextoModal.'
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-blue-grey" data-dismiss="modal" onclick="voltaAlteraDadosLogin()" >'."Fechar".'</button>
            </div>
        </div>
    </div>
</div>';
    
  echo "<script>
           $('#exampleModalPhp').modal().withTimeout(1000);
            </script>";
}


function mensagemModalPhpFechaEmail($recebeTituloModal,$recebeTextoModal){
    
    echo '<div class="modal fade" id="exampleModalPhp" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">'.$recebeTituloModal.'</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
            '.$recebeTextoModal.'
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-blue-grey" onclick="pagInicial()">'."Ok".'</button>
            </div>
        </div>
    </div>
</div>';
    
  echo "<script>
           $('#exampleModalPhp').modal().withTimeout(1000);
          
function pagInicial(){
window.location.replace('index.php');
}

</script>";
}


function mensagemModalPhpVoltaDenuncia($recebeTituloModal,$recebeTextoModal){
    
    echo '<div class="modal fade" id="exampleModalPhp" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">'.$recebeTituloModal.'</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
            '.$recebeTextoModal.'
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-blue-grey" onclick="voltaDenuncia()">'."Ok".'</button>
            </div>
        </div>
    </div>
</div>';
    
  echo "<script>
$('#exampleModalPhp').modal().withTimeout(1000);

function pagInicial(){
window.location.replace('index.php');
}

</script>";
}


function mensagemModalPhpCadastroSucesso($recebeTituloModal,$recebeTextoModal){
    
    echo '<div class="modal fade" id="exampleModalPhp" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">'.$recebeTituloModal.'</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
            '.$recebeTextoModal.'
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-blue-grey" onclick="voltaLogin()">'."Ok".'</button>
            </div>
        </div>
    </div>
</div>';
    
  echo "<script>
$('#exampleModalPhp').modal().withTimeout(1000);

function voltaLogin(){
window.location.replace('login.php');
}

</script>";
}

?>
        
</html>
