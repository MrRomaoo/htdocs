<?php
include('verifica-session.php');
?>

<html>

    <head>
        <?php
        include("head.html");
        include("mensagem-modal.php");
        include("mensagem-modal-php.php");
        include('conecta-banco.php');

        $servername = "mysql.hostinger.com.br";
        $username = "u457194965_bf";
        $password = "24458149/*-";
        $dbname = "u457194965_bf";

        $recebeCodPrestador = $_SESSION["cod_prestador"];

        $pasta_imagem = "imagem-upload-perfil/";

        //Preenche os inputs
        $sql = $conn->query("SELECT  prestador, email, senha, data_cadastro, titulo_negocio, interesse_1, interesse_2, interesse_3, interesse_4, interesse_5, 
                                                link_imagem, mensagem_visitantes, pais, rede_social, telefone, tipo_imagem, titulacao		
                                                FROM tbl_prestador
                                                WHERE cod_prestador =" . $recebeCodPrestador);

        if ($sql) {

            while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {

                $recebeDataCadastro = $row['data_cadastro'];
                $recebeTituloNegocio = $row['titulo_negocio'];
                $recebePrestadorPerfil = $row['prestador'];
                $recebeSenhaPrestador = $row['senha'];
                $recebeEmail = $row['email'];
                $recebeInteresse_1 = $row['interesse_1'];
                $recebeInteresse_2 = $row['interesse_2'];
                $recebeInteresse_3 = $row['interesse_3'];
                $recebeInteresse_4 = $row['interesse_4'];
                $recebeInteresse_5 = $row['interesse_5'];
                $recebeLinkImagem = $row['link_imagem'];
                $recebeTipoImagem = $row['tipo_imagem'];
                $recebeMensagemVisitantes = $row['mensagem_visitantes'];
                $recebeRua = $row['pais'];
                $recebeRedeSocial = $row['rede_social'];
                $recebeTelefone = $row['telefone'];
                $recebeTitulacao = $row['titulacao'];
            }
            
        } else {

            mensagemModalPhp("Atenção", "Erro de conexão, tente novamente.");
            
        }

        $sql->connection = null;
        $conn->connection = null;
        
        ?>

    </head>

    <body>

<?php include("body-nav-bar.php"); ?>

        <!-- Page Content -->
        <div class="container quebra_linha">

            <span style="display:block; height: 15px;"></span>
            <span style="display:block; weight: 5px;"></span>

            <!-- Contact Form -->
            <!-- In order to set the email address and subject line for the contact form go to the bin/contact_me.php file. -->
            <div class="row">
                <div class="col-lg-8 mb-4">
                    <h3 class="teal-text">Seu perfil</h3>

                    <form name="sentMessage" id="contactForm"  method="post" action="confirma-edita-perfil.php" enctype="multipart/form-data">

                        <!--Código do usuário em hidden!-->				
                        <input type="hidden" class="form-control" id="cod_prestador" name="cod_prestador" value="<?php echo $recebeCodPrestador; ?>">
                        <input type="hidden" class="form-control" id="link_imagem" name="link_imagem" value="<?php echo $recebeLinkImagem; ?>">
                        <input type="hidden" class="form-control" id="tipo_Imagem" name="tipo_imagem" value="<?php echo $tipoImagem; ?>">

                        <!--Imagem atual-->
                        <div class="control-group form-group">
                            <div class="controls">
                                <label><b>Imagem atual:</b></label>		      
                                <img src="<?php echo $pasta_imagem . $recebeLinkImagem . $recebeTipoImagem; ?>" alt="" id="img_atual" name="img_atual" border="3" height="200" width="200" class="rounded-circle" height="50" width="50"/>
                            </div>
                        </div>		
                        <!--/Imagem atual-->

                        <!--Alterar imagem-->
                        <div class="row">		
                            <div class="col-lg-6">
                                <span class="btn btn-sm btn-mdb-color" onclick="$(this).parent().find('input[type=file]').click();"><i class="fa fa-camera-retro" aria-hidden="true"></i>  Alterar Foto</span>
                                <input name="imagem_upload" id="imagem_upload" onchange="$(this).parent().parent().find('.form-control').html($(this).val().split(/[\\|/]/).pop());" style="display: none;" type="file" accept="image/*">
                            </div>
                        </div>
                        <!--/Alterar imagem-->

                        <div class="control-group form-group">
                            <div class="controls">
                                <label class="text-danger">Em alguns casos é preciso relogar para que as alterações tenham efeito.</label>
                            </div>
                        </div>

                        <div class="control-group form-group">
                            <div class="controls">
                                <label>Seu nome de perfil: <span style="color:red;font-weight:bold">*</span></label>
                                <input type="text" class="form-control" value="<?php echo $recebeTituloNegocio; ?>" minlength="3" maxlength="15" name="titulo_negocio" id="titulo_negocio" required >
                                <p class="help-block"></p>
                            </div>
                        </div>

                        <div class="control-group form-group">
                            <div class="controls">
                                <label>Interesses </label>
                                <select class="form-control" id="interesse_1" name="interesse_1">
                                    <option><?php echo $recebeInteresse_1; ?></option>

<?php
include("interesses.html");
?>

                                </select>	
                            </div>							
                        </div>

                        <div class="control-group form-group">
                            <div class="controls">
                                <label>Interesses </label>
                                <select class="form-control" id="interesse_2" name="interesse_2">
                                    <option><?php echo $recebeInteresse_2; ?></option>

<?php
include("interesses.html");
?>

                                </select>	
                            </div>							
                        </div>

                        <div class="control-group form-group">
                            <div class="controls">
                                <label>Interesses </label>
                                <select class="form-control" id="interesse_3" name="interesse_3">
                                    <option><?php echo $recebeInteresse_3; ?></option>

<?php
include("interesses.html");
?>

                                </select>	
                            </div>							
                        </div>

                        <div class="control-group form-group">
                            <div class="controls">
                                <label>Interesses </label>
                                <select class="form-control" id="interesse_4" name="interesse_4">
                                    <option><?php echo $recebeInteresse_4; ?></option>

<?php
include("interesses.html");
?>

                                </select>	
                            </div>							
                        </div>

                        <div class="control-group form-group">
                            <div class="controls">
                                <label>Interesses </label>
                                <select class="form-control" id="interesse_5" name="interesse_5">
                                    <option><?php echo $recebeInteresse_5; ?></option>

<?php
include("interesses.html");
?>

                                </select>	
                            </div>							
                        </div>

                        <div class="control-group form-group">
                            <div class="controls">
                                <label>Email, Rede social etc...:</label>
                                <input type="text" class="form-control" id="rede_social" name="rede_social" value="<?php echo $recebeRedeSocial; ?>">
                            </div>
                        </div>

                        <div class="control-group form-group">
                            <div class="controls">
                                <label>Telefone:</label>
                                <input type="text" class="form-control" id="telefone" name="telefone" value="<?php echo $recebeTelefone; ?>">
                            </div>
                        </div>

                        <div class="control-group form-group">
                            <div class="controls">
                                <label>Rua:</label>
                                <input type="text" class="form-control" id="pais" name="pais" value="<?php echo $recebeRua; ?>">
                            </div>
                        </div>

                        <div class="control-group form-group">
                            <div class="controls">
                                <label>Mensagem para visitantes: </label>
                                <textarea rows="10" cols="100" class="form-control" id="mensagem_visitantes" name="mensagem_visitantes" style="resize:true" ><?php echo$recebeMensagemVisitantes; ?></textarea>
                            </div>
                        </div>

                        <div class="control-group form-group">
                            <div class="controls">
                                <label>Email para notícias ou redefinição de senha</label>
                                <input  class="form-control" id="email" name="email" required value="<?php echo $recebeEmail; ?>">
                            </div>

                            <br>

                            <div class="control-group form-group">
                                <div class="controls">
                                    <!-- For success/fail messages -->
                                    <button  id="gravar" name="gravar" type="submit" value="send" class="btn btn-sm btn-dark-green"
                                             onclick=" if (document.getElementById('titulo_negocio').value != '' && document.getElementById('prestador_perfil').value != '' && document.getElementById('senha').value != '') {
                                                         waitingDialog.show();
                                                         setTimeout(function () {
                                                             waitingDialog.hide();
                                                         }, 100000000000)
                                                     }">
                                        <i class="fa fa-save" aria-hidden="true"></i> Gravar</button>
                                </div>
                                
                            </div>	

                        </div>
                        
                    </form>
                    
                         <hr>    
                     
            <form action="altera-dados-login.php" method="post">
                                                                                                                                                                            
            <label><b>Alterar minhas informações de login (usuário e senha):</b></label>

            <label class="text-danger">Cuidado ao alterar estas informações, elas serão seus novos login e senha.</label>

            <div class="control-group form-group">
                <div class="controls">
                    <!-- For success/fail messages -->
                    <button  id="gravar" name="gravar" type="submit "value="send" class="btn btn-sm btn-deep-orange"> <i class="fa fa-exclamation-circle" aria-hidden="true"></i> Alterar </button>
                </div>
            </div>	

        </form>
                         
                         <hr>
                         
        <div class="control-group form-group">
            <div class="controls">          
                <button  id="excluir" name="excluir" class="btn btn-sm btn-red"  onclick="deletaPrestador(<?php echo $recebeCodPrestador ?>)"><i class="fa fa-trash" aria-hidden="true"></i> Excluir perfil</button>
            </div>
        </div>
                    
                </div>
                
            </div>
            
        </div>
         
        <footer>

<?php include("footer.html"); ?>

            <script>
                var fileToRead = document.getElementById("imagem_upload");
                fileToRead.addEventListener("change", function (event) {
                    document.getElementById('img_atual').src = window.URL.createObjectURL(this.files[0]);
                }, false);
            </script>

            <script>
                var recebeVal1 = null;

                function deletaPrestador(val1) {

                    recebeVal1 = val1;

                    $('#exampleModalExcluirPrestador').modal().withTimeout(1000);

                }

                function deletaPrestador2() {
                    window.location.href = 'deleta-prestador.php?cod_prestador=' + recebeVal1;
                }
            </script>

            <!--Modal Loading-->    
            <script src="../baile-de-favela/assets/mdb-table-pagination/js/loading-modal.js"></script>

        </footer>

    </body>

</html>
