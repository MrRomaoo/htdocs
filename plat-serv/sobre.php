<html lang="en">

    <head>
        <?php
        include("head.html");
        ?>
    </head>

    <body>

        <?php
        include("body-nav-bar.php");
        ?>

        <header>
        </header>

        <!-- Page Content -->
        <div class="container quebra_linha">

            <!-- Contact Form -->
            <div class="row">

                <div class="col-md-8">

                    <br><br>

                    <!--Excerpt--> 
                    <h5 class="green-text"> O Baile de favela é um grupo seleto que possui a forma mais simples e objetiva possível de divulgar seus projetos e estar em contato com as melhores propostas de negócios e até de lazer.</h5>

                    <!--Grid row-->
                    <div class="row"> 

                        <!--Grid column-->
                        <div class="col-lg-5 col-xl-5 pb-3">
                            <!--Featured image-->
                            <div class="view overlay rounded z-depth-2">
                                <img src="imagem-site/aperta-mao.jpg" alt="Sample image for first version of blog listing" class="img-fluid">
                                <a>
                                    <div class="mask waves-effect waves-light"></div> 
                                </a>
                            </div>
                        </div>
                        <!--Grid column--> 

                        <!--Grid column--> 
                        <div class="col-lg-7 col-xl-7">

                    <p>O funcionamento da plataforma é simples: publique, seja visto(a) e contactado(a).</p>

                        </div>
                        <!--Grid column-->

                    </div>
                    <!--Grid row-->

                    <hr class="mb-5 mt-5 pb-3">

                    <!--Grid row-->
                    <div class="row">

                        <!--Grid column-->
                        <div class="col-lg-5 col-xl-5 pb-3">
                            <!--Featured image-->
                            <div class="view overlay rounded z-depth-2">
                                <img src="imagem-site/como_funciona.jpg" alt="Second image in the first version of blog listing" class="img-fluid">
                                <a>
                                    <div class="mask waves-effect waves-light"></div>
                                </a>
                            </div>
                        </div>
                        <!--Grid column-->

                        <!--Grid column-->
                        <div class="col-lg-7 col-xl-7 pb-3">

                            <h5 class="pink-text">Mas como funciona o grupo? </h5>
                            
                            <p> Aqui você cria uma publicação e a divulga, sendo que o seu tipo de membro influencia no destaque das suas publicações, por exemplo:<br><br>

                                <strong class="blue-text">Membros diamante:</strong> Têm suas publicações mais novas aparecendo no mural principal, aparecem em primeiro nos resultados das buscas, 
                                tem direito a 25 publicações e tem buscas liberadas. (Benefícios vitalícios)<br><br>

                                <strong class="orange-text">Membros ouro:</strong> Não têm suas publicações  aparecendo no mural principal, aparecem após os diamantes nos resultados ( é o segundo tipo de usuário nas buscas), 
                                tem direito a 15 publicações e podem fazer buscas.(Benefícios vitalícios)<br><br>

                                <strong class="">Membros bronze:</strong> Não têm suas publicações no mural principal, aparecem em último nos resultados (após os diamantes e ouro), 
                                tem direito a 5 publicações e podem fazer buscas.(Benefícios vitalícios)<br><br>

                            </p>

                        </div>
                        <!--Grid column-->

                    </div>
                    <!--Grid row-->

                    <hr class="mb-5 mt-4 pb-3">

                    <!--Grid row-->
                    <div class="row pb-5">

                        <!--Grid column-->
                        <div class="col-lg-5 col-xl-5 pb-3">
                            <!--Featured image-->
                            <div class="view overlay rounded z-depth-2">
                                <img src="imagem-site/contatos.jpg" alt="Thrid image in the blog listing." class="img-fluid">
                                <a>
                                    <div class="mask waves-effect waves-light"></div>
                                </a>
                            </div>
                        </div>
                        <!--Grid column-->

                        <!--Grid column-->
                        <div class="col-lg-7 col-xl-7">

                            <h5 class="indigo-text"> Venha conosco e aumente sua rede de contatos, parceiros e investidores!</h5>

                            <div class="col-lg-7 col-xl-7">
                                <p>Suas publicações e projetos serão exibidas para o mundo todo e não somente para um determinado grupo, já 
                                    imaginou quantas propostas você poderá receber? Adquira sua conta e seja um membro deste grupo exclusivo.
                                </p>
                            </div>

                            <p></p>

                        </div>

                    </div>

                </div>

            </div>

        </div>

        <footer>
            <?php include("footer.html"); ?>
        </footer>

    </body>

</html>
