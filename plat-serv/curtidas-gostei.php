<?php

include('conecta-banco.php');
//include('verifica-session.php');
//3° Momento: faço a query que me diz se este usuário já curtiu esta publicação, caso já tenha curtido, o botão fica desabilitado para curtir novamente

$recebeCodPrestadorLogado = $_GET['codPrestador'];
$recebeCodPublicacao = $_GET['codPublicacao'];
$recebeCodPrestadorPublicacao = $_GET['codPrestadorPublicacao'];

$sql = $conn->query("SELECT
        gostei
            FROM 
            tbl_curtidas
                WHERE
                tbl_publicacoes_cod_publicacao=$recebeCodPublicacao
                    AND
                    tbl_prestador_cod_prestador_curtiu=$recebeCodPrestadorLogado");

$verificaTemCurtida = $sql->rowCount();

if ($verificaTemCurtida > 0) {

    while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
        $recebeGosteiSql = $row['gostei'];
    }

    $sql->connection = null;
    $conn->connection = null;
//(FIM)3° Momento: faço a query que me diz se este usuário já curtiu esta publicação, caso já tenha curtido, o botão fica desabilitado para curtir novamente

    if ($recebeGosteiSql == "1") {

        $sql = "UPDATE tbl_curtidas 
                    SET gostei = '0'
                    WHERE 
                       tbl_publicacoes_cod_publicacao=$recebeCodPublicacao
                    AND
                    tbl_prestador_cod_prestador_curtiu=$recebeCodPrestadorLogado";

        $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $stmt = $conn->prepare($sql);
        $stmt->execute();

        $stmt->closeCursor();
        $conn->connection = null;
        $stmt->connection = null;
    } else {

        $sql = "UPDATE tbl_curtidas 
                    SET gostei = '1'
                    WHERE 
                       tbl_publicacoes_cod_publicacao=$recebeCodPublicacao
                    AND
                    tbl_prestador_cod_prestador_curtiu=$recebeCodPrestadorLogado;
                        
                UPDATE tbl_curtidas 
                    SET nao_gostei = '0'
                    WHERE 
                       tbl_publicacoes_cod_publicacao=$recebeCodPublicacao
                    AND
                    tbl_prestador_cod_prestador_curtiu=$recebeCodPrestadorLogado";

        $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $stmt = $conn->prepare($sql);
        $stmt->execute();

        $stmt->closeCursor();
        $conn->connection = null;
        $stmt->connection = null;
    }
} else {

    $sql = "INSERT INTO tbl_curtidas 
                        (cod_curtidas,
                        tbl_publicacoes_cod_publicacao, 
                        tbl_prestador_cod_prestador_curtiu,
                        tbl_prestador_cod_dono_publicacao,
                        gostei,
                        nao_gostei)
                        VALUES
                        (null, $recebeCodPublicacao, $recebeCodPrestadorLogado,$recebeCodPrestadorPublicacao,1,0)";

    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $stmt = $conn->prepare($sql);
    $stmt->execute();

    $stmt->closeCursor();
    $conn->connection = null;
    $stmt->connection = null;
}

$sql = $conn->query("SELECT 
    SUM(tbl_curtidas.gostei) AS gostei
    FROM
    tbl_curtidas
 WHERE
                tbl_publicacoes_cod_publicacao=$recebeCodPublicacao");

while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
    $recebeGostei = $row['gostei'];
}

$conn->connection = null;

echo $recebeGostei;
?>