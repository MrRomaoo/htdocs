<html >

    <head>
        <?php
        include("head.html");
        ?>
    </head>

    <body>

        <?php
        include("body-nav-bar.php");
        ?>

        <header>
        </header>

        <div class="container quebra_linha ">
               <br>
            <div class="row">

                <h2 class="brown-text">Nosso código de honra</h2>
                <br><br><br>

                
           <section>
            <blockquote class="blockquote bq-primary">
                <h5 class="blue-text">Respeito ao próximo</h5>
                <p>Todos os membros desse grupo devem a cima de tudo, respeitar uns aos outros, pois o respeito é um dos valores mais importantes 
                   que o ser humano pode ter e é a base para o bom convívio social, respeite o espaço do próximo para que o seu espaço seja respeitado.
                </p>
            </blockquote>
        </section>
                       
                
        <section>
            <blockquote class="blockquote bq-primary">
                <h5 class="blue-text">Privacidade</h5>
                <p> Nesta plataforma você terá grandes chances de fechar um grande negócio, pois aqui se encontra um grupo seleto de usuários (este é um
                    dos motivos da plataforma ser paga). Contatos, fotos e informações serão trocadas aqui, e isto é o nosso patrimônio, o nosso valor está em sermos diferenciados, por tanto, 
                    só divulgue, compartilhe ou dissemine o que você tiver autorização, evite problemas com o dono do material, pois ele(a) pode não gostar do fato de suas  publicações irem para alguma rede
                    social por exemplo. Respeite a matilha sempre!
                </p>
            </blockquote>
        </section>
                
                
           <section>
            <blockquote class="blockquote bq-primary">
                <h5 class="blue-text">Pornografia, pedofilia, ofensas à qualquer raça, sexo, religião ou direitos humanos.</h5>
                <p> É dever de toda matilha lutar contra isto, tire imediatamente um print da tela e envie para o nosso e-mail, o caso 
                    será analisado e confirmadas as suspeitas o banimento permanente do usuário no grupo será aplicado.
                </p>
            </blockquote>
        </section>

                
         <section>
            <blockquote class="blockquote bq-primary">
               <h5 class="blue-text">União da matilha</h5>
                <p> Lembre-se, você precisa dos outros e os outros precisam de você, a união faz a força..</p>
            </blockquote>
        </section>

        </div>
       </div>

        <footer>
            <?php include("footer.html"); ?>
        </footer>

    </body>

</html>