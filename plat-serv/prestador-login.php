<?php
include('verifica-session.php');
?>

<html>

    <head>

        <?php
        include("head.html");
        include('conecta-banco.php');

        $pasta_imagem = "imagem-upload-publicacoes/";
        $pasta_imagem_perfil = "imagem-upload-perfil/";
        $recebeCodPrestador = $_SESSION['cod_prestador'];
        $recebeTitulacao = $_SESSION['titulacao'];

        // 1° Momento - Aqui eu trago as informações do usuário
        $sql = $conn->query("SELECT link_imagem, tipo_imagem, titulacao, titulo_negocio, qtd_publicacoes
        FROM tbl_prestador 
        WHERE
        cod_prestador=$recebeCodPrestador");

        while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
            $recebeLinkImagemPerfil = $row['link_imagem'];
            $recebeTipoImagemPerfil = $row['tipo_imagem'];
            $recebeTitulacao = $row['titulacao'];
            $recebeTituloNegocio = $row['titulo_negocio'];
            $recebeQtdPublicacoes = $row['qtd_publicacoes'];
        }
        
        $sql->connection = null;
        $conn->connection = null;

        $maxPublicacoes = 0;
        $recebeTitulacao = $_SESSION['titulacao'];

        if ($recebeTitulacao == "D") {
            $maxPublicacoes = 25;
            /* $publicacoesRestantes = $maxPublicacoes - intval($recebeQtdPublicacoes); */
        }

        if ($recebeTitulacao == "O") {
            $maxPublicacoes = 15;
            /* $publicacoesRestantes = $maxPublicacoes - intval($recebeQtdPublicacoes); */
        }

        if ($recebeTitulacao == "S") {
            $maxPublicacoes = 5;
            /* $publicacoesRestantes = $maxPublicacoes - intval($recebeQtdPublicacoes); */
        }


        // 2° Momento - Aqui eu trago as informações das publicações deste usuário
        $sql = $conn->query("SELECT
                                tbl_publicacoes.cod_publicacao,
                                tbl_publicacoes.cod_prestador_publicacao,
                                tbl_prestador.titulo_negocio AS titulo_negocio_perfil,
                                tbl_publicacoes.descricao,
                                tbl_publicacoes.faixa_etaria,
                                tbl_publicacoes.rua,
                                tbl_publicacoes.rua,
                                tbl_publicacoes.numero,
                                tbl_publicacoes.complemento,
                                tbl_publicacoes.bairro,
                                tbl_publicacoes.cidade,
                                tbl_publicacoes.estado,
                                tbl_publicacoes.cep,
                                tbl_publicacoes.pais,
                                tbl_publicacoes.referencia,
                                tbl_publicacoes.contato,
                                tbl_publicacoes.data_do_evento,
                                tbl_publicacoes.horario_do_evento,
                                tbl_publicacoes.categoria_publicacao,
                                tbl_publicacoes.valor,
                                tbl_publicacoes.valor_homem,
                                tbl_publicacoes.valor_mulher,
                                SUM(tbl_curtidas.gostei) AS gostei,
                                SUM(tbl_curtidas.nao_gostei) AS nao_gostei,
                                DATE_FORMAT(tbl_publicacoes.data_hora_publicacao,'%d/%m/%Y %H:%i:%S') AS data_hora_publicacao,
                                tbl_publicacoes.link_imagem,
                                tbl_publicacoes.tipo_imagem,
                                tbl_publicacoes.visualizacoes,
                                tbl_prestador.link_imagem AS link_imagem_perfil,
                                tbl_prestador.tipo_imagem AS tipo_imagem_perfil,
                                tbl_prestador.titulacao AS titulacao,
                                tbl_publicacoes.titulo_negocio,
                                tbl_publicacoes.categoria_publicacao
                                    FROM
                                    tbl_publicacoes
                                    INNER JOIN tbl_prestador ON tbl_prestador.cod_prestador = tbl_publicacoes.cod_prestador_publicacao
                                    INNER JOIN tbl_curtidas ON tbl_curtidas.tbl_publicacoes_cod_publicacao = tbl_publicacoes.cod_publicacao
                                        WHERE
                                        tbl_curtidas.tbl_prestador_cod_dono_publicacao = $recebeCodPrestador
                                        GROUP BY
                                            tbl_publicacoes.cod_publicacao
                                                ORDER BY tbl_publicacoes.data_hora_publicacao 
                                                    DESC");

        $sql->connection = null;
        $conn->connection = null;
        ?>

    </head>

    <body>

        <?php include("body-nav-bar.php"); ?>

        <!-- Page Content -->
        <div class="container quebra_linha">

            <!-- Page Heading/Breadcrumbs -->
            <h3 class="teal-text" >Seja bem vindo(a) <?php echo $recebeTituloNegocio ?>!</h3>

            <!--Imagem atual-->
            <div class="control-group form-group">
                <div class="controls">
                    <label><b>Foto atual:</b></label>		      
                    <img src="<?php echo $pasta_imagem_perfil . $recebeLinkImagemPerfil . $recebeTipoImagemPerfil; ?>" alt="" id="img_atual" name="img_atual" border="3" height="200" width="200" class="rounded-circle"/>
                </div>
            </div>	

            <?php if ($recebeTitulacao == 'D') { ?>
                <div class="row">		
                    <div class="col-lg-6">
                        <label class="text-diamond-carrossel"><b><?php /* echo "Membro Diamante" */ ?></b></label>
                    </div>
                </div>
            <?php } ?>

            <?php if ($recebeTitulacao == 'O') { ?>
                <div class="row">		
                    <div class="col-lg-6">
                        <label class="text-gold"><b><?php /* echo "Membro Ouro" */ ?></b></label>
                    </div>
                </div>
            <?php } ?>

            <?php if ($recebeTitulacao == 'S') { ?>
                <div class="row">		
                    <div class="col-lg-6">
                        <label ><?php /* echo "Membro comum" */ ?></label>
                    </div>
                </div>
            <?php } ?>

            <form method="post" action="edita-perfil.php">
                <input type="hidden" class="form-control" id="cod_prestador" name="cod_prestador" value="<?php echo $recebeCodPrestador; ?>">
                <!-- Intro Content -->
                <div class="row">		
                    <div class="col-lg-6">
                        <button type="submit" class="btn btn-blue btn-sm" ><i class="fa fa-user-o" aria-hidden="true"></i> Meu Perfil</button>
                    </div>
                </div>
            </form>

            <!-- Intro Content -->
            <div class="row">		
                <div class="col-lg-6">
                    <button type="button" class="btn btn-green btn-sm" onClick="inserePublicacao()"><i class="fa fa-plus-square" aria-hidden="true"></i> Nova Publicação</button>
                </div>
            </div>
            <!-- /.row -->

            <!-- /.row -->
            <h1 class="my-4"></h1>
            <h1 class="my-4"></h1>
            <h1 class="my-4"></h1>

            <!--<h1 id="divTable" >  <?php /* echo "Minhas publicações:" . $publicacoesRestantes . " restantes de " . $maxPublicacoes; */ ?></h1>-->

            <h3 id="divTable" > </h3>

            <!--Tabela resultados-->
            <div class="container quebra_linha">
                <table id="myTable" class="table table-sm quebra_linha" width="100%" >
                    <thead>  
                        <tr>  
                            <th></th>  
                            <th></th>
                        </tr>  
                    </thead>  
                    <tbody>  

                        <?php
                        while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                            ?> 

                            <tr  align="left" >

                                <td onclick="edtPubl(<?php echo $row['cod_publicacao'] ?>,<?php echo $row['cod_prestador_publicacao'] ?>)" width="90%" >

                                    <a><img  src="<?php echo $pasta_imagem . $row['link_imagem'] . $row['tipo_imagem'] ?>" height="120" width="120" class="img-fluid rounded mb-4"/> </a>           

                                    <a><h4><b><?php echo $row['titulo_negocio']; ?></b></h4></a>

                                    <?php
                                    echo '<h6 .ml-3 ><b>' . $row['visualizacoes'] . ' visualizações</b>' . "</h6>";
                                    echo $row['gostei'] . '<i class="fa fa-thumbs-o-up ml-2" aria-hidden="true" fa-lg></i> ' . $row['nao_gostei'] . '<i class="fa fa-thumbs-o-down ml-2" aria-hidden="true" fa-lg></i>';

                                    // Trago aqui a cidade e o bairro
                                    if (trim($row['cidade']) != '' && trim($row['bairro']) != '') {
                                        echo '<h6>' . $row['cidade'] . ', ' . $row['bairro'];
                                    }
                                    //(FIM)Trago aqui a cidade e o bairro
                                    // Tratamento categoria
                                    echo "<h6>Categoria: " . $row['categoria_publicacao'] . "</h6>";
                                    // (FIM) Tratamento categoria
                                    // Tratamento do valor
                                    if ($row['valor_homem'] != "" || $row['valor_mulher'] != "") {
                                        echo '<i class="fa fa-male fa-2x indigo-text" aria-hidden="true"></i> ' . $row['valor_homem'] . '     ' . '<i class="fa fa-female fa-2x red-text" aria-hidden="true"></i> ' . $row['valor_mulher'];
                                    } else {
                                        echo "<h6>" . "R$ " . $row['valor'] . "</h6>";
                                    }
                                    // (FIM)Tratamento do valor

                                    echo '<h6>';

                                    // Tratamento descricao
                                    echo '<h6>' . substr($row['descricao'], 0, 35) . '...<a><b class="text-primary">Mais detalhes</b></a></h6>';
                                    // (FIM) Tratamento descricao

                                    echo '<h6>';

                                    if (trim($row['contato']) != '') {
                                        echo '<h6>' . $row['contato'] . '</h6>';
                                    }

                                    echo '<h6>' . 'Publicado em: ' . $row['data_hora_publicacao'] . '</h6>';
                                    ?>

                                    <!-- Tomar cuidado com o H6 pois ele contém toda a informação, incluindo o PHP!!!!!-->
                                    <h6>
                                        <a href="perfil.php?cod_prestador=<?php echo $recebeCodPrestador ?>" > 
                                            Publicado por: <b class="text-primary"><?php echo $row['titulo_negocio_perfil'] ?></b>
                                            <img  class="rounded-circle" height="50" width="50" src="<?php echo $pasta_imagem_perfil . $row['link_imagem_perfil'] . $row['tipo_imagem_perfil'] ?>" width="50" height="50">
                                        </a>

    <?php if ($row['titulacao'] == 'D') { ?>
                                            <label class="text-diamond-carrossel"><b><?php /* echo "Membro Diamante" */ ?></b></label>
                                        <?php } ?>

                                        <?php if ($row['titulacao'] == 'O') { ?>
                                            <label class="text-gold"><b><?php /* echo "Membro Ouro" */ ?></b></label>
                                        <?php } ?>

                                        <?php if ($row['titulacao'] == 'S') { ?>
                                            <label ><?php /* echo "Membro comum" */ ?></label>
                                        <?php } ?>

                                    </h6>

                                    <a><i class="fa fa-edit fa-2x text-secondary"></i></a>

                                </td>

                                <td width="10%">
                                    <!--Uma td diferente que não possui o método onclick-->
                                    <a  data-toggle="modal" data-target="#exampleModal" onclick="recebeCod_E_TipoImagem(<?php echo $row['cod_publicacao'] ?>, '<?php echo $row['tipo_imagem'] ?>')" ><i class="fa fa-trash fa-2x text-danger"></i></a>
                                </td>
                            </tr>

    <?php
}
?>

                    </tbody>  
                    <tfoot>  
                        <tr>  
                            <th></th>  
                            <th></th>  
                        </tr>  
                    </tfoot>  
                </table>  
            </div>
            <!--(FIM) Tabela resultados-->
            
            <!-- Modal -->
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Atenção!</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            Deseja realmente excuir esta publicação?
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-blue-grey" data-dismiss="modal">Não</button>
                            <button type="button" onclick="deletaPublicacao()" class="btn btn-danger">Sim</button>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <span style="display:block; height: 150px;"></span>

        <footer>

<?php include("footer.html"); ?>
            <!-- Carrego aqui a tabela resultados e suas configurações -->
            <script src="../baile-de-favela/assets/mdb-table-pagination/js/tabela-resultados.js"></script>

            <script>
                                function inserePublicacao() {
                                    var linkIns = 'insere-publicacao.php';
                                    window.location.href = linkIns;
                                }
            </script>

            <script>
                function edtPubl(val1, val2) {
                    window.location.href = 'edita-publicacao.php?cod_publicacao=' + val1 + '&cod_prestador=' + val2;
                }
            </script>

            <script>
                var cod = null
                var tipo_imagem = null;

                function recebeCod_E_TipoImagem(recebeVal1, recebeVal2) {
                    cod = recebeVal1;
                    tipo_imagem = recebeVal2;
                }

                function deletaPublicacao(recebeVal1, recebeVal2) {
                    window.location.href = 'deleta-publicacao.php?cod_publicacao=' + cod + '&tipo_imagem=' + tipo_imagem;
                }
            </script>


        </footer>

    </body>

</html>