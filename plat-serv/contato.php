    <head>
        <?php
        include("head.html");
        include("mensagem-modal-php.php");
        ?>

        <?php
        
        if ($_SERVER["REQUEST_METHOD"] == "POST") {

            $nome = $_POST["nome"];
            $email = $_POST["email"];
            $assunto = $_POST["assunto"];
            $mensagem = $_POST["mensagem"];

            //senha - 89gigabyte/*-
            $to = "contato.megavirtua@hotmail.com";
            $subject = $nome;
            $txt = $mensagem;
            $headers = "From: " . $email . "\r\n";

            mail($to, $subject, $txt, $headers);
            
            mensagemModalPhp("Mensagem", "Sua mensagem foi enviada! Dentro de 72 entraremos em contato.");
            
        }
        ?>

    </head>

    <body>

        <?php
        include("body-nav-bar.php");
        ?>

        <div class="container quebra_linha">

            <div class="row">
                <div class="col-lg-8 mb-4">

                    <form name="formContato" id="formLogin" method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">	

             <h3 class="brown-text">Alpha club - A matilha</h3> 

                        <section>
                            <div class="card" style="width: 20rem;">

                                <img class="card-img-top" src="http://megavirtua.com/baile-de-favela/imagem-site/emblema_card.jpg" alt="Card image cap">

                                <div class="card-body">

                                    <div class="control-group form-group">
                                        <div class="controls">
                                            <h5 class="card-title"><a>Nome</a></h5>
                                            <input type="text" class="form-control" id="nome"  name="nome" required>
                                            <p class="help-block"></p>
                                        </div>
                                    </div>

                                    <div class="control-group form-group">
                                        <div class="controls">
                                            <h5 class="card-title"><a>Seu e-mail</a></h5>
                                            <input type="email" class="form-control" id="email" name="email" required>
                                        </div>
                                    </div>

                                    <div class="control-group form-group">
                                        <div class="controls">
                                            <h5 class="card-title"><a>Assunto</a></h5>
                                            <input type="text" class="form-control" id="assunto" name="assunto" >
                                        </div>
                                    </div>

                                    <div class="control-group form-group">
                                        <div class="controls">
                                            <h5 class="card-title"><a>Mensagem</a></h5>

                                            <textarea class="form-control" id="mensagem" name="mensagem" required></textarea>
                                        </div>
                                    </div>

                                    <button type="submit" class="btn btn-sm btn-unique"><i class="fa fa-sign-in" aria-hidden="true"></i>  Enviar</button>

                                </div>

                            </div>
                        </section>

                    </form>

                </div>
                
            </div>
            
        </div>
        
<br><br><br><br><br><br><br><br><br><br><br><br>

        <footer>

            <?php
            include("footer.html");
            ?>
        </footer>

    </body>

</html>