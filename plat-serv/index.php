
<!DOCTYPE html>
<html>

    <head>                    

        <?php
        include ("head.html");
        include ('conecta-banco.php');
        include ("mensagem-modal.php"); 
        ?>

        <?php

        $pasta_imagem = "imagem-upload-publicacoes/";
        $pasta_imagem_perfil = "imagem-upload-perfil/";

        $sql = $conn->query("SELECT
    tbl_prestador.titulo_negocio AS titulo_negocio_perfil,
    tbl_prestador.link_imagem AS link_imagem_perfil,
    tbl_prestador.tipo_imagem AS tipo_imagem_perfil,
    tbl_prestador.titulacao AS titulacao,
    tbl_publicacoes.cod_publicacao,
    tbl_publicacoes.cod_prestador_publicacao,
    tbl_publicacoes.titulo_negocio,
    tbl_publicacoes.categoria_publicacao,
    tbl_publicacoes.descricao,
    tbl_publicacoes.valor,
    tbl_publicacoes.rua,
    tbl_publicacoes.contato,
    tbl_publicacoes.data_hora_publicacao AS data_hora_publicacao,
    tbl_publicacoes.link_imagem,
    tbl_publicacoes.tipo_imagem
FROM
    `tbl_publicacoes`
INNER JOIN tbl_prestador ON tbl_publicacoes.cod_prestador_publicacao = tbl_prestador.cod_prestador
INNER JOIN tbl_publicacoes_carrossel ON tbl_publicacoes_carrossel.tbl_publicacoes_cod_publicacao = tbl_publicacoes.cod_publicacao
ORDER BY
    data_hora_publicacao");                                                                                                

        $recebeResultadoCarrossel = array();
        $recebeStroke = "";
        $recebeStrokeTitulo = "";
        $recebeTitulacaoMembro = "";
        $contador = 0;

        while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
            $recebeSqlCarrosselCodigoPublicacao[] = $row['cod_publicacao'];
            $recebeSqlCarrosselLinkImagemFundo[] = $pasta_imagem . $row['link_imagem'] . $row['tipo_imagem'];
            $recebeSqlCarrosselTituloNegocio[] = $row['titulo_negocio'];
            $recebeSqlCarrosselImagemPerfil[] = $row['link_imagem_perfil'] . $row['tipo_imagem_perfil'];
            $recebeSqlCarrosselTituloNegocioPerfil[] = $row['titulo_negocio_perfil'];
            $recebeSqlCarrosselTitulacao[] = $row['titulacao'];
        }

        $sql->connection = null;
        $conn->connection = null;
        ?>	

    </head>  

    <body>

        <!-- Navigation -->
        <?php include("body-nav-bar.php"); ?>
        <!-- Navigation -->

        <header> 
            <h1 class="brown-text" >Destaques:</h1>
        </header>

        <!-- Page Content -->
        <div class="quebra_linha">

            <!--Carousel Wrapper-->
            <div id="carousel-example-2" class="carousel slide carousel-fade" data-ride="carousel">
                <!--Indicators-->
                <ol class="carousel-indicators">
                    <li data-target="#carousel-example-2" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-example-2" data-slide-to="1"></li>
                    <li data-target="#carousel-example-2" data-slide-to="2"></li>
                    <li data-target="#carousel-example-2" data-slide-to="3"></li>
                    <li data-target="#carousel-example-2" data-slide-to="4"></li>
                    <!--
                    <li data-target="#carousel-example-2" data-slide-to="5"></li>
                    <li data-target="#carousel-example-2" data-slide-to="6"></li>
                    <li data-target="#carousel-example-2" data-slide-to="7"></li>
                    <li data-target="#carousel-example-2" data-slide-to="8"></li>
                    <li data-target="#carousel-example-2" data-slide-to="9"></li>-->
                </ol>
                <!--/.Indicators-->
                <!--Slides-->
                <div class="carousel-inner" role="listbox">

                    <?php
                    /*if ($recebeSqlCarrosselTitulacao[$contador] == "D") {
                        $recebeStroke = "text-diamond-carrossel";
                        $recebeStrokeTitulo = "text-diamond-carrossel-titulo";
                        $recebeTitulacaoMembro = "Usuário Diamante";
                    } else {
                        $recebeStroke = "text-gold";
                        $recebeStrokeTitulo = "text-gold-carrossel-titulo";
                        $recebeTitulacaoMembro = "Usuário Ouro";
                    }*/
                    ?>
                    <div class="carousel-item-start-bootstrap carousel-item active" style="background-image: url('<?php echo $recebeSqlCarrosselLinkImagemFundo[$contador] ?>')" onclick="maisDetalhes(<?php echo $recebeSqlCarrosselCodigoPublicacao[$contador] ?>)">

                        <div class="carousel-caption" >
                            <div class="fundo-texto-carrossel">
                                <h3 class="h3-responsive <?php echo $recebeStrokeTitulo ?>"><?php echo " " . $recebeSqlCarrosselTituloNegocio[$contador] . " " ?></h3>
                                <p  class="<?php echo $recebeStroke ?>"><?php echo "Publicado por  " ?><img  class="rounded-circle" height="50" width="50" src="<?php echo $pasta_imagem_perfil . $recebeSqlCarrosselImagemPerfil[$contador] ?>" width="50" height="50"><?php echo " " . $recebeSqlCarrosselTituloNegocioPerfil[$contador] ?>
                                    <br>
                                    <?php echo $recebeTitulacaoMembro ?></p>
                            </div>
                        </div>

                    </div>
                    <?php
                    $recebeStroke = "";
                    $recebeStrokeTitulo = "";
                    $contador++;
                    ?>


                    <?php
                    /*if ($recebeSqlCarrosselTitulacao[$contador] == "D") {
                        $recebeStroke = "text-diamond-carrossel";
                        $recebeStrokeTitulo = "text-diamond-carrossel-titulo";
                        $recebeTitulacaoMembro = "Usuário Diamante";
                    } else {
                        $recebeStroke = "text-gold";
                        $recebeStrokeTitulo = "text-gold-carrossel-titulo";
                        $recebeTitulacaoMembro = "Usuário Ouro";
                    }*/
                    ?>
                    <div class="carousel-item-start-bootstrap carousel-item" style="background-image: url('<?php echo $recebeSqlCarrosselLinkImagemFundo[$contador] ?>')" onclick="maisDetalhes(<?php echo $recebeSqlCarrosselCodigoPublicacao[$contador] ?>)">

                        <div class="carousel-caption" >
                            <div class="fundo-texto-carrossel">
                                <h3 class="h3-responsive <?php echo $recebeStrokeTitulo ?>"><?php echo " " . $recebeSqlCarrosselTituloNegocio[$contador] . " " ?></h3>
                                <p  class="<?php echo $recebeStroke ?>"><?php echo "Publicado por  " ?><img  class="rounded-circle" height="50" width="50" src="<?php echo $pasta_imagem_perfil . $recebeSqlCarrosselImagemPerfil[$contador] ?>" width="50" height="50"><?php echo " " . $recebeSqlCarrosselTituloNegocioPerfil[$contador] ?>
                                    <br>
                                    <?php echo $recebeTitulacaoMembro ?></p>
                            </div>
                        </div>

                    </div>
                    <?php
                    $recebeStroke = "";
                    $recebeStrokeTitulo = "";
                    $contador++;
                    ?>


                    <?php
                  /*  if ($recebeSqlCarrosselTitulacao[$contador] == "D") {
                        $recebeStroke = "text-diamond-carrossel";
                        $recebeStrokeTitulo = "text-diamond-carrossel-titulo";
                        $recebeTitulacaoMembro = "Usuário Diamante";
                    } else {
                        $recebeStroke = "text-gold";
                        $recebeStrokeTitulo = "text-gold-carrossel-titulo";
                        $recebeTitulacaoMembro = "Usuário Ouro";
                    }*/
                    ?>
                    <div class="carousel-item-start-bootstrap carousel-item" style="background-image: url('<?php echo $recebeSqlCarrosselLinkImagemFundo[$contador] ?>')" onclick="maisDetalhes(<?php echo $recebeSqlCarrosselCodigoPublicacao[$contador] ?>)">

                        <div class="carousel-caption" >
                            <div class="fundo-texto-carrossel">
                                <h3 class="h3-responsive <?php echo $recebeStrokeTitulo ?>"><?php echo " " . $recebeSqlCarrosselTituloNegocio[$contador] . " " ?></h3>
                                <p  class="<?php echo $recebeStroke ?>"><?php echo "Publicado por  " ?><img  class="rounded-circle" height="50" width="50" src="<?php echo $pasta_imagem_perfil . $recebeSqlCarrosselImagemPerfil[$contador] ?>" width="50" height="50"><?php echo " " . $recebeSqlCarrosselTituloNegocioPerfil[$contador] ?>
                                    <br>
                                    <?php echo $recebeTitulacaoMembro ?></p>
                            </div>
                        </div>

                    </div>
                    <?php
                    $recebeStroke = "";
                    $recebeStrokeTitulo = "";
                    $contador++;
                    ?>


                    <?php
                    /*if ($recebeSqlCarrosselTitulacao[$contador] == "D") {
                        $recebeStroke = "text-diamond-carrossel";
                        $recebeStrokeTitulo = "text-diamond-carrossel-titulo";
                        $recebeTitulacaoMembro = "Usuário Diamante";
                    } else {
                        $recebeStroke = "text-gold";
                        $recebeStrokeTitulo = "text-gold-carrossel-titulo";
                        $recebeTitulacaoMembro = "Usuário Ouro";
                    }*/
                    ?>
                    <div class="carousel-item-start-bootstrap carousel-item" style="background-image: url('<?php echo $recebeSqlCarrosselLinkImagemFundo[$contador] ?>')" onclick="maisDetalhes(<?php echo $recebeSqlCarrosselCodigoPublicacao[$contador] ?>)">

                        <div class="carousel-caption" >
                            <div class="fundo-texto-carrossel">
                                <h3 class="h3-responsive <?php echo $recebeStrokeTitulo ?>"><?php echo " " . $recebeSqlCarrosselTituloNegocio[$contador] . " " ?></h3>
                                <p  class="<?php echo $recebeStroke ?>"><?php echo "Publicado por  " ?><img  class="rounded-circle" height="50" width="50" src="<?php echo $pasta_imagem_perfil . $recebeSqlCarrosselImagemPerfil[$contador] ?>" width="50" height="50"><?php echo " " . $recebeSqlCarrosselTituloNegocioPerfil[$contador] ?>
                                    <br>
                                    <?php echo $recebeTitulacaoMembro ?></p>
                            </div>
                        </div>

                    </div>
                    <?php
                    $recebeStroke = "";
                    $recebeStrokeTitulo = "";
                    $contador++;
                    ?>


                    <?php
                   /* if ($recebeSqlCarrosselTitulacao[$contador] == "D") {
                        $recebeStroke = "text-diamond-carrossel";
                        $recebeStrokeTitulo = "text-diamond-carrossel-titulo";
                        $recebeTitulacaoMembro = "Usuário Diamante";
                    } else {
                        $recebeStroke = "text-gold";
                        $recebeStrokeTitulo = "text-gold-carrossel-titulo";
                        $recebeTitulacaoMembro = "Usuário Ouro";
                    }*/
                    ?>
                    <div class="carousel-item-start-bootstrap carousel-item" style="background-image: url('<?php echo $recebeSqlCarrosselLinkImagemFundo[$contador] ?>')" onclick="maisDetalhes(<?php echo $recebeSqlCarrosselCodigoPublicacao[$contador] ?>)">

                        <div class="carousel-caption" >
                            <div class="fundo-texto-carrossel">
                                <h3 class="h3-responsive <?php echo $recebeStrokeTitulo ?>"><?php echo " " . $recebeSqlCarrosselTituloNegocio[$contador] . " " ?></h3>
                                <p  class="<?php echo $recebeStroke ?>"><?php echo "Publicado por  " ?><img  class="rounded-circle" height="50" width="50" src="<?php echo $pasta_imagem_perfil . $recebeSqlCarrosselImagemPerfil[$contador] ?>" width="50" height="50"><?php echo " " . $recebeSqlCarrosselTituloNegocioPerfil[$contador] ?>
                                    <br>
                                    <?php echo $recebeTitulacaoMembro ?></p>
                            </div>
                        </div>

                    </div>
                    <?php
                    $recebeStroke = "";
                    $recebeStrokeTitulo = "";
                    $contador++;
                    ?>
                    
                                   <?php
                   /* if ($recebeSqlCarrosselTitulacao[$contador] == "D") {
                        $recebeStroke = "text-diamond-carrossel";
                        $recebeStrokeTitulo = "text-diamond-carrossel-titulo";
                        $recebeTitulacaoMembro = "Usuário Diamante";
                    } else {
                        $recebeStroke = "text-gold";
                        $recebeStrokeTitulo = "text-gold-carrossel-titulo";
                        $recebeTitulacaoMembro = "Usuário Ouro";
                    }*/
                    ?>
                    <div class="carousel-item-start-bootstrap carousel-item" style="background-image: url('<?php echo $recebeSqlCarrosselLinkImagemFundo[$contador] ?>')" onclick="maisDetalhes(<?php echo $recebeSqlCarrosselCodigoPublicacao[$contador] ?>)">

                        <div class="carousel-caption" >
                            <div class="fundo-texto-carrossel">
                                <h3 class="h3-responsive <?php echo $recebeStrokeTitulo ?>"><?php echo " " . $recebeSqlCarrosselTituloNegocio[$contador] . " " ?></h3>
                                <p  class="<?php echo $recebeStroke ?>"><?php echo "Publicado por  " ?><img  class="rounded-circle" height="50" width="50" src="<?php echo $pasta_imagem_perfil . $recebeSqlCarrosselImagemPerfil[$contador] ?>" width="50" height="50"><?php echo " " . $recebeSqlCarrosselTituloNegocioPerfil[$contador] ?>
                                    <br>
                                    <?php echo $recebeTitulacaoMembro ?></p>
                            </div>
                        </div>

                    </div>
                    <?php
                    $recebeStroke = "";
                    $recebeStrokeTitulo = "";
                    $contador++;
                    ?>

                </div>
                <!--/.Slides-->
                <!--Controls-->
                <a class="carousel-control-prev" href="#carousel-example-2" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon fundo-texto-carrossel" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carousel-example-2" role="button" data-slide="next">
                    <span class="carousel-control-next-icon fundo-texto-carrossel" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
                <!--/.Controls-->
            </div>
            <!--/.Carousel Wrapper-->

            <div class="container">
                <div class="input-group-btn">
                    <h4 class="blue-text">Gostaria de ter suas publicações aqui em cima?</h4>
                    <a href="planos.php" class="btn btn-unique btn-sm" > <i class="fa fa-hand-o-right" aria-hidden="true"></i>  Ver planos</a>
                </div>

                <h1 class="my-4"></h1>

                <!-- Contact Form -->
                <!-- In order to set the email address and subject line for the contact form go to the bin/contact_me.php file. -->
                <div class="row">

                    <div class="col-lg-8 mb-4">

                        <h2>Pesquisa</h2>

                        <div class="md-form input-group" id="teste">
                            <input type="search" class="form-control" placeholder="Digite aqui..." id="campo_busca" name="campo_busca">
                            <div class="input-group-btn">
                                <!--<button class="btn btn-elegant" onClick="verificaVazio()" type="button" mdbRippleRadius><i class="fa fa-search"  aria-hidden="true"></i>  Procurar!</button>-->
                                <button class="btn btn-blue btn-sm" type="button" onclick="verificaVazio()" mdbRippleRadius><i class="fa fa-search"  aria-hidden="true"></i>  Procurar</button>
                            </div>
                        </div>

                        <h1 class="my-4"></h1>
                        <h1 class="my-4"></h1>
                        <h1 class="my-4"></h1>
                        <h1 class="my-4"></h1>
                        <h1 class="my-4"></h1>

                        <h2>Ou pesquise por categoria</h2>
                        <br>
                        <blockquote class="blockquote bq-primary">

                            <h5><a href="resultado-categoria.php?categoria=funk">Funk</a><h5>
                                       <h5><a href="resultado-categoria.php?categoria=pagode">Pagode</a><h5>  
                                                  <h5><a href="resultado-categoria.php?categoria=samba">Samba</a><h5>  
                                                          <h5><a href="resultado-categoria.php?categoria=bares">Bares</a><h5>  
                                                          <h5><a href="resultado-categoria.php?categoria=restaurantes">Restaurantes</a><h5>  
                                                          <h5><a href="resultado-categoria.php?categoria=cultura">Cultura</a><h5>  
                                                          <h5><a href="resultado-categoria.php?categoria=outros">Outros</a><h5>  

                                                                                                                    </blockquote>

                                                                                                                    </div>

                                                                                                                    </div>

                                                                                                                    </div>

                                                                                                                    <footer>

                                                                                                                        <?php include("footer-sem-trava-undo.html"); ?>

                                                                                                                        <script>
                                                                                                                            function verificaVazio() {
                                                                                                                                var x;
                                                                                                                                x = document.getElementById("campo_busca").value;

                                                                                                                                if (x.trim() != "") {

                                                                                                                                    var link = 'resultados-busca.php?campo_busca=';
                                                                                                                                    window.location.href = link.concat(x);
                                                                                                                                }
                                                                                                                                ;

                                                                                                                                if (x.trim() == "") {

                                                                                                                                    $('#exampleModalCampoVazio').modal();
                                                                                                                                }
                                                                                                                                ;
                                                                                                                            }
                                                                                                                        </script>

                                                                                                                        <script>
                                                                                                                            function maisDetalhes(val) {
                                                                                                                                var link = 'mais-detalhes.php?cod_publicacao=';
                                                                                                                                window.location.href = link.concat(val);
                                                                                                                            }
                                                                                                                        </script>

                                                                                                                        <script>
                                                                                                                            function myFunction() {
                                                                                                                                var x = document.getElementById("teste");
                                                                                                                                if (x.style.display === "none") {
                                                                                                                                    x.style.display = "block";
                                                                                                                                } else {
                                                                                                                                    x.style.display = "none";
                                                                                                                                }
                                                                                                                            }
                                                                                                                        </script>

                                                                                                                        <script type="text/javascript">
                                                                                                                            function showAndroidToast(toast) {
                                                                                                                                Android.showToast(toast);
                                                                                                                            }
                                                                                                                        </script>

                                                                                         
                                                                                                                        <script>
                                                                                                                            //Controla o tempo do carrossel
                                                                                                                            $('.carousel').carousel({
                                                                                                                                interval: 3500
                                                                                                                            })
                                                                                                                        </script>

                                                                                                                     </footer>

                                                                                                                    </body>

                                                                                                                    </html> 