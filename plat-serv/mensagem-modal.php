<!DOCTYPE html>

<html>

<?php 

    echo '<div class="modal fade" id="exampleModalCampoVazio" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">'."Atenção".'</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
            '."Campo vazio, digite algo para buscar.".'
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-blue-grey" data-dismiss="modal">'."Fechar".'</button>
            </div>
        </div>
    </div>
</div>';
    
    
    echo '<div class="modal fade" id="exampleModalExcluir" name="exampleModalExcluir" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">'."Atenção".'</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
               </div>
               <div class="modal-body">
                '."Deseja realmente excluir esta publicação?".'
                </div>
                <div class="modal-footer">
                <button type="button" class="btn btn-blue-grey" data-dismiss="modal" >'."Não".'</button>                
                <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="deletaPublicacao2()" >'."Sim".'</button>
            </div>
        </div>
    </div>
</div>';
 
        
  echo '<div class="modal fade" id="exampleModalExcluirPrestador" name="exampleModalExcluirPrestador" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">'."Atenção".'</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
            '."Deseja realmente excluir o seu usuário? Não será mais possível fazer login com esta conta e tudo o que postou, será excluído, deseja continuar?".'
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-blue-grey" data-dismiss="modal" >'."Não".'</button>                
               <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="deletaPrestador2()" >'."Sim".'</button>
            </div>
        </div>
    </div>
</div>';

        
    echo '<div class="modal fade" id="exampleModalDenuncia" name="exampleModalDenuncia" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">'."Envie sua denúncia".'</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
              '."Deseja denunciar esta publicação? Nos diga o que houve.".'
            </div>
              <div class="modal-body">
                <textarea type="text" class="form-control" id="mensagemDenuncia"  name="mensagemDenuncia" maxlength="1000" minlength="5"></textarea>
              </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-blue-grey" data-dismiss="modal">'."Fechar".'</button>
                <button type="button" class="btn btn-primary" id="enviar" name="enviar" onclick="enviaEmailModal();">'."Enviar".'</button>
            </div>
    </div>
    </div>
</div>';
        
    
?>

</html>
