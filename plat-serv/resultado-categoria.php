<html>

    <head>

        <?php
        include("head.html");
        include('conecta-banco.php');

        $pasta_imagem = "imagem-upload-publicacoes/";
        $pasta_imagem_perfil = "imagem-upload-perfil/";
        $recebeCategoria = $_GET['categoria'];

        // 2° Momento - Aqui eu trago as informações das publicações deste usuário
        $sql = $conn->query("SELECT
                                                        tbl_publicacoes.cod_publicacao,
                                                        tbl_publicacoes.cod_prestador_publicacao,
                                                        tbl_prestador.titulo_negocio AS titulo_negocio_perfil,
                                                        tbl_publicacoes.descricao,
                                                        tbl_publicacoes.faixa_etaria,
                                                        tbl_publicacoes.rua,
                                                        tbl_publicacoes.rua,
                                                        tbl_publicacoes.numero,
                                                        tbl_publicacoes.complemento,
                                                        tbl_publicacoes.bairro,
                                                        tbl_publicacoes.cidade,
                                                        tbl_publicacoes.estado,
                                                        tbl_publicacoes.cep,
                                                        tbl_publicacoes.pais,
                                                        tbl_publicacoes.referencia,
                                                        tbl_publicacoes.contato,
                                                        tbl_publicacoes.data_do_evento,
                                                        tbl_publicacoes.horario_do_evento,
                                                        tbl_publicacoes.categoria_publicacao,
                                                        tbl_publicacoes.valor,
                                                        tbl_publicacoes.valor_homem,
                                                        tbl_publicacoes.valor_mulher,
                                                        SUM(tbl_curtidas.gostei) AS gostei,
                                                        SUM(tbl_curtidas.nao_gostei) AS nao_gostei,
                                                        DATE_FORMAT(tbl_publicacoes.data_hora_publicacao,'%d/%m/%Y %H:%i:%S') AS data_hora_publicacao,
                                                        tbl_publicacoes.link_imagem,
                                                        tbl_publicacoes.tipo_imagem,
                                                        tbl_publicacoes.visualizacoes,
                                                        tbl_prestador.link_imagem AS link_imagem_perfil,
                                                        tbl_prestador.tipo_imagem AS tipo_imagem_perfil,
                                                        tbl_prestador.titulacao AS titulacao,
                                                        tbl_publicacoes.titulo_negocio,
                                                        tbl_publicacoes.categoria_publicacao
                                                            FROM
                                                            tbl_publicacoes
                                                            INNER JOIN tbl_prestador ON tbl_prestador.cod_prestador = tbl_publicacoes.cod_prestador_publicacao
                                                            INNER JOIN tbl_curtidas ON tbl_curtidas.tbl_publicacoes_cod_publicacao = tbl_publicacoes.cod_publicacao
                                                                WHERE
                                                                 tbl_publicacoes.categoria_publicacao ='$recebeCategoria'
                                                                GROUP BY
                                                                    tbl_publicacoes.cod_publicacao
                                                                        ORDER BY tbl_publicacoes.data_hora_publicacao 
                                                                            DESC;");

        $sql->connection = null;
        $conn->connection = null;
        ?>

    </head>

    <body>

        <?php include("body-nav-bar.php"); ?>

        <div class="container quebra_linha">

            <h3 id="divTable" class="teal-text">Resultados para: </h3>

            <h3 id="divTable" >"<?php echo $recebeCategoria ?>" </h3>

            <h1 class="my-4"></h1>

            <table id="myTable" class="table table-sm quebra_linha" width="100%" >
                <thead>  
                    <tr>  
                        <th></th>  
                    </tr>  
                </thead>  
                <tbody>  

                    <?php
                    while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                        ?> 

                        <tr  align="left" >

                            <td onclick="maisDetalhes(<?php echo $row['cod_publicacao'] ?>)" width="90%">

                                <a><img  src="<?php echo $pasta_imagem . $row['link_imagem'] . $row['tipo_imagem'] ?>" height="120" width="120" class="img-fluid rounded mb-4"/> </a>           

                                <a><h4><b><?php echo $row['titulo_negocio']; ?></b></h4></a>

                                <?php
                                echo '<h6 .ml-3 ><b>' . $row['visualizacoes'] . ' visualizações</b>' . "</h6>";
                                echo $row['gostei'] . '<i class="fa fa-thumbs-o-up ml-2" aria-hidden="true" fa-lg></i> ' . $row['nao_gostei'] . '<i class="fa fa-thumbs-o-down ml-2" aria-hidden="true" fa-lg></i>';

                                // Trago aqui a cidade e o bairro
                                if (trim($row['rua']) != '') {
                                    echo '<h6>' . $row['cidade'] . ', ' . $row['bairro'];
                                }
                                //(FIM)Trago aqui a cidade e o bairro
                                // Tratamento categoria
                                echo "<h6>Categoria: " . $row['categoria_publicacao'] . "</h6>";
                                // (FIM) Tratamento categoria
                                // Tratamento do valor
                                if ($row['valor_homem'] != "" || $row['valor_mulher'] != "") {
                                    echo '<i class="fa fa-male fa-2x indigo-text" aria-hidden="true"></i> ' . $row['valor_homem'] . '     ' . '<i class="fa fa-female fa-2x red-text" aria-hidden="true"></i> ' . $row['valor_mulher'];
                                } else {
                                    echo "<h6>" . "R$ " . $row['valor'] . "</h6>";
                                }
                                // (FIM)Tratamento do valor

                                echo '<h6>';

                                // Tratamento descricao
                                echo '<h6>' . substr($row['descricao'], 0, 35) . '...<a><b class="text-primary">Mais detalhes</b></a></h6>';
                                // (FIM) Tratamento descricao

                                echo '<h6>';

                                if (trim($row['contato']) != '') {
                                    echo '<h6>' . $row['contato'] . '</h6>';
                                }

                                echo '<h6>' . 'Publicado em: ' . $row['data_hora_publicacao'] . '</h6>';
                                ?>

                                <!-- Tomar cuidado com o H6 pois ele contém toda a informação, incluindo o PHP!!!!!-->
                                <h6>
                                    <a href="perfil.php?cod_prestador=<?php echo $recebeCodPrestador ?>" > 
                                        Publicado por: <b class="text-primary"><?php echo $row['titulo_negocio_perfil'] ?></b>
                                        <img  class="rounded-circle" height="50" width="50" src="<?php echo $pasta_imagem_perfil . $row['link_imagem_perfil'] . $row['tipo_imagem_perfil'] ?>" width="50" height="50">
                                    </a>

                                    <?php if ($row['titulacao'] == 'D') { ?>
                                        <label class="text-diamond-carrossel"><b><?php /* echo "Membro Diamante" */ ?></b></label>
                                    <?php } ?>

                                    <?php if ($row['titulacao'] == 'O') { ?>
                                        <label class="text-gold"><b><?php /* echo "Membro Ouro" */ ?></b></label>
                                    <?php } ?>

                                    <?php if ($row['titulacao'] == 'S') { ?>
                                        <label ><?php /* echo "Membro comum" */ ?></label>
                                    <?php } ?>

                                </h6>

                            </td>

                        </tr>

                        <?php
                    }
                    ?>

                </tbody>  
                <tfoot>  
                    <tr>  
                        <th></th>  
                    </tr>  
                </tfoot>  
            </table>  

        </div>

        <span style="display:block; height: 350px;"></span>

        <footer>

            <?php include("footer-sem-trava-undo.html"); ?>

            <!-- Carrego aqui a tabela resultados e suas configurações -->
            <script src="../baile-de-favela/assets/mdb-table-pagination/js/tabela-resultados.js"></script>

            <script>
                                function maisDetalhes(val) {
                                    var link = 'mais-detalhes.php?cod_publicacao=';
                                    window.location.href = link.concat(val);
                                }
            </script>

        </footer>

    </body>

</html>