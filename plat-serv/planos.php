<html>

    <head>                    
        <?php
        include("head.html");
        ?>
    </head>  

    <body >
        <!-- Navigation -->
        <?php include("body-nav-bar.php"); ?>
        <!-- Navigation -->

        <header> 

        </header>

        <!-- Page Content -->
        <div class="container quebra_linha" width="10%">

            <!--Conteúdo aqui!!!-->
            <h2 class="main-title brown-text">Para mais destaque em suas publicações faça um upgrade em sua conta.</h2>

            <!-- Grid row -->
            <div class="row">

                <!-- Card deck -->
                <div class="card-deck">

                    <!-- Card -->
                    <div class="card mb-4" >

                        <!--Card image-->
                        <div class="view overlay">
                            <img class="card-img-top" src="imagem-site/diamante3.jpg" alt="Card image cap">
                        </div>

                        <!--Card content-->
                        <div class="card-body ">

                            <!--Title-->
                            <h4 class="card-title light-blue-text">Diamante (Vitalício)</h4>
                            <!--Text-->
                            <h4 class="orange-text">R$10.000,00</h4>

                            <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
                                <input type="hidden" name="cmd" value="_s-xclick">
                                <input type="hidden" name="hosted_button_id" value="3DYZQJHWGW246">
                                <button type="submit" class="btn btn-blue btn-lg btn-sm"><i class="fa fa-cart-plus" aria-hidden="true"></i>  Comprar</button>
                                <img alt="" border="0" src="https://www.paypalobjects.com/pt_BR/i/scr/pixel.gif" width="1" height="1">
                            </form>

                            <ul class="list-group">
                                <li class="list-group-item text-center">Destaque no mural</li>
                                <li class="list-group-item text-center">25 Publicações</li>
                                <li class="list-group-item text-center">Primeiras posições nas buscas</li>
                                <li class="list-group-item text-center">Perfil com TAG Diamante</li>
                                <li class="list-group-item text-center">Busca Publicações</li>
                            </ul>

                        </div>

                    </div>
                    <!-- Card -->

                    <!-- Card -->
                    <div class="card mb-4">

                        <!--Card image-->
                        <div class="view overlay">
                            <img class="card-img-top" src="imagem-site/ouro3.jpg" alt="Card image cap">
                        </div>

                        <!--Card content-->
                        <div class="card-body">

                            <!--Title-->
                            <h4 class="card-title yellow-text">Ouro (Vitalício)</h4>
                            <!--Text-->
                            <h4 class="orange-text">R$5.000,00</h4>

                            <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
                                <input type="hidden" name="cmd" value="_s-xclick">
                                <input type="hidden" name="hosted_button_id" value="SF39LJU8UERUG">
                                <button type="submit" class="btn btn-yellow btn-lg btn-sm"><i class="fa fa-cart-plus" aria-hidden="true"></i> Comprar</button>
                                <img alt="" border="0" src="https://www.paypalobjects.com/pt_BR/i/scr/pixel.gif" width="1" height="1">
                            </form>

                            <ul class="list-group">
                                <li class="list-group-item text-center">Sem destaque no mural</li>
                                <li class="list-group-item text-center">15 Publicações</li>
                                <li class="list-group-item text-center">Segundo grupo das buscas</li>
                                <li class="list-group-item text-center">Perfil com TAG Ouro</li>
                                <li class="list-group-item text-center">Busca Publicações</li>
                            </ul>

                        </div>

                    </div>
                    <!-- Card -->

                    <!-- Card -->
                    <div class="card mb-4">

                        <!--Card image-->
                        <div class="view overlay">
                            <img class="card-img-top" src="imagem-site/membrocomum3.jpg" alt="Card image cap">
                        </div>

                        <!--Card content-->
                        <div class="card-body">

                            <!--Title-->
                            <h4 class="card-title amber-text">Bronze (Vitalício) </h4>
                            <!--Text-->
                            <h4 class="orange-text">R$1.500,00</h4>             

                            <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
                                <input type="hidden" name="cmd" value="_s-xclick">
                                <input type="hidden" name="hosted_button_id" value="JXZTUXT56HD36">
                         <button type="submit" class="btn btn-amber btn-lg btn-sm"><i class="fa fa-cart-plus" aria-hidden="true"></i> Comprar</button>
                                <img alt="" border="0" src="https://www.paypalobjects.com/pt_BR/i/scr/pixel.gif" width="1" height="1">
                            </form>


                            <ul class="list-group">
                                <li class="list-group-item text-center">Sem destaque no mural</li>
                                <li class="list-group-item text-center">5 Publicações</li>
                                <li class="list-group-item text-center">Não há preferência nas buscas</li>
                                <li class="list-group-item text-center">Sem TAG no perfil</li>
                                <li class="list-group-item text-center">Busca Publicações</li>
                            </ul>

                        </div>

                    </div>
                    <!-- Card -->

                </div>
                <!-- Card deck -->

            </div>

            <!--Termina conteúdo!!!-->

        </div>

        <footer>
            <!-- Footer -->
            <?php include("footer.html"); ?>
            <!-- Footer -->
        </footer>

    </body>

</html>