<html>

    <head>

        <?php
        include("head.html");
        include('conecta-banco.php');
        include_once ("mensagem-modal.php");
        ?>

        
        <?php
        $pasta_imagem = "imagem-upload-perfil/";

        $recebeCodPrestador = $_GET['cod_prestador'];

        //Preenche os inputs
        $sql = $conn->query("SELECT  *	
                                                FROM tbl_prestador
                                                WHERE cod_prestador =" . $recebeCodPrestador);

        if ($sql) {

            while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {

                $recebeDataCadastro = $row['data_cadastro'];
                $recebeTituloNegocio = $row['titulo_negocio'];
                $recebePrestador = $row['prestador'];
                $recebeEmail = $row['email'];
                $recebeSenhaPrestador = $row['senha'];
                $recebeInteresse_1 = $row['interesse_1'];
                $recebeInteresse_2 = $row['interesse_2'];
                $recebeInteresse_3 = $row['interesse_3'];
                $recebeInteresse_4 = $row['interesse_4'];
                $recebeInteresse_5 = $row['interesse_5'];
                $recebeLinkImagem = $row['link_imagem'];
                $recebeMensagemVisitantes = $row['mensagem_visitantes'];
                $recebeRua = $row['rua'];
                $recebeRedeSocial = $row['rede_social'];
                $recebeTelefone = $row['telefone'];
                $recebeTipoImagem = $row['tipo_imagem'];
                $recebeTitulacao = $row['titulacao'];
            }
        } else {

            mensagemModalPhp('Atenção', 'Houve um erro, tente novamente.');
            
        }

        $sql->connection = null;
        $conn->connection = null;
        ?>

    </head>

    <body>

        <?php include("body-nav-bar.php"); ?>

        <div class="container quebra_linha">

            <span style="display:block; height: 15px;"></span>
            <span style="display:block; weight: 5px;"></span>

            <div class="row">
                <div class="col-lg-8 mb-4">

                    <form name="sentMessage" id="contactForm"  method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>"enctype="multipart/form-data">
		
                        <input type="hidden" class="form-control" id="cod_prestador" name="cod_prestador" value="<?php echo $recebeCodPrestador; ?>">
                        <input type="hidden" class="form-control" id="prestador" name="prestador" value="<?php echo $recebePrestador; ?>">

                        <div class="control-group form-group">
                            <div class="controls">
                                <h3 class="teal-text" ><?php echo $recebeTituloNegocio; ?></h3>
                                <p class="help-block"></p>
                            </div>
                        </div>

                        <div class="control-group form-group">
                            <div class="controls">	      
                                <img src="<?php echo $pasta_imagem . $recebeLinkImagem . $recebeTipoImagem; ?>" alt="" id="img_atual" name="img_atual" border="3" class="rounded-circle" height="200" width="200" />
                            </div>
                        </div>		

                                <?php if($recebeTitulacao=='D'){?>
                                <label class="text-diamond-carrossel"><b><?php /*echo "Membro Diamante"*/?></b></label>
                                <?php }?>
                                
                                <?php if($recebeTitulacao=='O'){?>
                                <label class="text-gold"><b><?php /*echo "Membro Ouro"*/?></b></label>
                                <?php }?>
                                
                                <?php if($recebeTitulacao=='S'){?>
                                <label ><?php/* echo "Membro comum"*/?></label>
                                <?php }?>
                                
                              <div class="control-group form-group">
                                <div class="controls">
                                    <b class="blue-text">Membro desde: </b>
                                    <p class="text-left"><?php echo $recebeDataCadastro; ?></p>
                                </div>							
                            </div>

                        <div class="control-group form-group">
                            <div class="controls">	                            
                                <button type="button" class="btn btn-sm btn-brown waves-effect px-2" onclick="maisDetalhes(<?php echo $recebeCodPrestador; ?>, '<?php echo $recebeTituloNegocio; ?>')"><i class="fa fa-file-text-o" aria-hidden="true"></i> Posts deste membro</button>
                            </div>
                        </div>	

                        <div class="control-group form-group">

                            <?php if( trim($recebeInteresse_1) != '') { ?>
                            <div class="control-group form-group">
                                <div class="controls">
                                    <b class="blue-text">Interesses </b>
                                    <p class="text-left"><?php echo $recebeInteresse_1; ?></p>
                                </div>							
                            </div>
                        <?php } ?>
                            
                           <?php if( trim($recebeInteresse_2) != '') { ?>
                            <div class="control-group form-group">
                                <div class="controls">
                                    <b class="blue-text">Interesses </b>
                                    <p class="text-left"><?php echo $recebeInteresse_2; ?></p>
                                </div>							
                            </div>
                        <?php } ?>

                    <?php if( trim($recebeInteresse_3) != '') { ?>
                            <div class="control-group form-group">
                                <div class="controls">
                                    <b class="blue-text">Interesses </b>
                                    <p class="text-left"><?php echo $recebeInteresse_3; ?></p>
                                </div>							
                            </div>
                        <?php } ?>

                    <?php if( trim($recebeInteresse_4) != '') { ?>
                            <div class="control-group form-group">
                                <div class="controls">
                                    <b class="blue-text">Interesses </b>
                                    <p class="text-left"><?php echo $recebeInteresse_4; ?></p>
                                </div>							
                            </div>
                        <?php } ?>

                    <?php if( trim($recebeInteresse_5) != '') { ?>
                            <div class="control-group form-group">
                                <div class="controls">
                                    <b class="blue-text">Interesses </b>
                                    <p class="text-left"><?php echo $recebeInteresse_5 ?></p>
                                </div>							
                            </div>
                        <?php } ?>

                    <?php if( trim($recebeRedeSocial) != '') { ?>
                            <div class="control-group form-group">
                                <div class="controls">
                                    <b class="blue-text">Email, Rede social etc... </b>
                                    <p class="text-left"><?php echo $recebeRedeSocial; ?></p>
                                </div>							
                            </div>
                        <?php } ?>

                    <?php if( trim($recebeTelefone) != '') { ?>
                            <div class="control-group form-group">
                                <div class="controls">
                                    <b class="blue-text">Telefone </b>
                                    <p class="text-left"><?php echo $recebeTelefone; ?></p>
                                </div>							
                            </div>
                        <?php } ?>

                           <?php if( trim($recebeRua) != '') { ?>
                            <div class="control-group form-group">
                                <div class="controls">
                                    <b class="blue-text">Rua </b>
                                    <p class="text-left"><?php echo $recebeRua; ?></p>
                                </div>							
                            </div>
                        <?php } ?>

                              <?php if( trim($recebeEmail) != '') { ?>
                            <div class="control-group form-group">
                                <div class="controls">
                                    <b class="blue-text">Email </b>
                                    <p class="text-left"><?php echo $recebeEmail; ?></p>
                                </div>							
                            </div>
                        <?php } ?>

                          <?php if( trim($recebeMensagemVisitantes) != '') { ?>
                            <div class="control-group form-group">
                                <div class="controls">
                                    <b class="blue-text">Sobre </b>
                                    <p class="text-left"><?php echo $recebeMensagemVisitantes; ?></p>
                                </div>							
                            </div>
                        <?php } ?>

                       </div>
                                
                    </form>

            </div>

        </div>

</div>

        <footer>

             <?php include("footer-sem-trava-undo.html"); ?>
            
            <script>
                function maisDetalhes(val1, val2) {
                    var link = 'resultados-btn-perfil.php?cod_prestador=';
                    window.location.href = 'resultados-btn-perfil.php?cod_prestador=' + val1 + '&titulo_negocio=' + val2;
                }
            </script>

        </footer>

    </body>

</html>