<?php
session_start();

if (!empty($_SESSION['prestador']) && !empty($_SESSION['senha']) && !empty($_SESSION['cod_prestador'])) {
    //header('Location:tela_prestador_login.php?cod_prestador=' . $_SESSION['cod_prestador'] . '&' . 'prestador=' . $_SESSION['prestador']);
    header('Location: prestador-login.php');
}
?>

<head>

    <!--PHP-->	
    <?php
    include("head.html");
    include('conecta-banco.php');
    include_once ("mensagem-modal-php.php");

    $pasta_imagem_perfil = "imagem-upload-perfil/";
    ?>

    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {

        if (!empty($_POST["prestador"]) && !empty($_POST["senha"])) {

            $prestador = "";
            $senha = "";

            $testeCodPrestador = "";
            $testePrestador = "";
            $testeSenha = "";

            $prestador = $_POST["prestador"];
            $senha = $_POST["senha"];

            $sql = $conn->query("SELECT cod_prestador, prestador, senha, titulo_negocio, link_imagem, tipo_imagem, titulacao, email
						FROM tbl_prestador
						WHERE prestador='$prestador' 
						AND senha='$senha'");

            if ($sql) {

                while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {

                    $testeCodPrestador = $row['cod_prestador'];
                    $testePrestador = $row['prestador'];
                    $testeSenha = $row['senha'];
                    $recebeTituloNegocio = $row['titulo_negocio'];
                    $recebeLinkImagem = $row['link_imagem'];
                    $recebeTipoImagem = $row['tipo_imagem'];
                    $recebeTitulacao = $row['titulacao'];
                    $recebeEmail = $row['email'];
                    
                }

                if ($testePrestador == $prestador && $testeSenha == $senha) {

                    $_SESSION['cod_prestador'] = $testeCodPrestador;
                    $_SESSION['prestador'] = $testePrestador;
                    $_SESSION['senha'] = $testeSenha;
                    $_SESSION['titulo_negocio'] = $recebeTituloNegocio;
                    $_SESSION['imagem_perfil'] = $pasta_imagem_perfil . $recebeLinkImagem . $recebeTipoImagem;
                    $_SESSION['titulacao'] = $recebeTitulacao;
                    $_SESSION['email'] = $recebeEmail;

                    $sql->connection = null;
                    $conn->connection = null;

                    //Recebo aqui a quantidade de publicacoes do usuário
                    $servername = "mysql.hostinger.com.br";
                    $username = "u457194965_bf";
                    $password = "24458149/*-";
                    $dbname = "u457194965_bf";

                    header('Location: prestador-login.php?cod_prestador=' . $testeCodPrestador);
                    
                } else {
                    
                    mensagemModalPhp("Atenção", "Prestador ou senha incorreto(s), ou não existem");
                    
                }
                
            } else {

                mensagemModalPhp("Atenção", 'Falha na conexão');
                
            }
            
        } else {

            mensagemModalPhp("Atenção", 'Preencha todos os campos.');
            
        }
        
    }
    
    ?>

</head>

<body>

    <?php include("body-nav-bar.php"); ?>

    <div class="container quebra_linha">

        <div class="row">

            <div class="col-md-4">

                <form name="formLogin" id="formLogin" method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">

                    <h3 class="brown-text">Baile de favela</h3> 

                    <section>

                        <div class="card" style="width: 19rem;">

                            <img class="card-img-top" src="http://megavirtua.com/baile-de-favela/imagem-site/baile-logo.jpg" alt="Card image cap">

                            <div class="card-body">

                                <div class="control-group form-group">
                                    <div class="controls">
                                        <h5 class="card-title"><a>Login</a></h5>
                                        <input type="text" class="form-control" value="<?php
    if (!empty($_POST['prestador'])) {
        echo $_POST['prestador'];
    }
    ?>" id="prestador"  name="prestador">
                                        <p class="help-block"></p>
                                    </div>
                                </div>

                                <div class="control-group form-group">
                                    <div class="controls">
                                        <h5 class="card-title"><a>Senha</a></h5>
                                        <input type="password" class="form-control" value="<?php
    if (!empty($_POST['senha'])) {
        echo $_POST['senha'];
    }
    ?>" id="senha" name="senha">
                                    </div>
                                </div>

                                <button type="submit" class="btn btn-sm btn-unique"><i class="fa fa-sign-in" aria-hidden="true"></i>  Entrar</button>

                                <div class="control-group form-group">

                                    <div class="controls">
                                        <a href="esqueci-a-senha.php">Esqueci a senha</a>	
                                    </div>

                                </div>

                            </div>

                        </div>

                    </section>

                </form>

            </div>

            <div class="col-md-8">

                <br><br>

                <!--Excerpt--> 
                <h5 class="green-text"> O Baile de favela é um grupo seleto que possui a forma mais simples e objetiva possível de divulgar seus projetos e estar em contato com as melhores propostas de negócios e até de lazer.</h5>

                <!--Grid row-->
                <div class="row">

                    <!--Grid column-->
                    <div class="col-lg-5 col-xl-5 pb-3">
                        <!--Featured image-->
                        <div class="view overlay rounded z-depth-2">
                            <img src="imagem-site/baile-logo-2.jpg" alt="Sample image for first version of blog listing" class="img-fluid">
                            <a>
                                <div class="mask waves-effect waves-light"></div> 
                            </a>
                        </div>
                    </div>
                    <!--Grid column--> 

                    <!--Grid column--> 
                    <div class="col-lg-7 col-xl-7">

                        <p>O funcionamento da plataforma é simples: publique, seja visto(a) e contactado(a).</p>

                    </div>
                    <!--Grid column-->

                </div>
                <!--Grid row-->

                <hr class="mb-5 mt-5 pb-3">

                <!--Grid row-->
                <div class="row">

                    <!--Grid column-->
                    <div class="col-lg-5 col-xl-5 pb-3">
                        <!--Featured image-->
                        <div class="view overlay rounded z-depth-2">
                            <img src="imagem-site/baile-logo-3.jpg" alt="Second image in the first version of blog listing" class="img-fluid">
                            <a>
                                <div class="mask waves-effect waves-light"></div>
                            </a>
                        </div>
                    </div>
                    <!--Grid column-->

                    <!--Grid column-->
                    <div class="col-lg-7 col-xl-7 pb-3">

                        <h5 class="pink-text">Mas como funciona o grupo? </h5>

                        <p>Aqui você cria uma publicação e a divulga, sendo que o seu tipo de membro influenciará na forma em que a postagem é vista, por exemplo:<br><br>

                            <strong class="blue-text">Membros diamante:</strong> Têm suas publicações mais novas aparecendo no mural principal, aparecem em primeiro nos resultados das buscas, 
                            tem direito a 25 publicações e tem buscas liberadas. (Benefícios vitalícios)<br><br>

                            <strong class="orange-text">Membros ouro:</strong> Não têm suas publicações  aparecendo no mural principal, aparecem após os diamantes nos resultados ( é o segundo tipo de usuário nas buscas), 
                            tem direito a 15 publicações e podem fazer buscas.(Benefícios vitalícios)<br><br>

                            <strong class="">Membros bronze:</strong> Não têm suas publicações no mural principal, aparecem em último nos resultados (após os diamantes e ouro), 
                            tem direito a 5 publicações e podem fazer buscas.(Benefícios vitalícios)<br><br>

                        </p>

                    </div>
                    <!--Grid column-->

                </div>
                <!--Grid row-->

                <hr class="mb-5 mt-4 pb-3">

                <!--Grid row-->
                <div class="row pb-5">

                    <!--Grid column-->
                    <div class="col-lg-5 col-xl-5 pb-3">
                        <!--Featured image-->
                        <div class="view overlay rounded z-depth-2">
                            <img src="imagem-site/contatos.jpg" alt="Thrid image in the blog listing." class="img-fluid">
                            <a>
                                <div class="mask waves-effect waves-light"></div>
                            </a>
                        </div>
                    </div>
                    <!--Grid column-->

                    <!--Grid column-->
                    <div class="col-lg-7 col-xl-7">
                        <h5 class="indigo-text"> Aumente sua rede de contatos ou investidores</h5>
                        <div class="col-lg-7 col-xl-7">
                            <p>Suas publicações e projetos serão exibidas para o mundo todo e não somente para um determinado grupo, já 
                                imaginou quantas propostas você poderá receber? Adquira sua conta e seja um membro deste grupo exclusivo.
                            </p>
                        </div>
                        <p></p>
                    </div>

                </div>

            </div>

        </div>

    </div>

    <footer>
<?php include("footer.html"); ?>        
    </footer>

</body>