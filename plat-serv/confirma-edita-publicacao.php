<?php

include('verifica-session.php');
?>

<?php

echo "<script>
    if (document.getElementById('titulo_negocio').value != '' && document.getElementById('categoria_publicacao').value != '' && document.getElementById('faixa_etaria').value != '' ) {
                                                     waitingDialog.show();
                                                     setTimeout(function () {
                                                         waitingDialog.hide();
                                                     }, 100000000000)
                                                 }
       </script>";

include('conecta-banco.php');
include("mensagem-modal-php.php");

$pasta_imagem = "imagem-upload-publicacoes/";
$pasta_video = "video-upload-publicacoes/";

if ($_POST['valor'] == 0 || empty($_POST['valor'])) {

    if (($_POST['valor_homem'] == 0 || empty($_POST['valor_homem'])) && ($_POST['valor_mulher'] == 0 || empty($_POST['valor_mulher']))) {

        $_POST['valor'] = 'Grátis';
        $_POST['valor_homem'] = '';
        $_POST['valor_mulher'] = ''
                ;
    } else {

        $_POST['valor'] = '';
        
    }
}

if (!empty($_SESSION['prestador']) && !empty($_SESSION['senha']) && !empty($_SESSION['cod_prestador'])) {

    $recebeCodPublicacao = $_POST['cod_publicacao'];
    $recebeTituloNegocio = $_POST['titulo_negocio'];
    $recebeCodPrestadorPublicacao = $_POST['cod_prestador_publicacao'];
    $recebeDescricao = $_POST['descricao'];
    $recebeFaixaEtaria = $_POST['faixa_etaria'];
    $recebeRua = $_POST['rua'];
    $recebeNumero = $_POST['numero'];
    $recebeComplemento = $_POST['complemento'];
    $recebeBairro = $_POST['bairro'];
    $recebeCidade = $_POST['cidade'];
    $recebeEstado = $_POST['estado'];
    $recebeCep = $_POST['cep'];
    $recebePais = $_POST['pais'];
    $recebeReferencia = $_POST['referencia'];
    $recebeContato = $_POST['contato'];
    $recebeDataDoEvento = $_POST['data_do_evento'];
    $recebeHorarioDoEvento = $_POST['horario_do_evento'];
    $recebeCategoriaPublicacao = $_POST['categoria_publicacao'];
    $recebeValor = $_POST['valor'];
    $recebeValorHomem = $_POST['valor_homem'];
    $recebeValorMulher = $_POST['valor_mulher'];

    $sql = "UPDATE tbl_publicacoes 
                                                SET 
                                                        titulo_negocio = '$recebeTituloNegocio', 
                                                        descricao = '$recebeDescricao',
                                                        faixa_etaria = '$recebeFaixaEtaria',
                                                        rua= '$recebeRua',
                                                        numero= '$recebeNumero',
                                                        complemento = '$recebeComplemento',
                                                        bairro = '$recebeBairro',
                                                        cidade = '$recebeCidade',
                                                        estado = '$recebeEstado',
                                                        cep = '$recebeCep',
                                                        pais = '$recebePais',
                                                        referencia = '$recebeReferencia',
                                                        contato = '$recebeContato',
                                                        data_do_evento = '$recebeDataDoEvento',
                                                        horario_do_evento = '$recebeHorarioDoEvento',
                                                        categoria_publicacao = '$recebeCategoriaPublicacao', 
                                                        valor = '$recebeValor',
                                                        valor_homem = '$recebeValorHomem',
                                                        valor_mulher = '$recebeValorMulher'
                                                          WHERE 
                                                              cod_publicacao=$recebeCodPublicacao";

    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $stmt = $conn->prepare($sql);
    $stmt->execute();

    $conn->connection = null;
    $stmt->connection = null;


    if ($_FILES['imagem_upload']['size'] > 0) {

        //Estre tratamento é nescessário, cao haja um caso muito específico de um usuário upar uma imagem com o mesmo nome da que está como padrão do serviror para um user sem imagem.
        if ($_FILES['imagem_upload']['name'] == "sem_foto.png") {
            $_FILES['imagem_upload']['name'] = "publicacao_sem_foto.png";
        }

            $tipoImagem = '.' . strtolower(end(explode('.', $_FILES['imagem_upload']['name'])));


            
                                 $sql = "UPDATE tbl_publicacoes 
                                    SET link_imagem= '$recebeCodPublicacao', tipo_imagem='$tipoImagem'
                                    WHERE 
                                    cod_publicacao = $recebeCodPublicacao";
                                 
        $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $stmt = $conn->prepare($sql);
        $stmt->execute();

        $conn->connection = null;
        $stmt->connection = null;                     
        
        $pasta_imagem = 'imagem-upload-publicacoes/';
        $image_name = $_FILES['imagem_upload']['name'];
        $image_tmp_name = $_FILES['imagem_upload']['tmp_name'];
        $location = $pasta_imagem . $image_name;
        unlink($pasta_imagem .$recebeCodPublicacao.$tipoImagem);
        move_uploaded_file($image_tmp_name, $location);
        rename($location, $pasta_imagem .$recebeCodPublicacao. $tipoImagem);

        
    } 
    
    //Tratamento do video
    if ($_FILES['video_upload']['size'] > 0) {

        //Estre tratamento é nescessário, cao haja um caso muito específico de um usuário upar uma imagem com o mesmo nome da que está como padrão do serviror para um user sem imagem.
        if ($_FILES['video_upload']['name'] == "sem_video.mp4") {
            $_FILES['video_upload']['name'] = "prestador_sem_video.mp4";
        }

        //Neste Caso o tipo do vídeo é retirado com o método explode
        $tipo_video = '.' . strtolower(end(explode('.', $_FILES['video_upload']['name'])));

        $sql = "UPDATE tbl_publicacoes 
                                    SET link_video= '$recebeCodPublicacao', tipo_video='$tipo_video'
                                    WHERE 
                                    cod_publicacao = $recebeCodPublicacao";

        $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        
        $sql->connection = null;
        $conn->connection = null;
        $stmt->connection = null;

        $pasta_video = 'video-upload-publicacoes/';
        $video_name = $_FILES['video_upload']['name'];
        $video_tmp_name = $_FILES['video_upload']['tmp_name'];
        $location = $pasta_video . $video_name;
        unlink($pasta_video .$recebeCodPublicacao.$tipo_video);
        move_uploaded_file($video_tmp_name, $location);
        rename($location, $pasta_video.$recebeCodPublicacao . $tipo_video);

    }else{
        
        
                 if ($_POST['video_youtube'] != '' || !empty($_POST['video_youtube'])) {

                            $recebeVideoYoutube=$_POST['video_youtube'];
                            $link_video = str_replace("/watch?v=", "/embed/",$recebeVideoYoutube);
                            $tipo_video = '';

                            $sql = "UPDATE tbl_publicacoes 
                                    SET link_video= '$recebeVideoYoutube', tipo_video=''
                                    WHERE 
                                    cod_publicacao = $recebeCodPublicacao";

                            $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
                            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                            $stmt = $conn->prepare($sql);
                            $stmt->execute();

                            $sql->connection = null;
                            $conn->connection = null;
                            $stmt->connection = null;
                            
                        }else{
          
                            $sql = "UPDATE tbl_publicacoes 
                                    SET link_video= '', tipo_video=''
                                    WHERE 
                                    cod_publicacao = $recebeCodPublicacao";

                            $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
                            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                            $stmt = $conn->prepare($sql);
                            $stmt->execute();

                            $sql->connection = null;
                            $conn->connection = null;
                            $stmt->connection = null;
                            
                              unlink($pasta_video .$recebeCodPublicacao.$tipo_video);
                   
                        }
        
    }
    //(FIM)Tratamento do video
    
            
        if (!empty($_SESSION['prestador']) && !empty($_SESSION['senha']) && !empty($_SESSION['cod_prestador'])) {
            header('Location: prestador-login.php?cod_prestador=' . $_SESSION['cod_prestador'] . '&' . 'prestador=' . $_SESSION['prestador']);
        } else {
            header('Location:index.php?');
        }
    
} else {

    header('Location:index.php?');
}
?>