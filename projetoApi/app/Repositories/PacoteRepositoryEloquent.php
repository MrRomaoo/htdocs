<?php



namespace App\Repositories;

use App\Models\Pacote;
use App\Repositories\PacoteRepositoryInterface;
use Illuminate\Http\Request;

class PacoteRepositoryEloquent implements PacoteRepositoryInterface {
   
    private $pacote;
    
    public function __construct(Pacote $pacote) {
        
        $this->pacote = $pacote;
    }
    
    public function buscarDetalhePacote(int $id) {
        
        return $this->pacote
                ->select(
                        'id',
                        'nome',
                        'valor',
                        'dataInicio',
                        'dataFim',
                        'urlImagem'
                                             
                        )
                ->where('id',$id)
                ->get();
               
        
    }

    public function buscarPacote(int $id) {
        
    }

    public function buscarTodosPacotes() {
        
    }

    public function criarPacote(Request $request) {
        
    }

    public function editarPacote(int $id, Request $request) {
        
    }

    public function excluirPacote(int $id) {
        
    }

}
